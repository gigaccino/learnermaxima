var mix = require('laravel-mix');
var webpack = require('webpack');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copy('resources/assets/img/favicon.png', 'public')
   .js('resources/assets/js/bootstrap.js', 'public/js')
   .js('resources/assets/js/video.js', 'public/js')
   .js('resources/assets/js/form.js', 'public/js')
   .js('resources/assets/js/register.js', 'public/js')
   .js('resources/assets/js/payout.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .extract(['jquery', 'lodash', 'popper.js', 'axios', 'bootstrap', '@vimeo/player', 'sweetalert', 'flatpickr'])
   .webpackConfig({
   		plugins: [
   			// Bootstrap v4 Dependencies
   			new webpack.ProvidePlugin({
               '$': 'jquery',
   				'jQuery': 'jquery',
               'window.jQuery': 'jquery',
   				Popper: ['popper.js', 'default'],
			   })
   		]
   })
