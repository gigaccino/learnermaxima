<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('classroom_id');
            $table->string('title');
            $table->text('instructions');
            $table->string('questions')->nullable();
            $table->string('solutions')->nullable();
            $table->integer('solutions_release_days')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
        DB::update("ALTER TABLE assignments AUTO_INCREMENT = 9527827;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
