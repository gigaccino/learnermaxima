<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendingOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pending_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transaction_type')->default('NEW');
            $table->string('order_number');
            $table->string('price');
            $table->string('subscription_length');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::update("ALTER TABLE pending_orders AUTO_INCREMENT = 6527827;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pending_orders');
    }
}
