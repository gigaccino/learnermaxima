<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('lesson_id');
            $table->integer('teacher_id');
            $table->integer('payout_id')->nullable();
            $table->string('transaction_type')->default('NEW');
            $table->string('order_number');
            $table->string('price');
            $table->string('transaction_id');
            $table->dateTime('payment_date');
            $table->dateTime('refund_date')->nullable();
            $table->dateTime('reminder_date')->nullable();
            $table->integer('subscription_length');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::update("ALTER TABLE subscriptions AUTO_INCREMENT = 7527827;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
