<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->text('note')->nullable();
            $table->integer('credit')->default(0);
            $table->string('photo')->nullable();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('public_contact_email')->nullable();
            $table->string('phone');
            $table->string('tagline')->nullable();
            $table->string('identification')->nullable();
            $table->string('qualifications')->nullable();
            $table->text('teaching_experience')->nullable();
            $table->string('payout_method')->nullable();
            $table->string('bank')->nullable();
            $table->string('bank_account_number')->nullable();
            $table->text('address')->nullable();
            $table->string('password');
            $table->integer('privilege')->default(1);
            $table->integer('pending_upgrade')->default(0);
            $table->rememberToken();
            $table->dateTime('teacher_away_begin')->nullable();
            $table->dateTime('teacher_away_end')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        DB::update("ALTER TABLE users AUTO_INCREMENT = 1527827;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
