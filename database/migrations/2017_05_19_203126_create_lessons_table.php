<?php

use App\Defaults\Level;
use App\Defaults\Subject;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('photo')->nullable();
            $table->string('subject');
            $table->string('level');
            $table->string('price');
            $table->dateTime('last_publish_date');
            $table->dateTime('marked_for_deletion')->nullable();
            $table->integer('subscriptions_disabled')->default(0);
            $table->text('description');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::update("ALTER TABLE lessons AUTO_INCREMENT = 3527827;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lessons');
    }
}
