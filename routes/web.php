<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// THROTTLES
$throttle_15 = 'GrahamCampbell\Throttle\Http\Middleware\ThrottleMiddleware:5,15';
$throttle_60 = 'GrahamCampbell\Throttle\Http\Middleware\ThrottleMiddleware:5,60';


// PAGES
Route::get('/', 'PageController@home')
	->name('page.home');

Route::get('/how-it-works', 'PageController@howItWorks')
	->name('page.howItWorks');

Route::get('/faq', 'PageController@faq')
	->name('page.faq');

Route::get('/privacy', 'PageController@privacy')
	->name('page.privacy');

Route::get('/terms', 'PageController@terms')
	->name('page.terms');


// MY
Route::get('/my-lessons', 'LessonController@myLessons')
	->name('lesson.myLessons');

Route::get('/my-subscriptions', 'SubscriptionController@mySubscriptions')
	->name('subscription.mySubscriptions');

Route::get('/my-profile', 'UserController@myProfile')
	->name('user.myProfile');

Route::get('/my-account', 'UserController@myAccount')
	->name('user.myAccount');

// ADMIN
Route::get('/admin', 'AdminController@home')
	->name('admin.home');

Route::get('/admin/users', 'AdminController@users')
	->name('admin.users');

Route::get('/admin/lessons', 'AdminController@lessons')
	->name('admin.lessons');

Route::get('/admin/subscriptions', 'AdminController@subscriptions')
	->name('admin.subscriptions');

Route::get('/admin/find', 'AdminController@find')
	->name('admin.find');

// USERS

Route::get('/users/{user}', 'UserController@show')
	->name('user.show');

Route::get('/users/{user}/edit', 'UserController@edit')
	->name('user.edit');

Route::get('/users/{user}/profile/edit', 'UserController@editProfile')
	->name('user.editProfile');

Route::get('/users/{user}/payouts/edit', 'UserController@editPayoutDetails')
	->name('user.editPayoutDetails');

Route::get('/users/{user}/payoff', 'UserController@showPayOff')
	->name('user.showPayOff');

Route::get('/users/{user}/payouts', 'UserController@showPayouts')
	->name('user.showPayouts');

Route::get('/users/{user}/upgrade-to-teacher', 'UserController@showUpgradeToRegisteredTeacher')
	->name('user.showUpgradeToRegisteredTeacher');

Route::get('/users/{user}/become-a-tutor-or-teacher', 'UserController@showBecomeATeacher')
	->name('user.showBecomeATeacher');

Route::get('/users/{user}/profile/edit', 'UserController@editProfile')
	->name('user.editProfile');

Route::patch('/users/{user}', 'UserController@update')
	->name('user.update');

Route::patch('/users/{user}/profile', 'UserController@updateProfile')
	->name('user.updateProfile');

Route::patch('/users/{user}/payout', 'UserController@updatePayoutDetails')
	->name('user.updatePayoutDetails');

Route::post('/users/{user}/payoff', 'UserController@payOff')
	->name('user.payOff');

Route::post('/users/{user}/upgrade', 'UserController@upgrade')
	->name('user.upgrade');

Route::post('/users/{user}/upgrade/cancel', 'UserController@cancelUpgrade')
	->name('user.cancelUpgrade');

Route::post('/users/{user}/downgrade', 'UserController@downgrade')
	->name('user.downgrade');

Route::post('/users/{user}/become-a-tutor-teacher', 'UserController@becomeATeacher')
	->name('user.becomeATeacher');

Route::post('/users/{user}/upgrade-to-teacher', 'UserController@upgradeToRegisteredTeacher')
	->name('user.upgradeToRegisteredTeacher');

Route::post('/users/{user}/photo', 'UserController@storePhoto')
	->name('user.storePhoto');

Route::post('/users/{user}/identification', 'UserController@storeIdentification')
	->name('user.storeIdentification');

Route::post('/users/{user}/qualifications', 'UserController@storeQualifications')
	->name('user.storeQualifications');

Route::post('/users/{user}/gift', 'UserController@gift')
	->name('user.gift');

Route::put('/users/{user}/photo', 'UserController@replacePhoto')
	->name('user.replacePhoto');

Route::delete('/users/{user}/photo', 'UserController@deletePhoto')
	->name('user.deletePhoto');

Route::delete('/users/{user}/identification', 'UserController@deleteIdentification')
	->name('user.deleteIdentification');

Route::delete('/users/{user}/qualifications', 'UserController@deleteQualifications')
	->name('user.deleteQualifications');

Route::delete('/users/{user}', 'UserController@destroy')
	->name('user.destroy');


// LESSONS
Route::get('/lessons', 'LessonController@index')
	->name('lesson.index');

Route::get('/lessons/create', 'LessonController@create')
	->name('lesson.create');

Route::get('/lessons/{lesson}', 'LessonController@show')
	->name('lesson.show');

Route::get('/lessons/{lesson}/edit', 'LessonController@edit')
	->name('lesson.edit');

Route::patch('/lessons/{lesson}', 'LessonController@update')
	->name('lesson.update');

Route::post('/lessons', 'LessonController@store')
	->name('lesson.store');

Route::post('/lessons/{lesson}/photo', 'LessonController@storePhoto')
	->name('lesson.storePhoto');

Route::post('/lessons/{lesson}/subscriptions/enable', 'LessonController@enableSubscriptions')
	->name('lesson.enableSubscriptions');

Route::post('/lessons/{lesson}/subscriptions/disable', 'LessonController@disableSubscriptions')
	->name('lesson.disableSubscriptions');

Route::put('/lessons/{lesson}/photo', 'LessonController@replacePhoto')
	->name('lesson.replacePhoto');

Route::delete('/lessons/{lesson}', 'LessonController@destroy')
	->name('lesson.destroy')
	->middleware($throttle_60);

Route::delete('/lessons/{lesson}/photo', 'LessonController@deletePhoto')
	->name('lesson.deletePhoto');


// CLASSROOMS
Route::get('/lessons/{lesson}/classrooms/create', 'ClassroomController@create')
	->name('classroom.create');

Route::get('/lessons/{lesson}/classrooms/{classroom_number}', 'ClassroomController@show')
	->name('classroom.show');

Route::get('/lessons/{lesson}/classrooms/{classroom_number}/edit', 'ClassroomController@edit')
	->name('classroom.edit');

Route::get('/lessons/{lesson}/classrooms/{classroom_number}/video/upload', 'ClassroomController@uploadVideo')
	->name('classroom.uploadVideo')
	->middleware($throttle_15);

Route::get('/classrooms/{classroom}/video/uploaded', 'ClassroomController@uploadVideoComplete')
	->name('classroom.uploadVideoComplete');

Route::patch('/classrooms/{classroom}', 'ClassroomController@update')
	->name('classroom.update');

Route::post('/lessons/{lesson}/classrooms', 'ClassroomController@store')
	->name('classroom.store');

Route::post('/classrooms/{classroom}/attachment', 'ClassroomController@storeAttachment')
	->name('classroom.storeAttachment');

Route::post('/classrooms/{classroom}/publish', 'ClassroomController@publish')
	->name('classroom.publish');

Route::post('/classrooms/{classroom}/unpublish', 'ClassroomController@unpublish')
	->name('classroom.unpublish');

Route::delete('/classrooms/{classroom}', 'ClassroomController@destroy')
	->name('classroom.destroy')
	->middleware($throttle_15);

Route::delete('/classrooms/{classroom}/video', 'ClassroomController@deleteVideo')
	->name('classroom.deleteVideo')
	->middleware($throttle_15);

Route::delete('/classrooms/{classroom}/attachment', 'ClassroomController@deleteAttachment')
	->name('classroom.deleteAttachment');


// PROGRESS
Route::post('/classrooms/{classroom}/progresses', 'ProgressController@store')
	->name('progress.store');


// ASSIGNMENTS
Route::get('/lessons/{lesson}/classrooms/{classroom_number}/assignments', 'AssignmentController@index')
	->name('assignment.index');

Route::get('/lessons/{lesson}/classrooms/{classroom_number}/assignments/create', 'AssignmentController@create')
	->name('assignment.create');

Route::get('/lessons/{lesson}/classrooms/{classroom_number}/assignments/{assignment_number}/edit', 'AssignmentController@edit')
	->name('assignment.edit');

Route::patch('/assignment/{assignment}', 'AssignmentController@update')
	->name('assignment.update');

Route::post('/classrooms/{classroom}/assignments', 'AssignmentController@store')
	->name('assignment.store');

Route::delete('/assignment/{assignment}', 'AssignmentController@destroy')
	->name('assignment.destroy');


// QUIZZES
Route::get('/lessons/{lesson}/classrooms/{classroom_number}/quizzes', 'QuizController@index')
	->name('quiz.index');

Route::get('/lessons/{lesson}/classrooms/{classroom_number}/quizzes/create', 'QuizController@create')
	->name('quiz.create');

Route::get('/lessons/{lesson}/classrooms/{classroom_number}/quizzes/{quiz_number}', 'QuizController@show')
	->name('quiz.show');

Route::get('/lessons/{lesson}/classrooms/{classroom_number}/quizzes/{quiz_number}/edit', 'QuizController@edit')
	->name('quiz.edit');

Route::patch('/quizzes/{quiz}', 'QuizController@update')
	->name('quiz.update');

Route::post('/classrooms/{classroom}/quizzes', 'QuizController@store')
	->name('quiz.store');

Route::delete('/quizzes/{quiz}', 'QuizController@destroy')
	->name('quiz.destroy');


// QUESTIONS
Route::get('/lessons/{lesson}/classrooms/{classroom_number}/quizzes/{quiz_number}/questions/create', 'QuestionController@create')
	->name('question.create');

Route::get('/lessons/{lesson}/classrooms/{classroom_number}/quizzes/{quiz_number}/questions/{question_number}/edit', 'QuestionController@edit')
	->name('question.edit');

Route::patch('/questions/{question}', 'QuestionController@update')
	->name('question.update');

Route::post('/quizzes/{quiz}/questions', 'QuestionController@store')
	->name('question.store');

Route::post('/questions/{question}/photo', 'QuestionController@storePhoto')
	->name('question.storePhoto');

Route::put('/questions/{question}/photo', 'QuestionController@replacePhoto')
	->name('question.replacePhoto');

Route::delete('/questions/{question}', 'QuestionController@destroy')
	->name('question.destroy');

Route::delete('/questions/{question}/photo', 'QuestionController@deletePhoto')
	->name('question.deletePhoto');


// ANSWERS
Route::post('/quizzes/{quiz}/answers', 'AnswerController@store')
	->name('answer.store');


// DISCUSSIONS
Route::get('/lessons/{lesson}/classrooms/{classroom_number}/discussions/create', 'DiscussionController@create')
	->name('discussion.create');

Route::get('/lessons/{lesson}/classrooms/{classroom_number}/discussions/{discussion_number}/edit', 'DiscussionController@edit')
	->name('discussion.edit');

Route::patch('/discussions/{discussion}', 'DiscussionController@update')
	->name('discussion.update');

Route::post('/classrooms/{classroom}/discussions', 'DiscussionController@store')
	->name('discussion.store');

Route::delete('/discussions/{discussion}', 'DiscussionController@destroy')
	->name('discussion.destroy');


// REVIEWS

Route::get('/lessons/{lesson}/reviews/create', 'ReviewController@create')
	->name('review.create');

Route::get('/lessons/{lesson}/reviews/{review_number}/edit', 'ReviewController@edit')
	->name('review.edit');

Route::patch('/reviews/{review}', 'ReviewController@update')
	->name('review.update');

Route::post('/lessons/{lesson}/reviews', 'ReviewController@store')
	->name('review.store');

Route::delete('/reviews/{review}', 'ReviewController@destroy')
	->name('review.destroy');


// SUBSCRIPTIONS, ORDERS AND PAYMENTS
Route::get('/lessons/{lesson}/subscribe', 'SubscriptionController@create')
	->name('subscription.create');

Route::get('/lessons/{lesson}/renew', 'SubscriptionController@createRenew')
	->name('subscription.createRenew');

Route::get('/lessons/{lesson}/subscribe-for-free', 'SubscriptionController@createFree')
	->name('subscription.createFree');

Route::get('/lessons/{lesson}/subscribe/pay', 'SubscriptionController@createOrder')
	->name('subscription.createOrder')
	->middleware($throttle_15);

Route::get('/lessons/{lesson}/renew/pay', 'SubscriptionController@createRenewOrder')
	->name('subscription.createRenewOrder')
	->middleware($throttle_15);

Route::get('/lessons/{lesson}/subscriptions', 'SubscriptionController@index')
	->name('subscription.index');

Route::get('/subscriptions/order/{subscription}', 'SubscriptionController@show')
	->name('subscription.show');

Route::get('/subscriptions/process/wipay', 'SubscriptionController@processWipayOrder')
	->name('subscription.processWipayOrder');

Route::get('/subscriptions/process/wipay-cash', 'SubscriptionController@processWipayCashOrder')
	->name('subscription.processWipayCashOrder');

Route::get('/subscriptions/renew/process/wipay', 'SubscriptionController@processWipayRenewOrder')
	->name('subscription.processWipayRenewOrder');

Route::get('/subscriptions/renew/process/wipay-cash', 'SubscriptionController@processWipayCashRenewOrder')
	->name('subscription.processWipayCashRenewOrder');

Route::post('/lessons/{lesson}/subscribe/free', 'SubscriptionController@storeFree')
	->name('subscription.storeFree');

Route::post('/subscriptions/{subscription}/refund', 'SubscriptionController@refund')
	->name('subscription.refund');


// AUTH
Auth::routes();
