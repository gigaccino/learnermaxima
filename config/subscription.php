<?php

return [

	'price' => env('SUBSCRIPTION_PRICE', '149.00'),
	'gigaccino_fee' => env('GIGACCINO_FEE', '49.00'),
	'payment_processor_fee' => env('PAYMENT_PROCESSOR_FEE', '10.00'),

];