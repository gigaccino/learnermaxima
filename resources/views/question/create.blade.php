@extends ('layouts.form')

@section ('title', 'Adding Question to Quiz')

@section ('subtitle')
	<a href="{{ route('quiz.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number, 'quiz_number' => $quiz_number]) }}">{{ $quiz->title }} - {{ $classroom->title }} - {{ $lesson->level }} {{ $lesson->subject }}</a>
@endsection

@section ('form')

    @component ('components.form.master', ['method' => 'POST', 'form_action' => 'Add Question','url' => route('question.store', ['quiz' => $quiz->id]), 'enctype' => 'multipart/form-data', 'csrf_token' => csrf_token()])

    	@component ('components.form.textarea', ['name'=> 'question', 'errors' => $errors, 'max_characters' => \App\Question::getMaxQuestionLength(), 'value'=> old('question')])
    	    Question
    	@endcomponent

        @component ('components.form.file', ['name'=> 'photo', 'errors' => $errors, 'not_required' => true])
            Question Photo (optional)
        @endcomponent

    	@component ('components.form.textarea', ['name'=> 'answer_a', 'errors' => $errors, 'max_characters' => \App\Question::getMaxAnswerLength(), 'value'=> old('answer_a')])
    	    Answer A
    	@endcomponent

    	@component ('components.form.textarea', ['name'=> 'answer_b', 'errors' => $errors, 'max_characters' => \App\Question::getMaxAnswerLength(), 'value'=> old('answer_b')])
    	    Answer B
    	@endcomponent

    	@component ('components.form.textarea', ['name'=> 'answer_c', 'errors' => $errors, 'max_characters' => \App\Question::getMaxAnswerLength(), 'value'=> old('answer_c')])
    	    Answer C
    	@endcomponent

    	@component ('components.form.textarea', ['name'=> 'answer_d', 'errors' => $errors, 'max_characters' => \App\Question::getMaxAnswerLength(), 'value'=> old('answer_d')])
    	    Answer D
    	@endcomponent

        @component ('components.form.radio', ['name'=> 'correct_answer', 'errors' => $errors, 'option_list_value' => ['a', 'b', 'c', 'd'], 'option_list_text' => ['A', 'B', 'C', 'D'], 'in_line' => true])
            Correct Answer
        @endcomponent

    @endcomponent

@endsection
