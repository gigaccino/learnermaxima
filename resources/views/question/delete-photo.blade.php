@component ('components.form.master', ['method' => 'DELETE', 'form_action' => 'Delete Photo', 'btn_class' => 'btn-danger','url' => route('question.deletePhoto', ['question' => $question->id]), 'csrf_token' => csrf_token()])
@endcomponent
