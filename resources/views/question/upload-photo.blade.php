@component ('components.form.master',['method' => 'POST', 'form_action' => 'Upload Photo','url' => route('question.storePhoto', ['question' => $question->id]), 'enctype' => 'multipart/form-data', 'csrf_token' => csrf_token()])

    @component ('components.form.file', ['name'=> 'photo'])
        Photo
    @endcomponent

@endcomponent
