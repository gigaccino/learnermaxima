@extends ('layouts.form')

@section ('title', 'Updating Question '.$question_number)

@section ('subtitle') 
    <a href="{{ route('quiz.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number, 'quiz_number' => $quiz_number]) }}">{{ $quiz->title }} - {{ $classroom->title }} - {{ $lesson->level }} {{ $lesson->subject }}</a>
@endsection

@section ('form')

    <h3 class="text-center text-secondary">Correct Answer: <span class="text-uppercase">{{ $question->correct_answer }}</span></h3>
    <p class="text-center"><small>Please ensure that the correct answer is entered into the <span class="text-secondary">green text field below</span>.</small></p>

    <hr>

    <div class="row text-muted justify-content-center">
        <p class="text-center">Question Photo</p>
    </div>

    <div class="row mb-3 justify-content-center">
        @can ('deletePhoto', [\App\Question::class, $question])
            <div class="col-sm-6">
                <img class="card-img-top rounded-0" src="{{ $question->photo_url }}" alt="Question Photo">
            </div>
            <div class="col-sm-6 text-center pt-2">
                @include ('question.replace-photo')
            </div>
        @else
            <div class="col-sm-6">
                @can ('uploadPhoto', [\App\Question::class, $question])
                    @include ('question.upload-photo')
                @endcan
            </div>
        @endcan
    </div>

    
    @can ('deletePhoto', [\App\Question::class, $question])
        <div class="row mb-3 justify-content-center">
            @include ('question.delete-photo')
        </div>
    @endcan
    
    <hr>

    @component ('components.form.master', ['method' => 'PATCH', 'form_action' => 'Update Question','url' => route('question.update', ['question' => $question->id]), 'enctype' => 'multipart/form-data', 'csrf_token' => csrf_token()])

    	@component ('components.form.textarea', ['name'=> 'question','errors' => $errors, 'max_characters' => \App\Question::getMaxQuestionLength(), 'value'=> $question->question])
    	    Question
    	@endcomponent

    	@component ('components.form.textarea', ['name'=> 'answer_a', 'highlight' => ($question->correct_answer === 'a'),'errors' => $errors, 'max_characters' => \App\Question::getMaxAnswerLength(), 'value' => $question->answer_a])
    	    Answer A
    	@endcomponent

    	@component ('components.form.textarea', ['name'=> 'answer_b', 'highlight' => ($question->correct_answer === 'b'),'errors' => $errors, 'max_characters' => \App\Question::getMaxAnswerLength(), 'value' => $question->answer_b])
    	    Answer B
    	@endcomponent

    	@component ('components.form.textarea', ['name'=> 'answer_c', 'highlight' => ($question->correct_answer === 'c'),'errors' => $errors, 'max_characters' => \App\Question::getMaxAnswerLength(), 'value' => $question->answer_c])
    	    Answer C
    	@endcomponent

    	@component ('components.form.textarea', ['name'=> 'answer_d', 'highlight' => ($question->correct_answer === 'd'),'errors' => $errors, 'max_characters' => \App\Question::getMaxAnswerLength(), 'value' => $question->answer_d])
    	    Answer D
    	@endcomponent

    @endcomponent

    <hr>

    @can ('delete', [\App\Question::class, $question])
        <div class="row mb-3 justify-content-center">
            @include ('question.destroy')
        </div>
    @endcan

@endsection
