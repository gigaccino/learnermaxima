@component ('components.form.master', ['method' => 'DELETE', 'form_action' => 'Delete Question', 'btn_class' => 'btn-danger', 'url' => route('question.destroy', ['question' => $question->id]), 'csrf_token' => csrf_token()])
@endcomponent
