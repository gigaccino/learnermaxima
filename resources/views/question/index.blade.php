@if ($quiz->questions->count() < 1)
	<div class="container p-5">
		<h3 class="text-center">{{ $quiz->classroom->lesson->user->name }} hasn't added any Questions to this Quiz.</h3>
	</div>
@endif

<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8 col-lg-6">

			@include ('flash::message')

			@if (!$quiz->completed && 
				 Gate::denies('create', [\App\Question::class, $quiz]) && 
				 Gate::allows('create', [\App\Answer::class, $quiz]))

				@component ('components.form.master', ['method' => 'POST', 'form_action' => 'Submit Answers', 'url' => route('answer.store', ['quiz' => $quiz->id]), 'hide_submit' => $quiz->completed, 'csrf_token' => csrf_token()])
						
					@foreach ($questions as $index=>$question)

					@component('components.quiz.question', ['question' => $question, 'lesson' => $lesson, 'classroom_number' => $classroom_number, 'quiz_number' => $quiz_number, 'question_number' => $index + 1])

							@if (auth()->user()->getAnswer($question) === null)
								@component ('components.form.radio', ['name'=> 'answer_for_question_'.($index + 1), 'errors' => $errors, 'option_list_value' => ['a', 'b', 'c', 'd'], 'option_list_text' => [$question->answer_a, $question->answer_b, $question->answer_c, $question->answer_d]])
								    Question {{ $index + 1 }} Answer
								@endcomponent
							@else
								@component('components.quiz.answers', ['question' => $question])
								@endcomponent
							@endif

							@if (($index + 1) < $questions->count())
								<hr>
							@endif

						@endcomponent

					@endforeach

				@endcomponent


			@else

				@foreach ($questions as $index=>$question)

					@component('components.quiz.question', ['question' => $question, 'lesson' => $lesson, 'classroom_number' => $classroom_number, 'quiz_number' => $quiz_number, 'question_number' => $index + 1])

						@component('components.quiz.answers', ['question' => $question])
						@endcomponent

						@if (($index + 1) < $questions->count())
							<hr>
						@endif

					@endcomponent

				@endforeach

			@endif

		</div>
	</div>
</div>