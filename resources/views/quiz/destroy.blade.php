@component ('components.form.master', ['method' => 'DELETE', 'form_action' => 'Delete Quiz', 'btn_class' => 'btn-danger','url' => route('quiz.destroy', ['quiz' => $quiz->id]), 'csrf_token' => csrf_token()])
@endcomponent
