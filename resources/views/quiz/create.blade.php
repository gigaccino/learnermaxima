@extends ('layouts.form')

@section ('title', 'Creating Quiz')

@section ('subtitle')
	<a href="{{ route('classroom.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number]) }}">
	    {{ $classroom->title }} - {{ $lesson->level }} {{ $lesson->subject }}</a>
@endsection

@section ('form')

    @component ('components.form.master', ['method' => 'POST', 'form_action' => 'Create Quiz','url' => route('quiz.store', ['classroom' => $classroom->id]), 'csrf_token' => csrf_token()])

        @component ('components.form.input', ['name'=> 'title', 'type'=> 'text', 'errors' => $errors, 'value'=> old('title')])
            Title
        @endcomponent

        <p class="text-center">You will be asked to add your first question on the next page.</p>

    @endcomponent

@endsection
