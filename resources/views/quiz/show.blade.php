@extends ('layouts.form')

@section ('title')
	{{ $quiz->title }} - {{ $classroom->title }} - {{ $lesson->level }} {{ $lesson->subject }}, taught by {{ $lesson->user->name }}
@endsection

@section ('content')

	@component('components.banner.small')
	    <h2>{{ $quiz->title }}</h2>
	    
	    <small><a href="{{ route('classroom.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number]) }}">
	        {{ $classroom->title }} - {{ $lesson->level }} {{ $lesson->subject }}
	    </a></small>
	@endcomponent

	<div class="container">

		@include ('question.index')

		@can ('create', [\App\Question::class, $quiz])
		    <div class="row mb-3 justify-content-center">
		        <a href="{{ route('question.create', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number, 'quiz_number' => $quiz->number]) }}" class="btn btn-secondary text-white">Add Question</a>
		    </div>
		@endcan
		
	</div>

@endsection
