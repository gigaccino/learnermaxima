@extends ('layouts.app')

@section ('title')
	Quizzes - {{ $classroom->title }} - {{ $lesson->level }} {{ $lesson->subject }}, taught by {{ $lesson->user->name }}
@endsection

@section ('content')

	@component('components.banner.small')
	    <h2>Quizzes</h2>

	    <small><a href="{{ route('classroom.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number]) }}">
	        {{ $classroom->title }} - {{ $lesson->level }} {{ $lesson->subject }}
	    </a></small>
	@endcomponent

	@if ($classroom->quizzes->count() < 1)
		<div class="container p-5">
			<h3 class="text-center">{{ $lesson->user->name }} hasn't uploaded a Quiz to this Classroom.</h3>
		</div>
	@endif

	<div class="container">
		@foreach ($classroom->quizzes as $index=>$quiz)
			@component ('components.quiz.card', ['lesson' => $lesson, 'classroom' => $classroom, 'quiz' => $quiz, 'classroom_number' => $classroom_number, 'index' => $index])
			@endcomponent
		@endforeach
		@can ('create', [\App\Quiz::class, $classroom])
			<div class="row justify-content-center">
				<a href="{{ route('quiz.create', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number]) }}" class="btn btn-secondary text-white">Create Quiz</a>
			</div>
		@endcan
	</div>

@endsection
