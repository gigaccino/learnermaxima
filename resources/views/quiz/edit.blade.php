@extends ('layouts.form')

@section ('title', 'Updating your Quiz')

@section ('subtitle')
    <a href="{{ route('quiz.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number, 'quiz_number' => $quiz_number]) }}">Quiz: {{ $quiz->title }} - {{ $classroom->title }} - {{ $lesson->level }} {{ $lesson->subject }}</a>
@endsection

@section ('form')

    <div class="container-fluid text-center mt-3">
        @include ('flash::message')
    </div>

    @component ('components.form.master', ['method' => 'PATCH', 'form_action' => 'Update Quiz','url' => route('quiz.update', ['quiz' => $quiz->id]), 'csrf_token' => csrf_token()])

        @component ('components.form.input', ['name'=> 'title', 'type'=> 'text', 'errors' => $errors, 'value'=> $quiz->title])
            Title
        @endcomponent

    @endcomponent

    <hr>
    
    @can ('delete', [\App\Quiz::class, $quiz])
        <div class="row mb-3 justify-content-center">
            @include ('quiz.destroy')
        </div>
    @endcan

@endsection
