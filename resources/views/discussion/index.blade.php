@if($classroom->discussions->count() > 0)
	<hr>
	<div class="text-center">
		<h3>Latest Discussions</h3>
	</div>
	<div class="row justify-content-center">
		<div class="col-8">
			@foreach ($root_discussions as $index=>$discussion)
				<div class="mb-3">
					@component('components.discussion.comment', ['discussion' => $discussion, 'discussion_number'=> ($index + 1), 'classroom' => $classroom, 'lesson' => $lesson])
					@endcomponent
					@can('create', [\App\Discussion::class, $classroom])
						<small><a href="{{ route('discussion.create', ['lesson' => $lesson->pretty_id, 'classroom' => $classroom->number, 'replyto' => $discussion->id]) }}">Reply</a></small>
					@endcan
					<div class="ml-5">
						@foreach ($discussion->child_discussions as $child_index=>$child_discussion)
							@component('components.discussion.comment', ['discussion' => $child_discussion, 'discussion_number'=> $discussion->number, 'classroom' => $classroom, 'lesson' => $lesson])
							@endcomponent
						@endforeach
					</div>
				</div>
			@endforeach
		</div>
	</div>
@endif

<div class="text-center">
	@can('create', [\App\Discussion::class, $classroom])
		<a href="{{ route('discussion.create', ['lesson' => $lesson->pretty_id, 'classroom' => $classroom->number]) }}" class="btn btn-secondary text-white">New Comment</a>
	@endcan
</div>

<div class="row justify-content-center pt-2">

	{{ $root_discussions->links() }}

</div>