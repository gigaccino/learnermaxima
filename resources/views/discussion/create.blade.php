@extends ('layouts.form')

@section ('title', 'Writing a Comment')

@section ('subtitle')
    <a href="{{ route('classroom.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number]) }}">
    {{ $classroom->title }} - {{ $lesson->level }} {{ $lesson->subject }}</a>
@endsection

@section ('form')
	
	@if (isset($replyto))
		<p class="mb-2 lead text-muted">Replying to:</p>
		<div class="card">
			<div class="card-body">
			  <div class="mb-1">
			  	<a href="{{ route('user.show', ['user' => $replyto->user->pretty_id]) }}">
			  	    {{ $replyto->user->name }}
			  	</a>
			  	<span class="text-muted">
			  		•
			  		<small>{{ $replyto->created_at->diffForHumans() }}</small>
			  	</span>
			  </div>
			  <p class="mb-1">{{ $replyto->body }}</p>
			</div>
		</div>
		<hr>
	@endif

    @component ('components.form.master', ['method' => 'POST', 'form_action' => 'Post Comment','url' => route('discussion.store', ['classroom' => $classroom->id]), 'csrf_token' => csrf_token()])

        @component ('components.form.textarea', ['name'=> 'body', 'errors' => $errors, 'max_characters' => \App\Discussion::getMaxBodyLength(), 'value'=> old('body')])
            Comment
        @endcomponent

        @if (isset($replyto))
			<input type="hidden" name="replyto" value="{{ $replyto->id }}">
		@endif

    @endcomponent

@endsection
