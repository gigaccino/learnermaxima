@component ('components.form.master', ['method' => 'DELETE', 'form_action' => 'Delete Discussion', 'btn_class' => 'btn-danger','url' => route('discussion.destroy', ['discussion' => $discussion->id]), 'csrf_token' => csrf_token()])
@endcomponent
