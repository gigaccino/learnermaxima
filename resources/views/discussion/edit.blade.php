@extends ('layouts.form')

@section ('title', 'Updating your Discussion')

@section ('subtitle')
    <a href="{{ route('classroom.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number]) }}">
    {{ $classroom->title }} - {{ $lesson->level }} {{ $lesson->subject }}</a>
@endsection

@section ('form')

    @component ('components.form.master', ['method' => 'PATCH', 'form_action' => 'Update Discussion','url' => route('discussion.update', ['discussion' => $discussion->id]), 'csrf_token' => csrf_token()])

        @component ('components.form.textarea', ['name'=> 'body', 'errors' => $errors, 'max_characters' => \App\Discussion::getMaxBodyLength(), 'value'=> $discussion->body])
            Body
        @endcomponent

    @endcomponent

    <hr>

	@can ('delete', [\App\Discussion::class, $discussion])
		<div class="row mb-3 justify-content-center">
	    	@include ('discussion.destroy')
	    </div>
	@endcan

@endsection
