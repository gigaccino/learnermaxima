<table class="pb-2 table table-striped">
    <thead>
        <tr>
            <th>Subscriptions</th>
            <th>Total Income</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{ $user->teacher_subscriptions->count() }}</td>
            <td>${{ $user->amount_owed }} TTD</td>
        </tr>
    </tbody>
</table>