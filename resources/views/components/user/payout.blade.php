@if ($user->payout_method === 'Bank Transfer')
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Bank</th>
                <th>Account No.</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $user->bank }}</td>
                <td>{{ $user->bank_account_number }}</td>
            </tr>
        </tbody>
    </table>
@elseif ($user->payout_method === 'Cheque')
    <h3>Pay by Cheque</h3>
    <p>Mailed to:</p>
    <p>{{ $user->address }}</p>
@endif