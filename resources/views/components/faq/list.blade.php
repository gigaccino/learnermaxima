<div class="row justify-content-center">
	<div class="col-md-8">
		<div id="faq" role="tablist">
			{{ $slot }}
		</div>
	</div>
</div>