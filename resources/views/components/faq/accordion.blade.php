<div class="card">
	<div class="card-header" role="tab" id="heading-{{ str_replace(' ', '-', preg_replace("/[^a-zA-Z0-9\s]/", "", strtolower($title))) }}">
	<h5 class="mb-0 text-center">
		<a {{ (isset($is_collapsed) ? 'class="collapsed"' : '') }} data-toggle="collapse" href="#collapse-{{ str_replace(' ', '-', preg_replace("/[^a-zA-Z0-9\s]/", "", strtolower($title))) }}" role="button" aria-expanded="true" aria-controls="collapse-{{ str_replace(' ', '-', preg_replace("/[^a-zA-Z0-9\s]/", "", strtolower($title))) }}">
		 {{ $title }}
		</a>
	</h5>
	</div>

	<div id="collapse-{{ str_replace(' ', '-', preg_replace("/[^a-zA-Z0-9\s]/", "", strtolower($title))) }}" class="collapse {{ (isset($is_collapsed) ? 'show' : '') }}" role="tabpanel" aria-labelledby="heading-{{ str_replace(' ', '-', preg_replace("/[^a-zA-Z0-9\s]/", "", strtolower($title))) }}" data-parent="#faq">
		<div class="card-body">
			{{ $slot }}
		</div>
	</div>
</div>