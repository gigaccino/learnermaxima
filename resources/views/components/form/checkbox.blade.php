<div class="form-group text-center">

	<input class="form-check-input" type="checkbox" value="true" id="{{ $name }}" name="{{ $name }}" {{ !isset($not_required) ? 'required' : '' }}>
	<label for="{{ $name }}" class="text-muted">{{ $slot }}</label>


	@if ($errors->has($name))
		<div class="text-danger">
			<strong>{{ $errors->first($name) }}</strong>
		</div>
	@endif

</div>
