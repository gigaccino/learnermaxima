<div class="form-group text-center">

	<label for="{{ $name }}" class="text-muted">{{ $slot }}</label>

	<select id="{{ $name }}" class="w-100 custom-select" name={{ $name }}>
	  <option value="" {{ (!isset($value) ? 'selected' : '') }}>Select {{ $slot }}</option>
	  @foreach ($list as $item)
		<option value="{{ $item }}" {{ ( isset($value) ? (($value === $item) ? 'selected' : '') : '' ) }} >{{ $item }}</option>
	  @endforeach
	</select>

	@if ($errors->has($name))
		<span class="text-danger">
			<strong>{{ $errors->first($name) }}</strong>
		</span>
	@endif
	
</div>
