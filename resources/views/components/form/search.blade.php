<div id="search-{{ $type }}">
    <form method="GET" action="{{ route('lesson.index') }}">
        <div class="search form-group row">
            <label for="search-field-{{ $type }}" class="sr-only">Search</label>
            <div class="input-group">
                <input id="search-field-{{ $type }}" type="search" class="form-control border-primary" placeholder="What would you like to learn?" name="search_query">
                <button class="btn-search btn btn-primary text-white">Search</button>
            </div>
        </div>
    </form>
</div>
