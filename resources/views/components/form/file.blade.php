<div class="form-group text-center">

	<label for="{{ $name }}" class="text-muted">{{ $slot }}</label>
	<input id={{ $name }} type="file" name="{{ $name }}" class="form-control-file" {{ !isset($not_required) ? 'required' : '' }}>

	@if ($errors->has($name))
		<span class="text-danger">
			<strong>{{ $errors->first($name) }}</strong>
		</span>
	@endif

</div>
