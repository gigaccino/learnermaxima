<div class="form-group text-center">

	<label for="{{ $name }}" class="text-muted">{{ $slot }}</label>

	<input id="{{ $name }}" class="flatpickr-input form-control{{ $errors->has($name) ? ' is-invalid' : '' }}" placeholder="Enter {{ $slot }}" name="{{ $name }}" {{ !isset($not_required) ? 'required' : '' }}>
	
	@if ($errors->has($name))
		<span class="invalid-feedback">
			<strong>{{ $errors->first($name) }}</strong>
		</span>
	@endif
	
</div>
