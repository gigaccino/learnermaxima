<div class="form-group text-center">

	<label for="{{ $name }}" class="{{ (isset($highlight)) ? ( ($highlight === true) ? ('text-secondary') : 'text-muted' ) : 'text-muted' }}">{{ $slot }}</label>

	<textarea id="{{ $name }}" 
	style="min-height: {{ ($max_characters < 280) ? 100 : 200 }}px" class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }} {{ (isset($highlight)) ? ( ($highlight === true) ? ('border-secondary') : '' ) : '' }}" placeholder="Enter {{ $slot }} (Max Characters: {{ $max_characters }})" name="{{ $name }}" maxlength="{{$max_characters}}" {{ !isset($not_required) ? 'required' : '' }}>{{ isset($value) ? $value : '' }}</textarea>

	<p id="{{ $name }}-textarea-counter" class="text-muted w-100 text-right" max-characters="{{ $max_characters }}">Characters Left: {{ $max_characters - strlen($value) }}</p>

	@if ($errors->has($name))
		<span class="invalid-feedback">
			<strong>{{ $errors->first($name) }}</strong>
		</span>
	@endif
	
</div>
