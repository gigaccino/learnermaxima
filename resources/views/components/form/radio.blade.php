<div class="form-group text-center">

	<label for="{{ $name }}" class="text-muted mr-2">{{ $slot }}:</label>

		@foreach($option_list_value as $index=>$option_value)
			<div id="{{ $name }}-{{ $index + 1 }}" class="form-check {{ isset($in_line) ? 'form-check-inline' : '' }}">
				<label class="form-check-label">
					<input class="form-check-input" 
						type="radio" 
						name="{{ $name }}" 
						id="{{ 'option_'.$option_value }}" 
						value="{{ $option_value }}" 
						{{ isset($preselected_value) ? (($option_value === $preselected_value) ? 'checked' : '') : '' }}>

					{{ $option_list_text[$index] }}

				</label>
			</div>
		@endforeach


	@if ($errors->has($name))
		<div class="text-danger">
			<strong>{{ $errors->first($name) }}</strong>
		</div>
	@endif

</div>