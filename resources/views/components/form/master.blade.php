<form id="{{ str_replace(' ', '-', strtolower($form_action)) }}" method="POST" action="{{ $url }}" enctype= {{ isset($enctype) ? $enctype : "application/x-www-form-urlencoded" }} >

@if (isset($csrf_token))
	<input type="hidden" name="_token" value="{{ $csrf_token }}">
@endif

{{ $slot }}

@if ($method !== 'POST')
	<input type="hidden" name="_method" value="{{ $method }}">
@endif

<div class="form-group {{ !isset($align_btn_left) ? 'text-center' : '' }}">

@if (empty($hide_submit))
	@if (isset($btn_class))
	  <button type="submit" class="btn btn-secondary text-white {{ $btn_class }}" disabled="true">
	    {{ $form_action }}
	  </button>
	@else
	  <button type="submit" class="btn btn-secondary text-white" disabled="true">
	    {{ $form_action }}
	  </button>
	@endif
@endif

@if (isset($link))
	{{ $link }}
@endif
    
</div>

</form>
