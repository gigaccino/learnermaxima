<div class="form-group text-center">

	<label for="{{ $name }}" class="text-muted">{{ $slot }}</label>

	<input id="{{ $name }}" type="{{ $type }}" class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}" placeholder="Enter {{ $slot }}" name="{{ $name }}" maxlength="{{ $type === 'email' ? '50' : '30'}}" value="{{ isset($value) ? $value : '' }}" {{ !isset($not_required) ? 'required' : '' }} {{ isset($is_disabled) ? "disabled" : ""}}>
	
	@if ($errors->has($name))
		<span class="invalid-feedback">
			<strong>{{ $errors->first($name) }}</strong>
		</span>
	@endif
	
</div>
