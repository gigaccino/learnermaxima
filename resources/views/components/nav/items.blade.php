@if (Auth::guest())

    <li class="nav-item text-center">
        <a class="nav-link text-primary" href="{{ route('login') }}{{ request()->redirect_to ? '?redirect_to='.request()->redirect_to : ''}}">Log In</a>
    </li>

    <li class="nav-item text-center">
        <a class="nav-link text-primary" href="{{ route('register') }}{{ request()->redirect_to ? '?redirect_to='.request()->redirect_to : ''}}">Sign Up</a>
    </li>

@else

    <li class="nav-item text-center">
        <div class="d-block p-2 pr-0 pl-0">
            Hi {{ auth()->user()->name }}
        </div>
    </li>

    @can ('admin', \App\User::class)

        <li class="nav-item text-center">
            <a class="nav-link text-primary" href="{{ route('admin.home') }}">
                Admin Panel
            </a>
        </li>
        
    @endcan

    <li class="nav-item text-center">
        <div class="d-block p-2 pr-0 pl-0">
            <a class="text-primary" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                Logout
            </a>
        </div>
    </li>

@endif
