<div class="card border-lightgray mb-3">
	<div class="row no-gutters">
		<div class="container-card-number col-sm-2">
			<div class="card-body bg-darkgray h-100">
				<div class="text-center d-flex h-100">
					<div class="align-self-center m-auto">
						<span class="text-primary lead card-number">
							<small>{{ $index + 1 }}</small>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-10">
			<div class="card-body bg-lightgray h-100">
				<div class="row mb-1">
					<div class="col-10">
						<small class="card-text text-uppercase">Assignment {{$index + 1}}</small>
					</div>
					<div class="col-2">
						<div class="card-text float-right">
							@can ('update', [\App\Assignment::class, $assignment])
								<a href="{{ route('assignment.edit', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number, 'assignment_number' => $index + 1 ]) }}" class="ion-gear-b text-secondary"></a>
							@endcan
						</div>
					</div>
				</div>
				@can ('view', [\App\Assignment::class, $assignment])
					<h3>{{ $assignment->title }}</h3>
				@endcan
				@cannot ('view', [\App\Assignment::class, $assignment])
					<h3>{{ $assignment->title }}</h3>
				@endcannot
				<p class="card-text">{{ $assignment->instructions }}</p>
				@if (isset($assignment->questions) || isset($assignment->solutions))
					<hr>
					<div class="row">
						@if (isset($assignment->questions))
							<div class="col-6">
								<p class="card-text">Questions: <a href="{{ $assignment->questions_download_path }}" class="text-uppercase font-weight-light text-secondary"> <i class="ion-ios-cloud-download"> </i>Download</a></p>
							</div>
						@endif
						@if (isset($assignment->solutions) && $assignment->is_solutions_released)
							<div class="col-6">
								<p class="card-text">Solutions: <a href="{{ $assignment->solutions_download_path }}" class="text-uppercase font-weight-light text-secondary"> <i class="ion-ios-cloud-download"> </i>Download</a></p>
							</div>
						@elseif (!$assignment->is_solutions_released && isset($assignment->solutions_release_date))
							<div class="col-6">
								<p class="card-text">Solutions will be released {{ Carbon\Carbon::parse($assignment->solutions_release_date)->diffForHumans() }}.</p>
							</div>
						@elseif (isset($assignment->solutions_release_days) && (!$assignment->classroom->isPublished()))
							<div class="col-6">
								@if ($assignment->solutions_release_days > 0)
								    <p class="card-text">Solutions will be released {{ $assignment->solutions_release_days }} days after you publish this Classroom.</p>
								@else
								    <p class="card-text">Solutions will be released immediately after you publish this Classroom.</p>
								@endif
							</div>
					    @endif
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
