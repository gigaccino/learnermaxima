<div class="mb-1">
	@if ($discussion->user->isOwner($lesson))
		<i class="ion-ribbon-b text-secondary" title="Teacher"></i>
	@endif
	<a href="{{ route('user.show', ['user' => $discussion->user->pretty_id]) }}">
	    {{ $discussion->user->name }}
	</a>
	<span class="text-muted">
		•
		<small>{{ $discussion->created_at->diffForHumans() }}</small>
	</span>
	@can('update', [\App\Discussion::class, $discussion])
		<a href="{{ route('discussion.edit', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number, 'discussion_number' => $discussion->number]) }}" class="ion-gear-b text-secondary"></a>
	@endcan
</div>
<p class="mb-1">{{ $discussion->body }}</p>
