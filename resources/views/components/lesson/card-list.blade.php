<div class="row">
	@if ($lessons->count() === 0)
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-8">
					<img src="https://learnermaxima.nyc3.digitaloceanspaces.com/assets/pages/lessons/no-results.png" alt="No Results" class="img-fluid">
				</div>
			</div>
		</div>
	@endif
	@foreach ($lessons as $lesson)
		
		<div class="col-12 col-md-6">

			@component ('components.lesson.card', ['lesson' => $lesson, 'lesson_url' => route('lesson.show', ['lesson' => $lesson->pretty_id])])
			@endcomponent

		</div>

	@endforeach
</div>
