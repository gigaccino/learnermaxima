@for ($star = 0; $star < $rating; $star++)

	<i class="ion-android-star text-star" aria-hidden="true"></i>

@endfor

@for ($empty_star = $rating; $empty_star < resolve('\App\Defaults\Review')->getMax(); $empty_star++)
	
	<i class="ion-android-star-outline text-star" aria-hidden="true"></i>

@endfor
