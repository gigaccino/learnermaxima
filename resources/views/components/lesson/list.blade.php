<div class="list-lesson">

	@component ('components.banner.small')
		<h2>{{ $title }}</h2>
		@if (isset($subtitle))
			<small>{{ $subtitle }}</small>
		@endif
	@endcomponent

	{{ $slot }}

</div>
