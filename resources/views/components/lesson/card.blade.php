<div class="card m-4">
    
    <a href="{{ $lesson_url }}" class="lesson-card-link">
        <img class="card-img-top" src="{{ $lesson->photo_url }}" alt="{{ $lesson->level }} {{ $lesson->subject }} Lessons Photo">
    </a>

    <div class="card-body">
    
        @if (isset($days_remaining) || isset($unwatched_classrooms))
            <div class="row">
                <div class="col-6">
                    @if (isset($days_remaining))
                        @if ($days_remaining === 0)
                            <div class="badge badge-danger text-uppercase text-white">
                                Expires Today
                            </div>
                        @else
                            <div class="badge badge-secondary text-uppercase text-white">
                                {{ $days_remaining }} Days Remaining
                            </div>
                        @endif
                    @endif
                </div>
                <div class="col-6">
                    @if (isset($unwatched_classrooms))
                        @if($unwatched_classrooms > 0)
                            <div class="card-text small mb-2 badge badge-danger float-right">{{ $unwatched_classrooms }} New</div>
                        @endif
                    @endif
                </div>
            </div>
        @endif

        @if (!Auth::guest() && auth()->user()->isAdmin())
            <div class="row">
                <div class="col-6">
                    @if ($lesson->isSubscriptionsDisabled())
                        <div class="card-text mb-2 badge badge-danger text-white text-uppercase">Lesson Disabled</div>
                    @else
                        <div class="card-text mb-2 badge badge-secondary text-white text-uppercase">Lesson Enabled</div>
                    @endif
                </div>
                <div class="col-6">
                    <div class="card-text mb-2 small text-danger text-uppercase float-right">
                        @can ('enableSubscriptions', [\App\Lesson::class, $lesson])
                            <form action="{{ route('lesson.enableSubscriptions', ['lesson' => $lesson->id]) }}" method="POST">
                                {{ csrf_field() }}
                                <input type="submit" value="Enable" class="text-uppercase btn btn-link m-0 p-0">
                            </form>
                        @endcan
                        @can ('disableSubscriptions', [\App\Lesson::class, $lesson])
                            <form action="{{ route('lesson.disableSubscriptions', ['lesson' => $lesson->id]) }}" method="POST">
                                {{ csrf_field() }}
                                <input type="submit" value="Disable" class="text-uppercase btn btn-link m-0 p-0">
                            </form>
                        @endcan
                    </div>
                </div>
            </div>
        @endif

        <small class="card-text text-muted text-uppercase">
            Last Updated: {{ Carbon\Carbon::parse($lesson->last_publish_date)->diffForHumans() }}
        </small>

        <h2 class="card-title">
            <a href="{{ $lesson_url }}" class="lesson-card-link">
                {{ $lesson->subject }}
            </a>
        </h2>

        <p class="card-text">
            {{ str_limit($lesson->description, 100) }}
        </p>

    </div>

    <ul class="list-group list-group-flush">

        <li class="list-group-item">
            <a href="{{ route('lesson.index') }}?sort=newest-to-oldest&level={{ urlencode($lesson->level) }}&subject=all-subjects">{{ $lesson->level }}</a>
        </li>
        
        <li class="list-group-item">
            <a href="{{ route('user.show', ['user' => $lesson->user->pretty_id]) }}" class="lesson-card-link">
                @if ($lesson->user->isRegisteredTeacher())
                    <i class="ion-ribbon-b" title="Teacher"></i> Teacher:
                @elseif ($lesson->user->isTeacher())
                    <i class="ion-ribbon-a" title="Tutor"></i> Tutor:
                @endif
                {{ $lesson->user->name }}
            </a>
        </li>

        <li class="list-group-item">
            @component ('components.lesson.rating', ['rating' => $lesson->rating])
            @endcomponent
        </li>

    </ul>

</div>
