<div class="card border-lightgray mb-3">
	<div class="row no-gutters">
		<div class="container-card-number col-sm-2">
			@if ($classroom->progress_completed)
				<div class="card-body bg-primary h-100">
					<div class="text-center d-flex h-100">
						<div class="align-self-center m-auto">
							<span class="lead">
								<span class="text-white text-uppercase card-number"><i class="ion-ios-checkmark-outline"></i></span>
							</span>
						</div>
					</div>
				</div>
			@else
				<div class="card-body bg-darkgray h-100">
					<div class="text-center d-flex h-100">
						<div class="align-self-center m-auto">
							<span class="text-primary lead card-number">
								<small>{{ $index + 1 }}</small>
							</span>
						</div>
					</div>
				</div>
			@endif
		</div>
		<div class="col-sm-10">
			<div class="card-body bg-lightgray h-100">
				<div class="row mb-1">
					<div class="col-10">
						<small class="card-text text-uppercase">Classroom {{ $index + 1 }}</small>
						@if ($classroom->isPublished())
							<small> • </small>
							<small class="card-text text-uppercase text-secondary">Published {{ Carbon\Carbon::parse($classroom->publish_date)->diffForHumans() }}</small>
						@else
							<small> • </small>
							<small class="card-text text-uppercase text-danger">Unpublished</small>
						@endif
					</div>
					<div class="col-2">
						<div class="card-text float-right">
							@can ('update', [\App\Classroom::class, $classroom])
								<a href="{{ route('classroom.edit', ['lesson' => $classroom->lesson->pretty_id, 'classroom_number' => $index + 1 ]) }}" class="ion-gear-b text-secondary"></a>
							@endcan
						</div>
					</div>
				</div>
				@can ('view', [\App\Classroom::class, $classroom])
					<h3><a href="{{ route('classroom.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $index + 1]) }}">{{ $classroom->title }}</a></h3>
				@endcan
				@cannot ('view', [\App\Classroom::class, $classroom])
				<h3>{{ $classroom->title }}</h3>
				@endcannot
				<p class="card-text">{{ str_limit($classroom->objective, 100)}}</p>
				<div class="card-text row">
					<div class="row text-center w-100">
						@can ('index', [\App\Quiz::class, $classroom])
							<div class="col-md-3"><a href="{{ route('quiz.index', ['lesson' => $lesson->pretty_id, 'classroom_number' => $index + 1]) }}" class="text-secondary"><i class="ion-flash"></i> Quizzes: {{ $classroom->quizzes->count() }}</a></div>
						@endcan
						@can ('index', [\App\Assignment::class, $classroom])
						<div class="col-md-3"><a href="{{ route('assignment.index', ['lesson' => $lesson->pretty_id, 'classroom_number' => $index + 1]) }}" class="text-secondary"><i class="ion-link"></i> Assignments: {{ $classroom->assignments->count() }}</a></div>
						@endcan
						@can ('index', [\App\Discussion::class, $classroom])
							<div class="col-md-3"><a href="{{ route('classroom.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $index + 1]) }}" class="text-secondary"><i class="ion-chatbubbles"></i> Discussions: {{ $classroom->discussions->count() }}</a></div>
						@endcan
						@can ('view', [\App\Classroom::class, $classroom])
							@if (isset($classroom->attachment))
								<div class="col-md-3 mb-3 text-nowrap"><a href="{{ $classroom->attachment_download_path }}" class="text-secondary"> <i class="ion-ios-cloud-download"> </i> Notes</a></div>
							@endif
						@endcan
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
