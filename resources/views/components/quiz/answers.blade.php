<ul class="list-unstyled text-center">
	@foreach (['a', 'b', 'c', 'd'] as $option)
		<li class="
			{{ ($question->correct_answer === $option) ? 'text-secondary ' : '' }} 

			{{ (($question->correct_answer !== $option) && (auth()->user()->getAnswerValue($question) === $option)) ? 'text-danger ' : '' }}

			pb-2">

			@if (auth()->user()->getAnswerValue($question) === $option)
				<i class="ion-ios-circle-filled pr-1"></i>
			@else
				<span class="text-uppercase">
					{{ $option }}. 
				</span>
			@endif

			{{ $question['answer_'.$option] }}
		</li>
	@endforeach
</ul>