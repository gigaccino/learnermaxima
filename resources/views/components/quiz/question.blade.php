<p class="lead text-uppercase">
	Question {{ $question_number }}
	@can ('update', [\App\Question::class, $question])
		<a href={{ route('question.edit', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number, 'quiz_number' => $quiz_number, 'question_number' => $question_number]) }} class="ion-gear-b text-secondary"></a>
	@endcan
</p>

@if (isset($question->photo))
	<img class="card-img-top rounded-0 mb-3" src="{{ $question->photo_url }}" alt="Question Photo">
@endif

<p>{{ $question->question }}</p>

{{ $slot }}
