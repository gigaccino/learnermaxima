<div class="card border-lightgray mb-3">
	<div class="row no-gutters">
		<div class="container-card-number col-sm-2">
			@if ($quiz->completed || Gate::denies('create', [\App\Answer::class, $quiz]))
				<div class="card-body bg-primary h-100">
					<div class="text-center d-flex h-100">
						<div class="align-self-center m-auto">
							<span class="lead">
								<span class="text-white text-uppercase card-number"><i class="ion-ios-checkmark-outline"></i></span>
							</span>
						</div>
					</div>
				</div>
			@else
				<div class="card-body bg-darkgray h-100">
					<div class="text-center d-flex h-100">
						<div class="align-self-center m-auto">
							<span class="text-primary lead card-number">
								<small>{{ $index + 1 }}</small>
							</span>
						</div>
					</div>
				</div>
			@endif
		</div>
		<div class="col-sm-10">
			<div class="card-body bg-lightgray h-100">
				<div class="row mb-1">
					<div class="col-10">
						<small class="card-text text-uppercase">Quiz {{$index + 1}}</small>
					</div>
					<div class="col-2">
						<div class="card-text float-right">
							@can ('update', [\App\Quiz::class, $quiz])
								<a href="{{ route('quiz.edit', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number, 'quiz_number' => $index + 1 ]) }}" class="ion-gear-b text-secondary"></a>
							@endcan
						</div>
					</div>
				</div>
				@can ('view', [\App\Quiz::class, $quiz])
					<h3>
						<a href="{{ route('quiz.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number, 'quiz_number' => $index + 1 ]) }}"">
								{{ $quiz->title }}
						</a>
					</h3>
				@endcan
				@cannot ('view', [\App\Quiz::class, $quiz])
					<h3>{{ $quiz->title }}</h3>
				@endcannot
				<p class="card-text text-muted">{{ $quiz->questions->count() }} {{ ($quiz->questions->count() === 1) ? 'Question' : 'Questions' }}
					<small>(
						@if (!$quiz->completed)
							{{ $quiz->answered_count }} Answered
						@else
							Results: {{ $quiz->score }} out of {{ $quiz->questions->count() }} Correct
						@endif
					)</small>
				</p>
			</div>
		</div>
	</div>
</div>
