@extends ('layouts.form')

@section ('title', 'Profile Management')

@section ('subtitle')
    <a href="{{ route('user.edit', ['user' => $user->pretty_id]) }}">Manage Account Instead</a>
@endsection

@section ('form')

	<div class="row text-muted justify-content-center mb-3">
	    <a href="{{ route('user.show', ['user' => $user->pretty_id]) }}" class="btn btn-secondary text-white">View Profile</a>
	</div>

	<div class="row text-muted justify-content-center">
	    <p class="text-center">User Photo</p>
	</div>

	<div class="row mb-3">
	    <div class="col-sm-6">
	        <img src="{{ $user->photo_url }}" alt="{{ $user->name }}'s Profile Photo" class="img-fluid border">
	    </div>
	    <div class="col-sm-6 text-center pt-2 m-auto">
	        @can ('uploadPhoto', [\App\User::class, $user->id])
	            @include ('user.upload-photo')
	        @endcan

	        @can ('deletePhoto', [\App\User::class, $user->id])
	            @include ('user.replace-photo')
	        @endcan
	    </div>
	</div>


	@can ('deletePhoto', [\App\User::class, $user->id])
	    <div class="row mb-3 justify-content-center">
	        @include ('user.delete-photo')
	    </div>
	@endcan

    @component ('components.form.master', ['method' => 'PATCH', 'form_action' => 'Update Profile','url' => route('user.updateProfile', ['user' => $user->id]), 'csrf_token' => csrf_token()])

    	@component ('components.form.input', ['name'=> 'public_contact_email', 'type'=> 'text', 'errors' => $errors, 'value'=> $user->public_contact_email, 'not_required' => true])
    	    Public Contact Email (Visible to all students and teachers)
    	@endcomponent

        @component ('components.form.input', ['name'=> 'tagline', 'type'=> 'text', 'errors' => $errors, 'value'=> $user->tagline, 'not_required' => true])
            Tagline (Shown on your profile)
        @endcomponent

        @if ($user->isTeacher() || $user->isAdmin())
        	<hr>
			@component ('components.form.textarea', ['name'=> 'teaching_experience', 'errors' => $errors, 'max_characters' => \App\User::getMaxTeachingExperienceLength(), 'value'=> $user->teaching_experience, 'not_required' => true])
			    Your Teaching Experience (Public - Visible to potential students)
			@endcomponent
        @endif

    @endcomponent

@endsection
