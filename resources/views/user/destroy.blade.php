@component ('components.form.master', ['method' => 'DELETE', 'form_action' => 'Delete User', 'btn_class' => 'btn-danger', 'url' => route('user.destroy', ['user' => $user->id]), 'csrf_token' => csrf_token()])
@endcomponent
