@extends ('layouts.form')

@section ('title', 'Pay Off Tutor/Teacher')

@section ('subtitle')
    <a href="{{ route('user.edit', ['user' => $user->pretty_id]) }}">{{ $user->email }}</a>
@endsection

@section ('form')

	<h3>{{ $user->name }}</h3>
	<p class="lead">{{ $user->phone }}</p>
	<div class="mb-2 text-center">
		@component('components.user.income', ['user' => $user])
		@endcomponent
		@component('components.user.payout', ['user' => $user])
		@endcomponent
	</div>

	<div class="text-center">
		<p class="lead">Please do a payment to the above details. Once completed, click Pay Off to update the system and inform the user.</p>
		@component ('components.form.master', ['method' => 'POST', 'form_action' => 'Pay Off','url' => route('user.payOff', ['user' => $user->id]), 'csrf_token' => csrf_token()])

			<input type="hidden" name="payout_method" value="{{ $payout_method }}">
			
			@if ($payout_method === 'Cheque')
				@component ('components.form.input', ['name'=> 'cheque_number', 'type'=> 'text', 'errors' => $errors ])
				    Cheque Number
				@endcomponent
				<input type="hidden" name="address" value="{{ $address }}">
			@elseif ($payout_method === 'Bank Transfer')
				<input type="hidden" name="bank" value="{{ $bank }}">
				<input type="hidden" name="bank_account_number" value="{{ $bank_account_number }}">
			@endif

			<input type="hidden" name="subscriptions_count" value="{{ $user->teacher_subscriptions->count() }}">

		@endcomponent
	</div>

@endsection
