@extends ('layouts.form')

@section ('title', 'Payout Details Management')

@section ('subtitle')
    <a href="{{ route('user.edit', ['user' => $user->pretty_id]) }}">{{ $user->email }}</a>
@endsection

@section ('form')

    <div class="text-center">
        @include ('flash::message')
    </div>

    @component ('components.form.master', ['method' => 'PATCH', 'form_action' => 'Update Payout Details','url' => route('user.updatePayoutDetails', ['user' => $user->id]), 'csrf_token' => csrf_token()])

        @component ('components.form.radio', ['name'=> 'payout_method', 'errors' => $errors, 'option_list_value' => $payout_method_list, 'option_list_text' => $payout_method_list, 'in_line' => true, 'preselected_value' => $user->payout_method])
            Payout Method
        @endcomponent

        <div id="update-payout-form-container" style="display: none">

            <div id="update-payout-form-container-bank-transfer">
                @component ('components.form.select', ['name'=> 'bank', 'errors' => $errors, 'value' => $user->bank, 'not_required' => true, 'list' => $bank_list])
                    Bank
                @endcomponent

                @component ('components.form.input', ['name'=> 'bank_account_number', 'type'=> 'text', 'errors' => $errors, 'value'=> $user->bank_account_number, 'not_required' => true ])
                    Account Number
                @endcomponent
            </div>

            <div id="update-payout-form-container-cheque">
                @component ('components.form.textarea', ['name'=> 'address', 'errors' => $errors, 'value'=> $user->address, 'not_required' => true, 'max_characters' => \App\User::getMaxAddressLength()])
                    Address
                @endcomponent
            </div>

        </div>
        
    @endcomponent

    @section ('scripts')
        <script src="{{ asset('js/form.js') }}"></script>
        <script src="{{ asset('js/payout.js') }}"></script>
    @endsection

@endsection