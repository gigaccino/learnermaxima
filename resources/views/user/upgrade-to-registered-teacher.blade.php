@extends ('layouts.form')

@section ('title', 'Upgrade to Teacher')

@section ('subtitle')
    <a href="{{ route('user.edit', ['user' => $user->pretty_id]) }}">{{ $user->email }}</a>
@endsection

@section ('form')

    @component ('components.form.master', ['method' => 'POST', 'form_action' => 'Upgrade to Teacher','url' => route('user.upgradeToRegisteredTeacher', ['user' => $user->id]), 'enctype' => 'multipart/form-data', 'csrf_token' => csrf_token()])

        <p class="text-center text-primary">Please re-upload a copy of a valid form of ID (National ID, Drivers Permit or Passport).</p>
        @component ('components.form.file', ['name'=> 'identification', 'errors' => $errors, 'not_required' => true])
            Identification (Private and Confidential)
        @endcomponent

        <hr>

        <p class="text-center text-primary">Please upload a copy of the Registration Document / Slip containing the Teacher's Number.</p>
        @component ('components.form.file', ['name'=> 'qualifications', 'errors' => $errors, 'not_required' => true])
            Qualifications (Private and Confidential)
        @endcomponent
        
    @endcomponent

@endsection
