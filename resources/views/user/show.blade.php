@extends('layouts.app')

@section('title')
	{{ $user->name }} 
@endsection

@section('subtitle')
	{{ $user->tagline }}
@endsection

@section('content')
	
	@component('components.banner.small')
	    <h2>
	        @yield ('title', 'User Profile')
	        @can ('update', [\App\User::class, $user->id])
	        	<a href="{{ route('user.editProfile', ['user' => $user->pretty_id]) }}" class="ion-gear-b text-secondary"></a>
	        @endcan
	    </h2>

	    <small>
	        @yield ('subtitle')
	    </small>
	@endcomponent

	<div class="container">
	    <div class="row justify-content-center">
	        <div class="col-lg-9">

				<div class="row justify-content-center mb-3">
					<div class="col-6 col-md-3">
						<img src="{{ $user->photo_url }}" alt="{{ $user->name }}'s Profile Photo" class="img-fluid border">
						<div class="text-center text-secondary">
							@if ($user->isRegisteredTeacher())
								<i class="ion-ribbon-b" title="Teacher"></i> Teacher
							@elseif ($user->isTeacher())
								<i class="ion-ribbon-a" title="Tutor"></i> Tutor
							@elseif ($user->isAdmin())
								<i class="ion-ios-color-wand" title="Admin"></i> Admin
							@endif
						</div>
						@if (isset($user->public_contact_email))
							<div class="text-center">
								<a href="mailto:{{ $user->public_contact_email }}">{{ $user->public_contact_email }}</a>
							</div>
						@endif
					</div>
				</div>

				@if ( (($user->isTeacher() || $user->isAdmin() || auth()->user()->isAdmin()) && isset($user->teaching_experience)) )

					<h3 class="text-center text-muted lead">Teaching Experience</h3>
					<p>{{ $user->teaching_experience }}</p>

				@endif

				<div class="list-lesson">
					
					@if ($user->isStudent())

						@if ($user->subscriptions->count() > 0)

							<h3 class="text-center text-muted lead">Lesson Subscriptions</h3>

							<div class="row">
								@foreach ($user->subscriptions as $subscription)

									@if ($subscription->isActive())

										<div class="col-12 col-md-6">

											@component ('components.lesson.card', ['lesson' => $subscription->lesson, 'lesson_url' => route('lesson.show', ['lesson' => $subscription->lesson->pretty_id]), 'days_remaining' => $subscription->days_remaining, 'unwatched_classrooms' => $subscription->lesson->unwatched_classrooms])
											@endcomponent

										</div>

									@endif

								@endforeach
							</div>

						@endif

					@elseif ($user->isTeacher() || $user->isAdmin())

						@if ($user->lessons->count() > 0)

							<h3 class="text-center text-muted lead">Lessons Taught</h3>

							<div class="row">
								@foreach ($user->lessons as $lesson)

								    <div class="col-12 col-md-6">

								        @component ('components.lesson.card', ['lesson' => $lesson, 'lesson_url' => route('lesson.show', ['lesson' => $lesson->pretty_id])])
								        @endcomponent

								    </div>

								@endforeach
							</div>

						@endif

					@endif				

				</div>
				
	        </div>
	    </div>
	</div>

@endsection