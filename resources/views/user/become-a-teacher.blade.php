@extends ('layouts.form')

@section ('title', 'Become A Tutor/Teacher')

@section ('subtitle')
    <a href="{{ route('user.edit', ['user' => $user->pretty_id]) }}">{{ $user->email }}</a>
@endsection

@section ('form')

	<p>Thank you for your interest in becoming a Tutor/Teacher!</p>
    <p>Please fill out the form below to submit your request.</p>
    <p>Becoming a Tutor/Teacher grants you the privilege to Create Lessons and Classrooms, Upload Videos and Add Quizzes (MCQ) and Assignments.You will gain an income based on the number of Subscriptions you generate for a given month.</p>
    <p>Any questions you may have on this can be found in the <a href="{{ route('page.faq') }}">FAQ section</a>.</p>
	
	<hr>

    @component ('components.form.master', ['method' => 'POST', 'form_action' => 'Become a Tutor/Teacher','url' => route('user.becomeATeacher', ['user' => $user->id]), 'enctype' => 'multipart/form-data', 'csrf_token' => csrf_token()])

        <p class="text-center text-primary">Please upload a copy of a valid form of ID (National ID, Drivers Permit or Passport).</p>
        @component ('components.form.file', ['name'=> 'identification', 'errors' => $errors, 'not_required' => true])
            Identification (Private and Confidential)
        @endcomponent

        <hr>

        <p class="text-center text-primary">Teachers, please upload a copy of your Registration Document/Slip containing your Teacher's Number. Tutors, please upload a copy of your Degree / CAPE / CSEC Certificate.</p>
        @component ('components.form.file', ['name'=> 'qualifications', 'errors' => $errors, 'not_required' => true])
            Qualifications (Private and Confidential)
        @endcomponent

        <hr>

        <p class="text-center text-primary m-0">Please separate by commas.</p>
        @component ('components.form.textarea', ['name'=> 'teaching_experience', 'errors' => $errors, 'max_characters' => \App\User::getMaxTeachingExperienceLength(), 'value'=> $user->teaching_experience, 'not_required' => true])
            Your Teaching Experience (Public - Visible to potential students)
        @endcomponent

        <p>By upgrading to a Tutor/Teacher, you the User will be agreeing to these <a href="{{ route('page.terms') }}">Terms and Conditions</a> whereby all Tutors/Teachers are required to publish at least <span class="text-secondary">one classroom video</span> with a duration of twenty (20) to forty (40) minutes <span class="text-secondary">weekly</span>, or <span class="text-primary">4 classroom videos per month</span> for each of your Lessons(Subject) taught.</p>
        
    @endcomponent

@endsection
