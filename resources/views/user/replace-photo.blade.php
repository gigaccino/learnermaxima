@component ('components.form.master',['method' => 'PUT', 'form_action' => 'Replace Photo','url' => route('user.replacePhoto', ['user' => $user->id]), 'enctype' => 'multipart/form-data', 'csrf_token' => csrf_token()])

    @component ('components.form.file', ['name'=> 'photo'])
        Photo
    @endcomponent

@endcomponent
