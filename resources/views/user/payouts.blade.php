@extends ('layouts.app')

@section ('title', 'User Payouts')

@section ('content')

  @component('components.banner.small')
      <h2>
          Payouts
      </h2>

      <small>
        <a href="{{ route('user.edit', ['user' => $user->pretty_id]) }}">{{ $user->email }}</a>
      </small>
  @endcomponent

  <div class="container">
    <div class="mb-3">
        <div class="pb-2 text-center">
          
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Payout ID</th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach ($user->payouts->sortByDesc('payout_date') as $payout)
                    <tr>
                        <td>{{ $payout->id }}</td>
                        <td>${{ $payout->amount }} TTD</td>
                        <td>{{ \Carbon\Carbon::parse($payout->payout_date)->toFormattedDateString() }}</td>
                        <td>{{ $payout->payout_description }}</td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>
  </div>

@endsection
