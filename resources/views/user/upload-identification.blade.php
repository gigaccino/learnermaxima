@component ('components.form.master', ['method' => 'POST', 'form_action' => 'Upload Identification','url' => route('user.storeIdentification', ['user' => $user->id]), 'enctype' => 'multipart/form-data', 'csrf_token' => csrf_token()])

    @component ('components.form.file', ['name'=> 'identification'])
        Identification
    @endcomponent

@endcomponent
