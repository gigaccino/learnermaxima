@extends ('layouts.form')

@section ('title', 'Account Management')

@section ('subtitle')
    <a href="{{ route('user.editProfile', ['user' => $user->pretty_id]) }}">Manage Profile Instead</a>
@endsection

@section ('form')

    @if ($user->hasCredit())
        <p class="lead text-center">Congratulations! You can subscribe (for FREE) to any Lesson for <span class="text-secondary">{{ $user->credit }}</span> days.</p>
        <hr>
    @endif

    <p class="lead text-center">You are currently a {{ $user->privilege_name }} at <span class="text-primary">Learner</span> <span class="text-secondary">Maxima</span></p>

    @can ('updatePayoutDetails', [\App\User::class, $user->id])
        @if ($user->isTeacher() || $user->isAdmin())
            <div class="text-center">
                @include ('flash::message')
            </div>
        @endif
    @endcan

    @can('becomeATeacher', [\App\User::class, $user->id])
        <div class="text-center mb-3">
            <a href="{{ route('user.showBecomeATeacher', ['user' => $user->pretty_id]) }}" class="btn btn-secondary text-white">Become a Tutor/Teacher</a>
        </div>
    @endcan

    @component ('components.form.master', ['method' => 'PATCH', 'form_action' => 'Update Account','url' => route('user.update', ['user' => $user->id]), 'csrf_token' => csrf_token()])

        @component ('components.form.input', ['name'=> 'name', 'type'=> 'text', 'errors' => $errors, 'value'=> $user->name])
            Full Name
        @endcomponent

        @component ('components.form.input', ['name'=> 'email', 'type'=> 'email', 'errors' => $errors, 'value'=> $user->email, 'is_disabled' => true])
            Email
        @endcomponent

        @component ('components.form.input', ['name'=> 'phone', 'type'=> 'tel', 'errors' => $errors, 'value'=> $user->phone])
            Phone
        @endcomponent

        <div class="row justify-content-center">
            <div class="col-8">
                <hr>
            </div>
        </div>

        <p class="text-center">Update Password (Leave empty to keep unchanged)</p>

        {{-- Password --}}
         @component ('components.form.input', ['name'=> 'current_password', 'type'=> 'password', 'errors' => $errors, 'not_required' => true])
            Old Password
        @endcomponent

         @component ('components.form.input', ['name'=> 'password', 'type'=> 'password', 'errors' => $errors, 'not_required' => true])
            New Password
        @endcomponent

         @component ('components.form.input', ['name'=> 'password_confirmation', 'type'=> 'password', 'errors' => $errors, 'not_required' => true])
            New Password Confirmation
        @endcomponent

	@endcomponent

    @can ('updatePayoutDetails', [\App\User::class, $user->id])
        @if ($user->isTeacher() || $user->isAdmin())
            <hr>
            <div class="mb-3">
                <div class="pb-2 text-center">
                    <h3>Earnings this month</h3>
                    @component('components.user.income', ['user' => $user])
                    @endcomponent
                    @can ('viewPayouts', [\App\User::class, $user->id])
                        <a href="{{ route('user.showPayouts', ['user' => $user->pretty_id]) }}" class="btn btn-secondary text-white">View Payouts</a>
                    @endcan
                </div>

                <div class="text-center pb-2">

                    @component('components.user.payout', ['user' => $user])
                    @endcomponent
                    
                    <a href="{{ route('user.editPayoutDetails', ['user' => $user->pretty_id]) }}" class="btn btn-secondary text-white">Update Payout Details</a>
                </div>

                <p>You will receive an Email when your Payout has been processed. It is automatically processed during the last week of every month. We'll contact you at your phone number above should there be any issues.</p>

                <p>Email us for all inquiries at: <a href="mailto:learnermaxima@gmail.com?Subject=Teacher%20Payouts%20Inquiry">learnermaxima@gmail.com</a></p>

                <hr>

            </div>
        @endif
    @endcan

    @if (auth()->user()->isAdmin() && ($user->isTeacher() || $user->isAdmin()))
        <h3 class="text-center">Identification (Admin Only)</h3>
        @can ('uploadIdentification', [\App\User::class, $user->id])
            <div class="row mb-3 justify-content-center">
                @include ('user.upload-identification')
            </div>
        @endcan
        @can ('deleteIdentification', [\App\User::class, $user->id])
            <div class="text-center mb-2">
                <a href="{{ $user->identification_url }}" class="btn btn-secondary text-white" target="_blank">View Identification</a>
            </div>
            <div class="row mb-3 justify-content-center">
                @include ('user.delete-identification')
            </div>
        @endcan
        <hr>
        <h3 class="text-center">Qualifications (Admin Only)</h3>
        @can ('uploadQualifications', [\App\User::class, $user->id])
            <div class="row mb-3 justify-content-center">
                @include ('user.upload-qualifications')
            </div>
        @endcan
        @can ('deleteQualifications', [\App\User::class, $user->id])
            <div class="text-center mb-2">
                <a href="{{ $user->qualifications_url }}" class="btn btn-secondary text-white" target="_blank">View Qualifications</a>
            </div>
            <div class="row mb-3 justify-content-center">
                @include ('user.delete-qualifications')
            </div>
        @endcan
    @endif
    @can ('delete', [\App\User::class, $user->id])
        <hr>
        <h3 class="text-center">Delete User (Admin Only)</h3>
        <div class="row mb-3 justify-content-center">
            @include ('user.destroy')
        </div>
    @endcan

@endsection
