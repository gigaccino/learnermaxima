@component ('components.form.master',['method' => 'POST', 'form_action' => 'Upload Qualifications','url' => route('user.storeQualifications', ['user' => $user->id]), 'enctype' => 'multipart/form-data', 'csrf_token' => csrf_token()])

    @component ('components.form.file', ['name'=> 'qualifications'])
        Qualifications
    @endcomponent

@endcomponent
