@extends ('layouts.app')

@section ('title', '404 Error')

@section ('content')
	<div class="container">
		<div class="row justify-content-center pt-5 pb-4">
			<div class="col-8">
				<img src="https://learnermaxima.nyc3.digitaloceanspaces.com/assets/pages/global/404.png" alt="404" class="img-fluid">
			</div>
		</div>
	</div>
@endsection
