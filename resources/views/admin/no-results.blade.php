@extends('layouts.admin')

@section('admin_content')

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-8">
        <img src="https://learnermaxima.nyc3.digitaloceanspaces.com/assets/pages/lessons/no-results.png" alt="No Results" class="img-fluid">
      </div>
    </div>
  </div>

@endsection
