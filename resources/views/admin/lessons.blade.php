@extends('layouts.admin')

@section('admin_content')

  <p class="lead">
    Showing {{ $show_description }}
  </p>

  <div class="mb-3">
    @if (isset(request()->show))
      <a href="{{ route('admin.lessons')}}" class="btn btn-secondary text-white mr-2">Show All</a>
    @else
      <a href="{{ route('admin.lessons', ['show' => 'inactive'])}}" class="btn btn-secondary text-white mr-2">Show Inactive</a>
      <a href="{{ route('admin.lessons', ['show' => 'disabled'])}}" class="btn btn-secondary text-white mr-2">Show Disabled</a>
    @endif
  </div>

  @if ($lessons->count() < 1)

    <div class="row justify-content-center">
      <div class="col-8">
        <img src="https://learnermaxima.nyc3.digitaloceanspaces.com/assets/pages/lessons/no-results.png" alt="No Results" class="img-fluid">
      </div>
    </div>

  @else

    <div class="table-responsive">
      <table class="table table-striped text-center">
        <thead>
          <tr>

            @foreach ($table_headings as $heading)

              <th class="text-capitalize lead">{{ $heading }}</th>

            @endforeach

            <th class="text-capitalize lead">action</th>
          </tr>
        </thead>
        <tbody>

          @foreach ($lessons as $lesson)

            <tr>

              @foreach ($table_headings as $heading)

                @if ($heading === 'lesson')
                  <td>
                    <a href="{{ route('lesson.show', ['lesson' => $lesson->pretty_id]) }}" class="btn" target="_blank">{{ $lesson->level }} {{ $lesson->subject }}</a>
                  </td>
                @elseif ($heading === 'teacher')
                  <td>
                    <a href="{{ route('user.edit', ['user' => $lesson->user->pretty_id]) }}" class="btn" target="_blank">
                      {{ $lesson->user->name }}
                    </a>
                    <div class="small">
                      ({{ $lesson->user->phone }})
                    </div>
                  </td>
                @elseif ($heading === 'last updated (trinidad time)')
                  <td>
                    {{ Carbon\Carbon::parse($lesson->last_publish_date)->diffForHumans() }}
                    <div class="small">
                      {{ Carbon\Carbon::parse($lesson->last_publish_date)->subHours(4)->toDayDateTimeString() }}
                    </div>
                  </td>
                @elseif ($heading === 'subscriptions')
                  <td>
                    {{ $lesson->active_subscriptions_count }}
                  </td>
                @elseif ($heading === 'status')
                  <td>
                    @if ($lesson->isSubscriptionsDisabled())
                      <span class="text-danger text-uppercase">Disabled</span>
                    @else
                      <span class="text-secondary text-uppercase">Enabled</span>
                    @endif
                  </td>
                @else
                  <td>{{ $lesson->{$heading} }}</td>
                @endif
                
              @endforeach

              <td>
                @can ('enableSubscriptions', [\App\Lesson::class, $lesson])
                    <form action="{{ route('lesson.enableSubscriptions', ['lesson' => $lesson->id]) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" value="Enable Lesson" class="btn btn-link m-0 p-0">
                    </form>
                @endcan
                @can ('disableSubscriptions', [\App\Lesson::class, $lesson])
                    <form action="{{ route('lesson.disableSubscriptions', ['lesson' => $lesson->id]) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" value="Disable Lesson" class="btn btn-link m-0 p-0">
                    </form>
                @endcan
              </td>

            </tr>

          @endforeach

        </tbody>
      </table>
    </div>

    @if (request()->show === null && Route::currentRouteName() === 'admin.lessons')
      <div class="row justify-content-center pt-2">

        {{ $lessons->links() }}

      </div>
    @endif

  @endif

@endsection
