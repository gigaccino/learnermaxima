@extends('layouts.admin')

@section('admin_content')

  <p class="lead">
    Showing {{ $show_description }}
  </p>

  <div class="mb-3">
    @if (Route::currentRouteName() === 'admin.users')
      @if (isset(request()->show))
        <a href="{{ route('admin.users')}}" class="btn btn-secondary text-white mr-2">Show All</a>
      @else
        <a href="{{ route('admin.users', ['show' => 'owed'])}}" class="btn btn-secondary text-white mr-2">Show Owed</a>
        <a href="{{ route('admin.users', ['show' => 'pending_upgrade'])}}" class="btn btn-secondary text-white mr-2">Show Pending Upgrades</a>
      @endif
    @endif
  </div>

  <div id="search-users" class="container-fluid">
      <form method="GET" action="{{ route('admin.find') }}">
          <div class="search form-group row">
              <label for="search-field-user" class="sr-only">Find User</label>
              <div class="input-group">
                  <input id="search-field-user" type="email" class="form-control border-primary" placeholder="Enter User's Email Address" name="search_query">
                  <input type="hidden" name="type" value="user">
                  <button class="btn-search btn btn-primary text-white">Find User</button>
              </div>
          </div>
      </form>
  </div>

  @if ($users->count() < 1)
  
    <div class="row justify-content-center">
      <div class="col-8">
        <img src="https://learnermaxima.nyc3.digitaloceanspaces.com/assets/pages/lessons/no-results.png" alt="No Results" class="img-fluid">
      </div>
    </div>
    
  @else

    <div class="table-responsive">
      <table class="table table-striped text-center">
        <thead>
          <tr>

            @foreach ($table_headings as $heading)

              <th class="text-capitalize lead">{{ $heading }}</th>

            @endforeach

            <th class="text-capitalize lead">action</th>
          </tr>
        </thead>
        <tbody>

          @foreach ($users as $user)

            <tr>

              @foreach ($table_headings as $heading)

                @if ($heading === 'privilege')
                  <td class="{{ ($user->isPendingUpgrade()) ? 'text-danger' : '' }}">
                    {{ $user->privilege_name }} 
                    @if ($user->isPendingUpgrade())
                      <small>(Pending Upgrade)</small>
                    @endif
                  </td>
                @elseif ($heading === 'identification')
                  <td>
                    @if (isset($user->identification_url))
                      <a href="{{ $user->identification_url }}" class="btn" target="_blank">ID</a>
                    @else
                      <p>N/A</p>
                    @endif
                  </td>
                @elseif ($heading === 'qualifications')
                  <td>
                    @if (isset($user->qualifications_url))
                      <a href="{{ $user->qualifications_url }}" class="btn" target="_blank">Qualification</a>
                    @else
                      <p>N/A</p>
                    @endif
                  </td>
                @elseif ($heading === 'amount owed')
                  <td>
                    @can ('payOff', [\App\User::class, $user->id])
                      <a href="{{ route('user.showPayOff', ['user' => $user->pretty_id]) }}" class="btn">${{ $user->amount_owed }} TTD</a>
                    @else
                      ${{ $user->amount_owed }} TTD
                    @endcan
                  </td>
                @elseif ($heading === 'name')
                  <td><a href="{{ route('user.edit', ['user' => $user->pretty_id]) }}" class="btn" target="_blank">{{ $user->name }}</a></td>
                @else
                  <td>{{ $user->{$heading} }}</td>
                @endif
                
              @endforeach

              <td>
                @can('upgradeToRegisteredTeacher', [\App\User::class, $user->id])
                    <div class="text-center mb-3">
                        <a href="{{ route('user.showUpgradeToRegisteredTeacher', ['user' => $user->pretty_id]) }}">Upgrade to Teacher</a>
                    </div>
                @endcan
                @can('upgrade', [\App\User::class, $user->id, 'Tutor'])
                  <form action="{{ route('user.upgrade', ['user' => $user->id]) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="new_privilege" value="Tutor">
                    <input type="submit" value="Upgrade to Tutor" class="btn btn-link">
                  </form>
                @endcan
                @can('upgrade', [\App\User::class, $user->id, 'Teacher'])
                  <form action="{{ route('user.upgrade', ['user' => $user->id]) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="new_privilege" value="Teacher">
                    <input type="submit" value="Upgrade to Teacher" class="btn btn-link">
                  </form>
                @endcan
                @can('cancelUpgrade', [\App\User::class, $user->id])
                  <form action="{{ route('user.cancelUpgrade', ['user' => $user->id]) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="submit" value="Cancel Upgrade" class="btn btn-link">
                  </form>
                @endcan
                @can('downgrade', [\App\User::class, $user->id])
                  <form action="{{ route('user.downgrade', ['user' => $user->id]) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="submit" value="Downgrade" class="btn btn-link">
                  </form>
                @endcan
                @can('gift', [\App\User::class, $user->id])
                  <form action="{{ route('user.gift', ['user' => $user->id]) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="submit" value="Gift {{ \App\User::GIFT_CREDIT }} days of Credit" class="btn btn-link">
                  </form>
                @endcan
              </td>

            </tr>

          @endforeach

        </tbody>
      </table>
    </div>

    @if (request()->show === null && Route::currentRouteName() === 'admin.users')
      <div class="row justify-content-center pt-2">

        {{ $users->links() }}

      </div>
    @endif

  @endif

@endsection
