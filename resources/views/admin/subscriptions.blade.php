@extends('layouts.admin')

@section('admin_content')

  <p class="lead">
    Showing {{ $show_description }}
  </p>

  <div id="search-users" class="container-fluid">
      <form method="GET" action="{{ route('admin.find') }}">
          <div class="search form-group row">
              <label for="search-field-user" class="sr-only">Find Subscription</label>
              <div class="input-group">
                  <input id="search-field-user" type="search" class="form-control border-primary" placeholder="Enter Order Number" name="search_query">
                  <input type="hidden" name="type" value="subscription">
                  <button class="btn-search btn btn-primary text-white">Find Subscription</button>
              </div>
          </div>
      </form>
  </div>

  @if ($subscriptions->count() < 1)
  
    <div class="row justify-content-center">
      <div class="col-8">
        <img src="https://learnermaxima.nyc3.digitaloceanspaces.com/assets/pages/lessons/no-results.png" alt="No Results" class="img-fluid">
      </div>
    </div>
    
  @else

    <div class="table-responsive">
      <table class="table table-striped text-center">
        <thead>
          <tr>

            @foreach ($table_headings as $heading)

              <th class="text-capitalize lead">{{ $heading }}</th>

            @endforeach

          </tr>
        </thead>
        <tbody>

          @foreach ($subscriptions as $subscription)

            <tr>

              @foreach ($table_headings as $heading)

                @if ($heading === 'student')
                  <td>
                    <a href="{{ route('user.edit', ['user' => $subscription->user->pretty_id]) }}">{{ $subscription->user->name }}</a>
                  </td>
                @elseif ($heading === 'lesson')
                  <td>
                    <a href="{{ route('lesson.show', ['lesson' => $subscription->lesson->pretty_id]) }}">{{ $subscription->lesson->level }} {{ $subscription->lesson->subject }}</a>
                  </td>
                @elseif ($heading === 'teacher')
                  <td>
                    <a href="{{ route('user.edit', ['user' => $subscription->lesson->user->pretty_id]) }}">{{ $subscription->lesson->user->name }}</a>
                  </td>
                @elseif ($heading === 'order number')
                  <td>
                    <a href="{{ route('subscription.show', ['order_number' => $subscription->order_number])}}">{{ $subscription->order_number }}</a>
                  </td>
                @elseif ($heading === 'price (TTD)')
                  <td>
                    {{ $subscription->price }}
                  </td>
                @elseif ($heading === 'transaction ID')
                  <td>
                    {{ $subscription->transaction_id }}
                  </td>
                @elseif ($heading === 'payment date (trinidad time)')
                  <td>
                    {{ Carbon\Carbon::parse($subscription->payment_date)->diffForHumans() }}
                    <div class="small">
                      {{ Carbon\Carbon::parse($subscription->payment_date)->subHours(4)->toDayDateTimeString() }}
                    </div>
                  </td>
                @elseif ($heading === 'subscription length (days)')
                  <td>
                    {{ $subscription->subscription_length }}
                  </td>
                @elseif ($heading === 'status')
                  <td class="text-uppercase">
                    {!! isset($subscription->refund_date) ? '<span class="text-warning">Refunded</span>' : '<span class="text-secondary">Completed</span>' !!}
                  </td>
                @elseif ($heading === 'transaction type')
                  <td>
                    <span class="text-secondary">{{ $subscription->transaction_type }}</span>
                  </td>
                @else
                  <td>{{ $subscription->{$heading} }}</td>
                @endif
                
              @endforeach

            </tr>

          @endforeach

        </tbody>
      </table>
    </div>

    @if (request()->show === null && Route::currentRouteName() === 'admin.subscriptions')
      <div class="row justify-content-center pt-2">

        {{ $subscriptions->links() }}

      </div>
    @endif

  @endif

@endsection
