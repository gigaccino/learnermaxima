<div class="list-classroom">
	@foreach ($lesson->classrooms as $index=>$classroom)
		@component ('components.classroom.card', ['lesson' => $lesson, 'classroom' => $classroom, 'index' => $index])
		@endcomponent
	@endforeach
</div>
