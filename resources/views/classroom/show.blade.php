@extends ('layouts.app')

@section ('title')
	{{ $classroom->title }} - {{ $lesson->level }} {{ $lesson->subject }}, taught by {{ $lesson->user->name }}
@endsection

@section ('content')
	@if (isset($classroom->video))
		<div class="row no-gutters justify-content-center banner mb-3">
			<div class="col-xl-10">
				<div class="container-video">
					{!! $classroom->getVideoEmbed() !!}
				</div>
			</div>
		</div>
	@else
		@component('components.banner.large')
			<h3 class="text-center">{{ $lesson->user->name }} hasn't uploaded a video to this Classroom as yet.</h3>
		@endcomponent
	@endif

	<div class="row no-gutters justify-content-center meta-header-classroom">
		<div class="col-10">
			<div class="text-muted text-uppercase mb-2">
				<div class="row">
					<div class="col-12 col-sm-6">
						Classroom {{ $classroom_number }} • <span id="video-duration"></span> • 
						@if ($classroom->isPublished())
							<span class="text-secondary">Published {{ Carbon\Carbon::parse($classroom->publish_date)->diffForHumans() }}</span>
						@else
							<span class="text-danger">Unpublished</span>
						@endif
					</div>
					@if (isset($next_classroom))
						<div class="col-12 col-sm-6 next-classroom">
							<a href="{{ route('classroom.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $next_classroom->number]) }}">Next Classroom ></a>
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>

	<div class="row no-gutters justify-content-center mb-3">
		<div class="col-10 col-lg-5">
			<h3><a href="{{ route('lesson.show', ['lesson' => $lesson->pretty_id]) }}">{{ $lesson->level }} {{ $lesson->subject }}</a></h3>
			<h3>
				{{ $classroom->title }}
				@can ('update', [\App\Classroom::class, $classroom])
					<a href="{{ route('classroom.edit', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number ]) }}" class="ion-gear-b text-secondary"></a>
				@endcan
			</h3>
			<p class="text-uppercase"><a href="{{ route('user.show', ['user' => $lesson->user->pretty_id])}}">{{ $lesson->user->name }}</a></p>
		</div>
		<div class="col-10 col-lg-5">
			<p class="text-muted">{{ $classroom->objective }}</p>
		</div>
	</div>

	<div class="row no-gutters justify-content-center text-center">
		<div class="col-sm-4 mb-3 text-nowrap"><a href="{{ route('quiz.index', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number]) }}" class="text-uppercase font-weight-light p-2 border border-secondary text-secondary"><i class="ion-flash"></i> Quizzes: {{ $classroom->quizzes->count() }}</a></div>
		<div class="col-sm-4 mb-3 text-nowrap"><a href="{{ route('assignment.index', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number]) }}" class="text-uppercase font-weight-light p-2 border border-secondary text-secondary"><i class="ion-link"></i> Assignments: {{ $classroom->assignments->count() }}</a></div>
		@if (isset($classroom->attachment))
			<div class="col-sm-4 mb-3 text-nowrap"><a href="{{ $classroom->attachment_download_path }}" class="text-uppercase font-weight-light p-2 border border-secondary text-secondary"> <i class="ion-ios-cloud-download"> </i> Notes</a></div>
		@endif
	</div>

	<div class="row no-gutters justify-content-center">
		<div class="col-10">
			@include('discussion.index')
		</div>
	</div>

@endsection

@section ('scripts')
	<script src="{{ asset('js/video.js') }}"></script>
@endsection
