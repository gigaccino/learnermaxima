@component ('components.form.master', ['method' => 'POST', 'form_action' => 'Upload Notes','url' => route('classroom.storeAttachment', ['classroom' => $classroom->id]), 'enctype' => 'multipart/form-data', 'csrf_token' => csrf_token()])
	
	@component ('components.form.file', ['name'=> 'attachment'])
	    Notes (optional) - Word Document, PDF, Powerpoint, Zip etc.
	@endcomponent

@endcomponent