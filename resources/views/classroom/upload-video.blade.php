@extends ('layouts.form')

@section ('title', 'Uploading Classroom Video')

@section ('subtitle')
	<a href="{{ route('classroom.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number]) }}">{{ $classroom->title }} - {{ $lesson->level }} {{ $lesson->subject }}</a>
@endsection

@section ('form')

    @component ('components.form.master',['method' => 'POST', 'form_action' => 'Upload Video','url' => $upload_link_secure, 'enctype' => 'multipart/form-data'])

        <input type="hidden" name="name" value="{{ $classroom->id }}">

        @component ('components.form.file', ['name'=> 'file_data'])
            Video File
        @endcomponent

    @endcomponent

@endsection
