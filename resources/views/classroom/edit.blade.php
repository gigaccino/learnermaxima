@extends ('layouts.form')

@section ('title', 'Updating your Classroom')

@section ('subtitle')
    <a href="{{ route('classroom.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number]) }}">{{ $classroom->title }}</a>
@endsection

@section ('form')

    @can ('publish', [\App\Classroom::class, $classroom])
        <p class="text-center lead">This Classroom is currently <span class="text-danger">Unpublished</span>.</p>
        <div class="row justify-content-center">
           @include ('classroom.publish')
        </div>
    @endcan

    @can ('unpublish', [\App\Classroom::class, $classroom])
        <p class="text-center lead">This Classroom is currently <span class="text-secondary">Published</span>.</p>
        <div class="row justify-content-center">
           @include ('classroom.unpublish')
        </div>
    @endcan

    @cannot ('publish', [\App\Classroom::class, $classroom])
        @cannot ('unpublish', [\App\Classroom::class, $classroom])
            <p class="text-center lead text-warning ">You cannot publish this Classroom until a video is uploaded (see below).</p>
        @endcannot
    @endcannot

    @component ('components.form.master', ['method' => 'PATCH', 'form_action' => 'Update Classroom','url' => route('classroom.update', ['classroom' => $classroom->id]), 'csrf_token' => csrf_token()])

        @component ('components.form.input', ['name'=> 'title', 'type'=> 'text', 'errors' => $errors, 'value'=> $classroom->title])
            Title
        @endcomponent

        @component ('components.form.textarea', ['name'=> 'objective', 'errors' => $errors, 'max_characters' => \App\Classroom::getMaxObjectiveLength(), 'value'=> $classroom->objective])
            Objective
        @endcomponent

    @endcomponent

    <p class="text-center mt-2 mb-0 text-muted">Notes</p>
    <hr class="mt-0">

    @can ('uploadAttachment', [\App\Classroom::class, $classroom])
        <div class="row justify-content-center">
            @include ('classroom.upload-attachment')
        </div>
    @endcan

    @can ('deleteAttachment', [\App\Classroom::class, $classroom])
        <div class="row justify-content-center">
            @include ('classroom.delete-attachment')
        </div>
    @endcan

    <p class="text-center mt-2 mb-0 text-muted">Classroom Video</p>
    <hr class="mt-0">

    @can ('uploadVideo', [\App\Classroom::class, $classroom])
        <div class="row mb-3 justify-content-center">
            <a href="{{ route('classroom.uploadVideo', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number]) }}" class="btn btn-secondary text-white">Upload Video</a>
        </div>
    @endcan

    @can ('deleteVideo', [\App\Classroom::class, $classroom])
        <div class="row justify-content-center">
            @include ('classroom.delete-video')
        </div>
    @endcan

    <hr class="mt-2">
    
    @can ('delete', [\App\Classroom::class, $classroom])
        <div class="row justify-content-center">
            @include ('classroom.destroy')
        </div>
    @endcan

@endsection
