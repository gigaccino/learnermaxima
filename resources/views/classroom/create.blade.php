@extends ('layouts.form')

@section ('title', 'Creating Classroom')

@section ('subtitle')
	<a href="{{ route('lesson.show', ['lesson' => $lesson->pretty_id]) }}">{{$lesson->level}} {{$lesson->subject}}</a>
@endsection

@section ('form')

    @component ('components.form.master', ['method' => 'POST', 'form_action' => 'Create Classroom','url' => route('classroom.store', ['lesson' => $lesson->id]), 'enctype' => 'multipart/form-data', 'csrf_token' => csrf_token()])

        @component ('components.form.input', ['name'=> 'title', 'type'=> 'text', 'errors' => $errors, 'value'=> old('title')])
            Title
        @endcomponent

        @component ('components.form.textarea', ['name'=> 'objective', 'errors' => $errors, 'max_characters' => \App\Classroom::getMaxObjectiveLength(), 'value'=> old('objective')])
            Objective
        @endcomponent

        @component ('components.form.file', ['name'=> 'attachment', 'errors' => $errors, 'not_required' => true])
            Notes (optional) - Word Document, PDF, Powerpoint, Zip etc.
        @endcomponent

    @endcomponent

@endsection
