@component ('components.form.master', ['method' => 'DELETE', 'form_action' => 'Delete Classroom', 'btn_class' => 'btn-danger float-right','url' => route('classroom.destroy', ['classroom' => $classroom->id]), 'csrf_token' => csrf_token()])
@endcomponent
