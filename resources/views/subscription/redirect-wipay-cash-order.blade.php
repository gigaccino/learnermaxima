@extends('layouts.form')

@section ('title', 'Redirecting')

@section ('subtitle', 'Please wait...')

@section ('form')

<div class="container">
	<img src="https://learnermaxima.nyc3.digitaloceanspaces.com/assets/pages/global/redirecting.png" alt="404" class="img-fluid">
</div>

<form id="wipay" action="{{ $wipay_url }}" method='post'> 
	<input type="hidden" name="total" value="{{ $wipay_data->get('total') }}"/>      
	<input type="hidden" name="order_id" value="{{ $wipay_data->get('order_id') }}"/> 
	<input type="hidden" name="developer_id" value="{{ $wipay_data->get('developer_id') }}"/>  
	@if ($transaction_type === 'RENEW')
		<input type="hidden" name="return_url" value="{{ $wipay_data->get('return_url_renew') }}"/>
	@else
		<input type="hidden" name="return_url" value="{{ $wipay_data->get('return_url') }}"/>
	@endif   
	<input type="hidden" name="cancel_url" value="{{ $wipay_data->get('cancel_url') }}"/>  
</form>

@endsection

@section ('scripts')
	<script src="{{ asset('js/form.js') }}"></script>
	<script>
	  fbq('track', 'Purchase');
	</script>
@endsection
