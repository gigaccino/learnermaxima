@extends ('layouts.app')

@section ('title', 'My Subscriptions')

@section ('content')

  @component ('components.lesson.list')

    @slot ('title', 'My Subscriptions')

    <div class="container-fluid">

       <div class="row justify-content-center">
           <div class="col-lg-9">
              <div class="row">

        				@if ($subscriptions_active->count() > 0)
                  @foreach ($subscriptions_active as $subscription)

                    <div class="col-12 col-md-6">

                      @component ('components.lesson.card', ['lesson' => $subscription->lesson, 'lesson_url' => route('lesson.show', ['lesson' => $subscription->lesson->pretty_id]), 'days_remaining' => $subscription->days_remaining, 'unwatched_classrooms' => $subscription->lesson->unwatched_classrooms])
                      @endcomponent

                    </div>

                  @endforeach
                @else
                  <div class="row justify-content-center">
                    <div class="col-10 col-md-8 p-2 mb-3">
                      <img src="https://learnermaxima.nyc3.digitaloceanspaces.com/assets/pages/lessons/no-subscriptions.png" alt="No Subscriptions" class="img-fluid">
                    </div>
                  </div>
                @endif

              </div>
           </div>
       </div>
      
      @if ($subscriptions_expired->count() > 0)
        <h3 class="text-center">Expired/Refunded Subscriptions</h3>
         <div class="row justify-content-center">
             <div class="col-lg-9">
                <div class="row">
                  <table class="table table-striped text-center">
                    <thead>
                      <tr>
                        <th class="text-capitalize lead">Lesson</th>
                        <th class="text-capitalize lead">Teacher</th>
                        <th class="text-capitalize lead">Order Number</th>
                        <th class="text-capitalize lead">Payment Date (Trinidad Time)</th>
                        <th class="text-capitalize lead">Subscription Length (days)</th>
                        <th class="text-capitalize lead">Transaction Type</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($subscriptions_expired as $subscription)
                      <tr>
                        <td><a href="{{ route('lesson.show', ['lesson' => $subscription->lesson->pretty_id])}}">{{ $subscription->lesson->level }} {{ $subscription->lesson->subject }}</a></td>
                        <td><a href="{{ route('user.show', ['user' => $subscription->lesson->user->pretty_id])}}">{{ $subscription->lesson->user->name }}</a></td>
                        <td><a href="{{ route('subscription.show', ['order_number' => $subscription->order_number])}}">{{ $subscription->order_number }}</a></td>
                        <td>
                          {{ Carbon\Carbon::parse($subscription->payment_date)->diffForHumans() }}
                          <div class="small">
                            {{ Carbon\Carbon::parse($subscription->payment_date)->subHours(4)->toDayDateTimeString() }}
                          </div>
                        </td>
                        <td>{{ $subscription->subscription_length }}</td>
                        <td><span class="text-secondary">{{ $subscription->transaction_type }}</span></td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
             </div>
         </div>
      @endif
       
    </div>

  @endcomponent
  
@endsection
