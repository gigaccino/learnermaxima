@extends ('layouts.form')

@section ('title', 'Subscribing to '.$lesson->level.' '.$lesson->subject)

@section ('subtitle', 'Congrats, this one\'s on us! Happy Learning!')

@section ('form')

	<div class="text-center">
		
		<h2><a href="{{ route('lesson.show', ['lesson' => $lesson->pretty_id]) }}">{{ $lesson->level }} {{ $lesson->subject }}</a></h2>
		<p class="text-uppercase font-weight-light">Teacher: <a href="{{ route('user.show', ['user' => $lesson->user->pretty_id]) }}">{{ $lesson->user->name }}</a></p>
		<h3 class="text-center">Account Credit: <span class="text-secondary">{{ auth()->user()->credit }}</span></h3>

		<hr>

		<div class="row justify-content-center mb-2">
			<p class="lead text-center">You will be subscribed to this Lesson for <span class="text-secondary">{{ auth()->user()->credit }}</span> days, at no cost to you.</p>
			<small class="text-center pb-2">By proceeding with this transaction your Account Credit will be depleted. You can renew this subscription by paying as usual in the future.</small>
			@component ('components.form.master', ['method' => 'POST', 'form_action' => 'Redeem Credits and Subscribe', 'url' => route('subscription.storeFree', ['lesson' => $lesson->id]), 'csrf_token' => csrf_token()])
			@endcomponent							
		</div>


	</div>
	
@endsection

@section ('scripts')
	<script src="{{ asset('js/form.js') }}"></script>
	<script>
	  fbq('track', 'Purchase');
	</script>
@endsection
