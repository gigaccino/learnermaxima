@extends ('layouts.app')

@section ('title', 'Subscription')

@section ('content')

 @component('components.banner.small')
      <h2>
          @yield ('title', 'FAQ')
      </h2>
      <small>
          Order Number: {{ $subscription->order_number }}
      </small>
  @endcomponent

  <div class="text-center mb-3">
    
    <p class="text-uppercase font-weight-light">Student: <a href="{{ route('user.show', ['lesson' => $subscription->user->pretty_id]) }}">{{ $subscription->user->name }}</a></p>
    <p class="text-uppercase font-weight-light">Lesson: <a href="{{ route('lesson.show', ['lesson' => $subscription->lesson->pretty_id]) }}">{{ $subscription->lesson->level }} {{ $subscription->lesson->subject }}</a></p>
    <p class="text-uppercase font-weight-light">Teacher: <a href="{{ route('user.show', ['user' => $subscription->lesson->user->pretty_id]) }}">{{ $subscription->lesson->user->name }}</a></p>
    <p class="text-uppercase font-weight-light">Order Number: {{ $subscription->order_number }}</p>
    <p class="text-uppercase font-weight-light">Transaction ID: {{ $subscription->transaction_id }}</p>
    <p class="text-uppercase font-weight-light">Price: ${{ $subscription->price }} TTD</p>
    <p class="text-uppercase font-weight-light">Payment Date (Trinidad Time): <span class="text-secondary font-weight-normal">{{ Carbon\Carbon::parse($subscription->payment_date)->subHours(4)->toDayDateTimeString() }}</span></p>
    <p class="text-uppercase font-weight-light">Subscription Length: <span class="text-secondary font-weight-normal">{{ $subscription->subscription_length }}</span> days</p>
    <p class="text-uppercase font-weight-light">Expiry Date (Trinidad Time): <span class="text-danger font-weight-normal">{{ Carbon\Carbon::parse($subscription->expiry_date)->subHours(4)->toDayDateTimeString() }}</span></p>
    <p class="text-uppercase font-weight-light">Transaction Type: <span class="text-secondary">{{ $subscription->transaction_type }}</span></p>
    @if(isset($subscription->payout) && (auth()->user()->isAdmin() || (auth()->user()->isTeacher() && auth()->user()->isOwner($subscription->lesson))))
      <p class="text-uppercase font-weight-light">Paid out: <span class="text-secondary">{{ Carbon\Carbon::parse($subscription->payout->payout_date)->subHours(4)->toDayDateTimeString() }}</span></p>
    @endif
    <p class="text-uppercase font-weight-light">Status: {!! isset($subscription->refund_date) ? '<span class="text-warning">Refunded</span>' : '<span class="text-secondary">Completed</span>' !!}</p>
    
  </div>

  <div class="text-center">
    @can('refund', [\App\Subscription::class, $subscription])
      <form action="{{ route('subscription.refund', ['subscription' => $subscription->id]) }}" method="POST">
        {{ csrf_field() }}
        <input type="submit" value="Refund" class="btn btn-secondary text-white">
      </form>
    @endcan
  </div>
  
@endsection
