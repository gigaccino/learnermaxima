@extends ('layouts.form')

@section ('title', 'Renewing Subscription to '.$lesson->level.' '.$lesson->subject)

@section ('subtitle', 'You will have continue to have access to this Lesson for the period of time you pay for.')

@section ('form')

	<div class="text-center">
		
		<h2><a href="{{ route('lesson.show', ['lesson' => $lesson->pretty_id]) }}">{{ $lesson->level }} {{ $lesson->subject }}</a></h2>
		<p class="text-uppercase font-weight-light">Teacher: <a href="{{ route('user.show', ['user' => $lesson->user->pretty_id]) }}">{{ $lesson->user->name }}</a></p>
		<h3 class="text-secondary">${{ $lesson->price }} TTD / month</h3>

		<hr>

		<h3>Renew Now</h3>

		@can ('createFree', [\App\Subscription::class, $lesson])
			<div class="row justify-content-center mb-2">
				<p class="lead text-center">Your account credit allows you to renew this Lesson for free for <span class="text-secondary">{{ auth()->user()->credit }}</span> days.</p>
				<a href="{{ route('subscription.createFree', ['lesson' => $lesson->pretty_id]) }}" class="btn btn-secondary text-white">Renew For Free</a>							
			</div>
			<div class="row lead justify-content-center">OR</div>
		@endcan
		
	</div>

	@component ('components.form.master', ['method' => 'GET', 'form_action' => 'Renew','url' => route('subscription.createRenewOrder', ['lesson' => $lesson->pretty_id]), 'csrf_token' => csrf_token()])

	    @component ('components.form.select', ['name'=> 'payment_method', 'errors' => $errors, 'list' => $payment_method_list])
	        Payment Method
	    @endcomponent

	    @component ('components.form.select', ['name'=> 'subscription_length', 'errors' => $errors, 'list' => $subscription_length_list])
	        Renewal Length
	    @endcomponent

	@endcomponent
	
@endsection

@section ('scripts')
	<script src="{{ asset('js/form.js') }}"></script>
	<script>
	  fbq('track', 'AddToCart');
	</script>
@endsection
