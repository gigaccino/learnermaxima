@component ('vendor.mail.markdown.message')
# Account Not Upgraded

Hi {{ $user->name }},

Unfortunately, at this time we are not able to upgrade your account to a Teachers account. 

Feel free to contact us to find out more if you're still interested in becoming a teacher.

You can try re-submitting your request, but please ensure that your Name, Email, Phone Number and Identification are all up to date and correct.

Thanks,

{{ config('app.name') }}
@endcomponent
