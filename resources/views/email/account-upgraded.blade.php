@component ('vendor.mail.markdown.message')
# Account Upgraded

Hi {{ $user->name }},

Congratulations! You're now a {{ $user->privilege_name }} at Learner Maxima.

You can now create Lessons and Classrooms, Upload Videos and Add Quizzes (MCQ) and Assignments.

As a Tutor/Teacher, you have agreed to publish at least one classroom video with a duration of twenty (20) to forty (40) minutes weekly, or 4 classroom videos per month for each of your Lessons(Subject) taught.

Your Classroom Videos can be recorded videos of yourself in front a whiteboard, a screencast or other creative forms of educational methods. Please ensure your videos are decent in quality. We'll be doing general Quality Control on the site to ensure this.

PLEASE VISIT YOUR MY ACCOUNT PAGE AND SET UP YOUR PAYOUT DETAILS AS SOON AS POSSIBLE.

Payment Scheme:
Subscriptions Cost for Students: $149.00/month per Lesson, per Subscripion
Our fees: $49.00/month per Lesson, per Subscripion
Payment Processor fees: $10.00/month per Lesson, per Subscripion
YOUR INCOME: $90.00/MONTH (PER LESSON, PER SUBSCRIPTION)

The more students that subscribe to you, the higher your income. We provide the platform, and you provide the educational content. Feel free to advertise your Lessons page on your Facebook profile to get more students subscribed to you. We'll be advertising the website on a whole to get as much students on board as possible.

Should you have any questions regarding your new privilege at Learner Maxima, please visit our FAQ.

You can also simply reply to this email (learnermaxima@gmail.com) for something that you can't find in our FAQ.

@component ('mail::button', ['url' => route('page.faq')])
FAQ
@endcomponent

@component ('mail::panel', ['url' => ''])
“A good teacher can inspire hope, ignite the imagination, and instill a love of learning.” - Brad Henry
@endcomponent

Happy Teaching,

{{ config('app.name') }}
@endcomponent
