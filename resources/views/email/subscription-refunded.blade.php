@component ('vendor.mail.markdown.message')

Hi {{ $subscription->user->name }},

Order Number: {{ $subscription->order_number }}

We've received your request for a refund and have successfully processed it.

You refund has been returned as credit in your Learner Maxima account. You can now Subscribe (free of charge) to any Lesson of your choice on the site. On the Subscribe page of a Lesson you'll see a "Subscribe for Free" option.

@component ('mail::panel', ['url' => ''])
	“Knowledge, like air, is vital to life. Like air, no one should be denied it.”  - Alan Moore
@endcomponent

Thanks,

{{ config('app.name') }}
@endcomponent
