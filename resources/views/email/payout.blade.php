@component ('vendor.mail.markdown.message')
# Payout Processed!

Hi {{ $user->name }},

We've processed your Payout for the amount of: ${{ $payout->amount }} TTD.

A Payout was initiated using the details in your account:

@if ($user->payout_method === 'Bank Transfer')
	Bank Transfer
	Bank: {{ $user->bank }}
	Account No.: {{ $user->bank_account_number }}
@elseif ($user->payout_method === 'Cheque')
	Cheque, mailed to:
	{{ $user->address }}
@endif

Until Next Month,

{{ config('app.name') }}
@endcomponent
