@component ('vendor.mail.markdown.message')
# Renewal Reminder

Hi {{ $subscription->user->name }},

It looks like your subscription to {{ $subscription->lesson->level }} {{ $subscription->lesson->subject }} requires renewal. If you'd like to continue receiving content for this Lesson, you may now renew your subscription. 

@component ('mail::button', ['url' => route('subscription.createRenew', ['lesson' => $subscription->lesson->pretty_id])])
Renew
@endcomponent

Happy Learning,

{{ config('app.name') }}
@endcomponent
