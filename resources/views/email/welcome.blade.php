@component ('vendor.mail.markdown.message')
# Welcome to Learner Maxima!

Hi {{ $user->name }},

You're account is all setup and you're ready to start learning. Simply find a lesson you're interested in, subscribe, and you're on your way to a brighter future!

@component ('mail::button', ['url' => config('app.url')])
Start Learning
@endcomponent

@component ('mail::panel', ['url' => ''])
“Knowledge, like air, is vital to life. Like air, no one should be denied it.”  - Alan Moore
@endcomponent

Thanks,

{{ config('app.name') }}
@endcomponent
