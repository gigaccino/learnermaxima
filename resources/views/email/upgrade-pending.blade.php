@component ('vendor.mail.markdown.message')
# Teacher Upgrade Pending

Hi {{ $user->name }},

Your request to have your account upgraded to a Tutor/Teacher's account is currently being processed. We'll get back to you in 1-2 business days.

@component ('mail::panel', ['url' => ''])
“A good teacher can inspire hope, ignite the imagination, and instill a love of learning.” - Brad Henry
@endcomponent

Thanks,

{{ config('app.name') }}
@endcomponent
