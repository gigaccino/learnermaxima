@component ('vendor.mail.markdown.message')
# Thanks for Subscribing!

Hi {{ $user->name }},

Order Number: {{ $order_number }}

Transaction Type: {{ $transaction_type }}

Number of Days: {{ $days_remaining }}

We've received your payment and you've successfully been subscribed to:

{{ $lesson->level }} {{ $lesson->subject }}

Teacher: {{ $lesson->user->name }}

@component ('mail::button', ['url' => route('lesson.show', ['lesson' => $lesson->pretty_id])])
	{{ $lesson->level }} {{ $lesson->subject }}
@endcomponent

@component ('mail::panel', ['url' => ''])
	“Knowledge, like air, is vital to life. Like air, no one should be denied it.”  - Alan Moore
@endcomponent

Thanks,

{{ config('app.name') }}
@endcomponent
