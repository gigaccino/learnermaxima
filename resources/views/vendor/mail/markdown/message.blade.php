@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="https://learnermaxima.nyc3.digitaloceanspaces.com/assets/pages/layout/logo-transparent.png" height="50" class="d-inline-block align-top" alt="Learner Maxima Logo">
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.owner') }}. All rights reserved.
        @endcomponent
    @endslot
@endcomponent
