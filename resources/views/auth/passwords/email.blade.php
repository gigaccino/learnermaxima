@extends ('layouts.form')

@section ('title', 'Reset your password')

@section ('subtitle', 'Don\'t worry. It happens to the best of us!')

@section ('form')

	@if (session('status'))

	    <div class="alert alert-success">
	        {{ session('status') }}
	    </div>
	    
	@endif

	@component ('components.form.master', ['method' => 'POST', 'form_action' => 'Send Password Reset Link','url' => route('password.email'), 'csrf_token' => csrf_token()])

	    @component ('components.form.input', ['name'=> 'email', 'type'=> 'email', 'errors' => $errors])
	        Email Address
	    @endcomponent

	@endcomponent

@endsection
