@extends ('layouts.form')

@section ('title', 'Set your new password')

@section ('subtitle', 'Make sure it\'s safe and secure.')

@section ('form')

    @if (session('status'))

        <div class="alert alert-success">
            {{ session('status') }}
        </div>

    @endif

    @component ('components.form.master', ['method' => 'POST', 'form_action' => 'Reset Password','url' => route('password.request'), 'csrf_token' => csrf_token()])

        @component ('components.form.input', ['name'=> 'token', 'type'=> 'hidden', 'errors' => $errors, 'value' => $token])
            Token
        @endcomponent

        @component ('components.form.input', ['name'=> 'email', 'type'=> 'email', 'errors' => $errors])
            Email Address
        @endcomponent

        @component ('components.form.input', ['name'=> 'password', 'type'=> 'password', 'errors' => $errors])
            Password
        @endcomponent

        @component ('components.form.input', ['name'=> 'password_confirmation', 'type'=> 'password', 'errors' => $errors])
            Confirm New Password
        @endcomponent

    @endcomponent

@endsection
