@extends ('layouts.form')

@section ('title', 'Log In')

@section ('meta-description', "Log In to continue learning with ".config('app.name')." - ".config('app.description'))

@section ('subtitle')
    Not a member? <a href="{{ route('register') }}{{ request()->redirect_to ? '?redirect_to='.request()->redirect_to : ''}}">Create account</a>
@endsection

@section ('form')

    @component ('components.form.master', ['method' => 'POST', 'form_action' => 'Log In', 'url' => route('login'), 'align_btn_left' => true, 'csrf_token' => csrf_token()])

        <input type="hidden" name="redirect_to" value="{{ request()->redirect_to }}">

        @component ('components.form.input', ['name'=> 'email', 'type'=> 'email', 'errors' => $errors, 'value'=> old('email')])
            Email Address
        @endcomponent

        @component ('components.form.input', ['name'=> 'password', 'type'=> 'password', 'errors' => $errors])
            Password
        @endcomponent

        @slot ('link')
            <a class="btn btn-link" href="{{ route('password.request') }}">
                Forgot your password?
            </a>
        @endslot

    @endcomponent

@endsection
