@extends ('layouts.form')

@section ('title', 'Account Sign Up')

@section ('meta-description', "Sign Up to get started learning with ".config('app.name')." - ".config('app.description'))

@section ('subtitle')
    Already a member? <a href="{{ route('login') }}{{ request()->redirect_to ? '?redirect_to='.request()->redirect_to : ''}}">Log in</a>
@endsection

@section ('form')

    @component ('components.form.master', ['method' => 'POST', 'form_action' => 'Sign Up','url' => route('register'), 'enctype' => 'multipart/form-data', 'csrf_token' => csrf_token()])

        <input type="hidden" name="redirect_to" value="{{ request()->redirect_to }}">

        @component ('components.form.radio', ['name'=> 'account_type', 'errors' => $errors, 'option_list_value' => ['student', 'teacher'], 'option_list_text' => ['Student', 'Tutor/Teacher'], 'in_line' => true, 'preselected_value' => old('account_type')])
            Account Type
        @endcomponent

        <div id="create-account-form-container" style="display: none">

            <div class="account-details-container">

                @component ('components.form.input', ['name'=> 'name', 'type'=> 'text', 'errors' => $errors, 'value'=> old('name')])
                    Full Name
                @endcomponent

                @component ('components.form.input', ['name'=> 'email', 'type'=> 'email', 'errors' => $errors, 'value'=> old('email')])
                    Email Address
                @endcomponent

                @component ('components.form.input', ['name'=> 'phone', 'type'=> 'tel', 'errors' => $errors, 'value'=> old('phone')])
                   Phone Number
                @endcomponent

                @component ('components.form.input', ['name'=> 'password', 'type'=> 'password', 'errors' => $errors, 'value'=> old('password')])
                    Password
                @endcomponent

                @component ('components.form.input', ['name'=> 'password_confirmation', 'type'=> 'password', 'errors' => $errors, 'value'=> old('password_confirmation')])
                    Password Confirmation
                @endcomponent

            </div>

            <div class="teacher-account-details-container">
                
                <p class="text-center text-primary">Please upload a copy of a valid form of ID (National ID, Drivers Permit or Passport).</p>
                @component ('components.form.file', ['name'=> 'identification', 'errors' => $errors, 'not_required' => true])
                    Identification (Private and Confidential)
                @endcomponent
                
                <hr>
                
                <p class="text-center text-primary">Teachers, please upload a copy of your Registration Document/Slip containing your Teacher's Number. Tutors, please upload a copy of your Degree / CAPE / CSEC Certificate.</p>
                @component ('components.form.file', ['name'=> 'qualifications', 'errors' => $errors, 'not_required' => true])
                    Qualifications (Private and Confidential)
                @endcomponent
                
                <hr>
                
                @component ('components.form.textarea', ['name'=> 'teaching_experience', 'errors' => $errors, 'max_characters' => \App\User::getMaxTeachingExperienceLength(), 'value'=> old('teaching_experience'), 'not_required' => true])
                    Your Teaching/Tutoring Experience (Public)
                @endcomponent
                
            </div>

            <div class="teacher-account-details-container">

                <p>By registering as a Tutor/Teacher, you the User will be agreeing to these <a href="{{ route('page.terms') }}">Terms and Conditions</a> whereby all Tutors/Teachers are required to publish at least <span class="text-secondary">one classroom video</span> with a duration of twenty (20) to forty (40) minutes <span class="text-secondary">weekly</span>, or <span class="text-primary">4 classroom videos per month</span> for each of your Lessons(Subject) taught.</p>

            </div>

            <div class="account-details-container">

                @component ('components.form.checkbox', ['name'=> 'terms', 'errors' => $errors])
                    I agree to the <a href="{{ route('page.terms') }}">Terms and Conditions</a> of <span class="text-primary">Learner</span> <span class="text-secondary">Maxima</span>.
                @endcomponent

            </div>

        </div>

    @endcomponent

    @section ('scripts')
        <script src="{{ asset('js/form.js') }}"></script>
        <script src="{{ asset('js/register.js') }}"></script>
    @endsection

@endsection
