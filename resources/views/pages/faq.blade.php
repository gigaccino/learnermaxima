@extends ('layouts.app')

@section ('meta-description', "View our Frequently Asked Questions for more information about ".config('app.name')." - ".config('app.description'))

@section ('content')

	@component('components.banner.small')
	    <h2>
	        @yield ('title', 'FAQ')
	    </h2>
	@endcomponent

	<div class="container">
		@component ('components.faq.list')

			@component('components.faq.accordion', ['title' => 'How do I become a Student?', 'is_collapsed' => true])
				Simply Sign Up, fill out the form and select “<span class="text-secondary">Student</span>”.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'How do I become a Tutor/Teacher?'])
				Simply Sign Up or Log In and upload a form of Identification (.doc, .docx, .png, .jpeg, .pdf and .zip). If you’re a Registered Teacher, please upload the documentation containing your registration number. If you are a Tutor, please upload all CXC CSEC and CAPE Certificates, as well as any other degrees, diplomas or certificates (.doc, .docx, .png, .jpeg, .pdf and .zip). All information sent to us is NOT Public, but private and confidential. You can fill out your experiences, if chosen to, on the Sign Up form or in your Profile. Once you have signed up, it takes 1-2 business days for you to be approved as a Tutor/Teacher. Once approved, you will receive an email giving you a payment break down scheme and you’ll be allowed to create lessons, create classrooms and upload videos, create quizzes, upload assignments and the assignments solutions.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'I would like to become a Tutor, what qualifications do I need to upload?'])
				Once you’re 18 years old or over and have your CSEC and CAPE passes you can teach on our site. If you have any other certificates, degrees or diplomas, as well as any type of tutoring or teaching experience, it can help your subscription list to grow and you can fill out your “Experiences” field in your Sign Up form or in your profile.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'I am currently a Tutor, how do I become a Teacher?'])
				A Tutor can become a Teacher by emailing us a form of identification along with their Teacher’s Registration Number. The process to upgrade a Teacher will take 1-2 business days to approve.
			@endcomponent
			
			@component('components.faq.accordion', ['title' => 'How do I pay for a Subscription?'])
				Simply use a <span class="text-secondary">Credit Card</span>, a <span class="text-secondary">Pre-paid Card (VTM or bmobile Visa)</span>, or use cash via a <span class="text-secondary">WiPay Voucher Code</span>.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'How much is a Subscription'])
				A subscription is <span class="text-secondary">$149.00 TTD per month per Lesson</span>.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'How does WiPay work?'])
				For more information, you can check out <a href="https://wipaytoday.com">https://wipaytoday.com</a>. However, the basic steps are as followed:
				<ul>
					<li>Open an account on WiPay for free: <a href="https://www.wipayfinancial.com/v1/Sign_Up">https://www.wipayfinancial.com/v1/Sign_Up</a>.</li>
					<li>Purchase a "WIPAY" Top-up Card at ANY Lotto booth or Massy Stores nationwide, valued at <span class="text-secondary">10, 30, 50, 100, 300, 500 and 1000 TTD</span>. A <span class="text-secondary">$5.00 TTD transaction fee</span> will be added, so if you decide to buy a $200.00 TTD WiPay voucher, you will be required to pay $205.00 TTD.</li>
					<li>
						Once you’re logged on into your WiPay Account, select <span class="text-secondary">DEPOSIT</span>, then select <span class="text-secondary">VOUCHER</span>. Load the <span class="text-secondary">Serial Number</span> (refer to the photo below highlighted in blue) onto your account on WiPay: 
						<div class="text-center">
							<img class="m-2" style="max-height: 350px" src="https://learnermaxima.nyc3.digitaloceanspaces.com/assets/pages/faq/wipay_voucher.jpg" alt="Wipay Voucher">
						</div>
					</li>
					<li>Your account will then have credit to make payments and you can then subscribe to any lesson you desire.</li>
				</ul>

			@endcomponent

			@component('components.faq.accordion', ['title' => 'How do you subscribe to a Lesson?'])
				When you click on a lesson, there is a “<span class="text-secondary">Subscribe</span>” button. Simply click “<span class="text-secondary">Subscribe</span>”.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'How long does a Subscription last?'])
				We have provided options for you. You can subscribe for:
				<ul>
					<li>1 month (30 days)</li>
					<li>3 months (90 Days)</li>
					<li>6 month (180 Days)</li>
				</ul>
				You can decide the best option for you, and pay accordingly.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'Do Subscriptions Auto-Renew?'])
				No. We have placed on each lesson, the number of days remaining. It is up to you to keep track of the remaining time and renew the subscription if you wish.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'I am a Tutor/Teacher. How do I create a Lesson?'])
				There are several places to create a lesson:
				<ul>
					<li>“Create Lesson” in top bar.</li>
					<li>“My Lessons” in top bar has both a link right under the heading, and a “Create Lesson” button as you scroll down.</li>
					<li>There is a “Create Lesson” button on the home page.</li>
				</ul>
				Once you create a lesson, you will be re-directed to your “My Lessons” page. 
			@endcomponent

			@component('components.faq.accordion', ['title' => 'What are some ways in which a video can be produced?'])
				We leave the video production choice completely up to you. However, ways in which you can teach and produce a video are:
				<ul>
					<li>In front of a blackboard or whiteboard.</li>
					<li>Get a screen recording software, which will record your computer’s screen and you can do a voiceover. There is good free screen recording software available, you can search the internet for your choice.</li>
					<li>You can get a digital or interactive whiteboard.</li>
					<li>Buying a digital graphics tablet can come in handy for your recordings. You can find them on Amazon.</li>
				</ul>
			@endcomponent

			@component('components.faq.accordion', ['title' => 'How do Tutors/Teachers decide what to cover in a subject Lesson?'])
				Every Tutor/Teacher, should cover the syllabus for each subject they decide to teach. They can do so by following the syllabus given by CXC. They can download the syllabus from CXC’s website.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'I would like to do a full Lesson covering ONLY SBAs or IAs, is that allowed?'])
				Once the lesson covering SBAs or IAs releases at a minimum one video per week, that will be fine. If the SBA or IA videos may cover less than 4 videos per month, we suggest that you place those videos in your Lesson as Classrooms.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'Is there a minimum number of videos a Tutor/Teacher must upload per month?'])
				Students and their parents are paying you for a service. Therefore, we only think it fair to them that you as a Tutor/Teacher should upload at minimum <span class="text-secondary">one 20-minute video per week</span>.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'What is a "Lesson"?'])
				On Learner Maxima, a Lesson is a subject taught by a Teacher. It consists of several classrooms. 
			@endcomponent

			@component('components.faq.accordion', ['title' => 'What are "Classrooms"?'])
				Classrooms are basically topics. It includes a description, video lesson, and can include online quizzes, assignments, discussions and notes.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'I am a Tutor/Teacher, How do I create Classrooms?'])
				When you click on a Lesson, there is a “Create Classroom”. Simply click on the button.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'How long should a Classroom Video be?'])
				We recommend that the length of a Video be between 20 minutes to 40 minutes.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'How do I create Classrooms?'])
				Simply click on “<span class="text-secondary">Quizzes</span>” under a Classroom. It will take you to the Quizzes page. Click on “<span class="text-secondary">Create Quiz</span>”.	
			@endcomponent
			
			@component('components.faq.accordion', ['title' => 'How do I create Quizzes?'])
				Simply click on “<span class="text-secondary">Quizzes</span>” under a Classroom. It will take you to the Quizzes page. Click on “<span class="text-secondary">Create Quiz</span>”.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'How do I create Assignments?'])
				Click on "<span class="text-secondary">Assignments</span>" under a Classroom. It will take you to the Assignments page. Write the Title, Description and upload a question sheet and/or a solution sheet if necessary. The Solution Sheet has a release date you can choose, or once left blank, it will be released immediately.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'What are Notes?'])
				Notes are optional for a Teacher/Tutor to upload for their students. It can contain basic points to a full transcript of a classroom video. Any content in notes are up to the Teacher/Tutor.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'How many Notes are allowed per Classroom?'])
				Only one attachment is allowed.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'How do I Publish or Unpublish a Classroom?'])
				When a classroom is created, it is created as Unpublish, simply click “Publish” to make it public to your subscribers.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'How do I change a Classroom\'s Video?'])
				To change a classroom video, you must first delete the current video by clicking the gear icon at the top right-side of the classroom. Select “<span class="text-danger">Delete Video</span>”. Once deleted you can upload a new video by clicking “<span class="text-secondary">Upload Video</span>”. 
			@endcomponent

			@component('components.faq.accordion', ['title' => 'How do I delete a Lesson, Classroom, Quiz or Assignment?'])
				To delete, click the gear icon, and select “<span class="text-danger">Delete</span>”.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'As a Tutor/Teacher, How do I update my Payout Details to ensure that I receive my income?'])
				To update your Payout Details, go to “<span class="text-secondary">My Account</span>”, select “<span class="text-secondary">Update Payout Details</span>”, fill out the correct information and click “<span class="text-secondary">Update Payout Details</span>”.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'What happens when I do not upload or publish any classrooms for a month?'])
				A Teacher/ Tutor is deemed inactive, if he/she does not publish a classroom after three (3) weeks. If a Teacher/ Tutor remains inactive after four (4) weeks, subscriptions will be disabled for your lesson and payouts will be held. A Teacher/ Tutor will have two (2) more weeks to re-activate their lesson, but will not be paid for their period of inactivity, and their students will be refunded website credit. If more than six (6) weeks passes, we will be removing all inactive lessons and all data will be lost.
			@endcomponent

			@component('components.faq.accordion', ['title' => 'If I upload a classroom but set it as Unpublished, when I do decide to Publish what date will my Lesson be seen as “Last Updated”?'])
				The “Last Updated” will show the last date a classroom has been Published.
			@endcomponent
		@endcomponent
	</div>

@endsection
