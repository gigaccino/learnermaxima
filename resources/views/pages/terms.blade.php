@extends ('layouts.app')

@section ('content')

	@component('components.banner.small')
	    <h2>
	        @yield ('title', 'Terms and Conditions')
	    </h2>
	@endcomponent

	<div class="container">
		
		<p>Leaner Maxima is a product of Gigaccino Designs (“Us”, “We”). These Terms and Conditions (“the Terms and Conditions”) govern you (“the User”) use of the Learner Maxima website located at the domain name <a href="https://www.learnermaxima.com">https://www.learnermaxima.com</a> (“the Website”). By subscribing, accessing and using the Website, the User agrees to be bound by the Terms and Conditions set out in this legal notice. The User may not access, display, use, download, and/or otherwise copy or distribute Content obtained on the website for marketing and other purposes without the consent of Gigaccino Designs. These Terms and Conditions are subject to change without prior written notice, in Gigaccino Design’s discretion. If Gigaccino Designs decides to send an update notice, it may be done so by email or you can see it by the “last updated” date.</p>
		<br>

		<h3>Electronic Communications</h3>
		<p>By using this Website or communicating with Gigaccino Designs by electronic means, the user consents and acknowledges that any and all agreements, notices, disclosures, or any other communication satisfies any legal requirement, including but not limited to the requirement that such communications should be in writing.</p>
		<br>

		<h3>Purchasing and Account Security</h3>
		<p>The Website <a href="https://www.learnermaxima.com">https://www.learnermaxima.com</a> offers a platform to CXC CSEC and CAPE teachers, tutors and students online.  The use of any service subscribed or bought from this Website is at the purchaser’s risk. The purchaser/ user indemnifies and holds Gigaccino Designs harmless against any loss, monetary or otherwise, which may be sustained as a result of using the services being paid for on the Website.
		The private information required for executing the orders placed through the Financial Handler, namely the User’s personal information and credit card details, address and telephone numbers will be kept in the strictest confidence by Gigaccino Designs and not sold or made known to third parties. Credit card details are not kept by Gigaccino Designs under any circumstances.
		Gigaccino Designs cannot be held responsible for security breaches occurring on the User’s electronic device (Personal Computer or other electronic device used to browse the Website), which may result due to the lack of adequate virus protection software or spyware that the User may inadvertently have installed on his/her device.
		To make a purchase on Learner Maxima, you must be a registered Learner Maxima user and comply with these Terms and Conditions. You acknowledge that you are responsible for maintaining the security of, and restricting access to, your account and password, and you agree to accept responsibility for all purchases and other activities that occur under your account. Learner Maxima's subscription services only to those users who can legally make purchases with a credit card. If you are between the ages of 13 and 18 you may make purchases on Learner Maxima only with the permission of a parent or guardian. Gigaccino Designs reserves the right to refuse or cancel orders or terminate accounts, at any time in its sole discretion.
		</p>
		<br>

		<h3>Online Payment – WiPay Financial</h3>
		<p>All online payments are processed by WiPay Financial.  Card Holders may go to <a href="https://wipaytoday.com">https://wipaytoday.com</a> or <a href="https://www.wipayfinancial.com">https://www.wipayfinancial.com</a> to view their security policies.</p>
		<br>

		<h3>Refund Policy</h3>
		<p>When you make a purchase on Learner Maxima, you have immediate access to, and use of, the Lesson Services you have subscribed to for that month. Accordingly, you lose your right to cancel once you have made your purchase, and we do not offer any refunds or credits, including, without limitation, fees for Subscription Services. Gigaccino Designs reserves the right to modify this refund policy at any time.</p>
		<br>

		<h3>Subscription Service Terms</h3>
		<p>Learner Maxima offers services on a subscription basis with recurring payments from you, the User. Subscription services will NOT renew automatically. You agree to pay your subscription fee in advance of receiving any such Subscription service. Gigaccino Designs reserves the right to discontinue or modify any subscription fee payment option. If we discontinue or modify a subscription payment option, we will provide notice of such discontinuance or modification. Once your subscription period is up, it is up to you, the User, to re-subscribe, if you choose to.
		You are responsible for all charges incurred under your account. 
		</p>
		<br>

		<h3>Cancellation of Subscription Services</h3>
		<p>There is absolutely no cancellation of a subscribed service. The User is paying for a service at his/her own risk. You must wait out the period of time you have paid for.</p>
		<br>

		<h3>Independent Contractors</h3>
		<p>Any User that wishes to use Learner Maxima as a platform to teach or tutor, will be considered an independent contractor. This agreement states that no parties will create any relationship of employer and employee, principal and agent, partnership or joint venture, or any other fiduciary relationship.

		The User may not act as agent for, or on behalf of, to represent, or bind Gigaccino Designs in any manner. 
		The User will not be entitled to worker's compensation, retirement, insurance or other benefits afforded to employees of Gigaccino Designs.
		</p>
		<br>

		<h3>Tutor/Teacher’s Agreement</h3>
		<p>By agreeing to these Terms and Conditions, a Tutor/Teacher (User), will be agreeing to publish at least <span class="text-secondary">one classroom video</span> with a duration of twenty (20) to forty (40) minutes <span class="text-secondary">weekly</span>, or <span class="text-primary">4 classroom videos per month</span> for each of your Lessons(Subject) taught.</p>
		<p>A Tutor/Teacher is deemed inactive, if he/she does not publish a classroom after three (3) weeks. If a Tutor/Teacher remains inactive after four (4) weeks, subscriptions will be disabled for your lesson and payouts will be held. A Tutor/Teacher will have two (2) more weeks to re-activate their lesson, but will not be paid for their period of inactivity, and their students will be refunded website credit. If more than six (6) weeks passes, we will be removing all inactive lessons and all data will be lost.
		</p>Any Teachers Registration Number or any Qualifications (All certificates or otherwise) a Tutor may upload, must not be forged, counterfeit, a hoax or imitation of any sort. If discovered that any user violates this agreement, their account will be banned and terminated, and their payouts will be cancelled upon discovery.</p>
		<br>

		<h3>Compliance Requirements</h3>
		<p>The User will ensure that the information in your application and otherwise associated with your account, including your email address and other contact information and identification of your account, is at all times complete, accurate, and up-to-date.</p>
		<br>

		<h3>Violation</h3>
		<p>At no time is any User to violate this Terms and Conditions agreement. In addition, the User also agrees, not to abuse this website by using foul or inappropriate language, text, videos or photos. If this agreement is violated, you, the User, agree that we reserve the right to withhold any fees payable to you under this agreement.</p>
		<br>

		<h3>Updating of these Terms and Conditions</h3>
		<p>Gigaccino Designs reserves the rights to change, modify, add or remove from portions or the whole of these Terms and Conditions from time to time. Changes to these Terms and Conditions will become effective upon such changes being posted to this Website. It is the User’s obligation to periodically check these Terms and Conditions at the Website for changes or updates. The User’s continued use of this Website following the posting of changes or updates will be considered notice of the User’s acceptance to abide by and be bound by these Terms and Conditions, including such changes or updates.</p>
		<br>

		<h3>Copyright and Intellectual Property Rights</h3>
		<p>Gigaccino Designs provides certain information on the Website. Content currently or anticipated to be displayed on this Website is provided by the User, its affiliates and/or subsidiary, or any other third-party owners of such content, and includes but is not limited to Literary Works, Musical Works, Artistic Works, Sound Recordings, Cinematograph Films, Sound and Television Broadcasts, Program-Carrying Signals, Published Editions and Computer Programs (“the Content”). All proprietary works, and the compilation of the proprietary works, are copyright of Gigaccino Designs, its affiliates or subsidiary, or any other third-party owner of such rights (“the Owners”), and is protected by Copyright Laws. Gigaccino Designs reserve the right to make any changes to the Website, the Content, or to products and/or services offered through the Website at any times and without notice. All rights in and to the Content is reserved and retained by the Owners. Except as specified in these Terms and Conditions, the User is not granted a license or any other right including without limitation under Copyright, Trademark, Patent or other Intellectual Property Rights in or to the Content.</p>
		<br>
		
		<h3>Limitation of liability</h3>
		<p>The Website and all Content on the Website, including any current or future offer of products or services, are provided on an “as is” basis, and may include inaccuracies or typographical errors. The Owners make no warranty or representation as to the availability, accuracy or completeness of the Content. Neither Gigaccino Designs nor any holding company, affiliate or subsidiary of Gigaccino Designs, shall be held responsible for any direct or indirect special, consequential or other damage of any kind whatsoever suffered or incurred, related to the use of, or the inability to access or use the Content or the Website or any functionality thereof, or of any linked website, even if Gigaccino Designs is expressly advised thereof.</p>
		<br>
		
		<h3>Privacy:  casual surfing</h3>
		<p>The User may visit the Website without providing any personal information. The Website servers will in such instances collect the IP address of the User computer, but not the email address or any other distinguishing information. This information is aggregated to measure the number of visits, average time spent at the Website, pages viewed, etc. Gigaccino Designs uses this information to determine use of the Website, and to improve Content thereon. Gigaccino Designs assumes no obligation to protect this information, and may copy, distribute or otherwise use such information without limitation.</p>
		<br>
		
		<h3>Choice of Law</h3>
		<p>This Website is controlled, operated and administered by Gigaccino Designs from its offices within the Republic of Trinidad and Tobago. If the User accesses this Website from locations outside of Trinidad and Tobago, that User is responsible for compliance with all local laws. These Terms and Conditions shall be governed by the laws of the Republic of Trinidad and Tobago, and the User consents to the jurisdiction of the High Court in the event of any dispute. If any of the provisions of these Terms and Conditions are found by a court of competent jurisdiction to be invalid or unenforceable, that provision shall be enforced to the maximum extent permissible so as to give effect to the intent of these Terms and Conditions, and the remainder of these Terms and Conditions shall continue in full force and effect. These Terms and Conditions constitute the entire agreement between Gigaccino Designs and the User with regard to the use of the Content and this Website.</p>
		
	</div>

@endsection
