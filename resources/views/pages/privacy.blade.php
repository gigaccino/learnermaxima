@extends ('layouts.app')

@section ('content')

	@component('components.banner.small')
	    <h2>
	        @yield ('title', 'Privacy Policy')
	    </h2>
	@endcomponent

	<div class="container">
		
		<p>Leaner Maxima is a product of Gigaccino Designs (“Us”, “We”).</p>
		<p>Your privacy is critically important to us. On Learner Maxima, we have a few fundamental principles:</p>
		<ul>
			<li>We are thoughtful about the personal information we ask you to provide and the personal information that we collect about you through the operation of our services.</li>
			<li>We store personal information for only as long as we have a reason to keep it.</li>
			<li>We aim to make it as simple as possible for you to control what information on our website is shared publicly (or kept private), indexed by search engines, and permanently deleted.</li>
			<li>We help protect you from overreaching government demands for your personal information.</li>
			<li>We aim for full transparency on how we gather, use, and share your personal information.</li>
		</ul>
		<p>Below is Leaner Maxima’s privacy policy, which incorporates and clarifies these principles.</p>
		<br>
		<h3>Who We Are and What This Policy Covers</h3>
		<p>Hey There! We are the folks behind Learner Maxima’s services designed to allow anyone–from teachers, tutors, students and parents–to use our platform to teach and learn for CXC CSEC and CAPE exams. Our mission is to democratize online CSEC and CAPE lessons.</p>
		<p>This Privacy Policy applies to information that we collect about you when you use our website.</p>
		<p>Throughout this Privacy Policy we’ll refer to our website as “Services.”</p>
		<p>Below we explain how we collect, use, and share information about you, along with the choices that you have with respect to that information.</p>
		<p>Please note that this Privacy Policy does not apply to any of our services that have a separate privacy policy.</p>
		<p>If you have any questions about this Privacy Policy, please contact us: <a href="mailto:learnermaxima@gmail.com">learnermaxima@gmail.com</a>.</p>
		<br>
		<h3>Information We Collect</h3>
		<p>We only collect information about you if we have a reason to do so–for example, to provide our Services, to communicate with you, or to make our Services better.
		We collect information in two ways: if and when you provide information to us, and automatically through operating our services.
		</p>
		<h5>Information You Provide to Us</h5>
		<p>It’s probably no surprise that we collect information that you provide to us. The amount and type of information depends on the context and how we use the information. Here are some examples:</p>
		<ul>
			<li><strong>Basic Account Information:</strong> We ask for basic information from you in order to set up your account. For Students, we require individuals who sign up for an account to provide a full name, phone number and email address, and that’s it. For Tutors/Teachers, we require individuals who sign up for an account to provide a full name, phone number, email address, valid identification and qualifications.</li>
			<li><strong>Public Profile Information:</strong> If you have an account with us, we collect the information that you provide for your public profile. For example, decide to put a public email address at which students can email you. Your public profile is just that, public, so please keep that in mind when deciding what information you would like to include.</li>
			<li><strong>Transaction and Billing Information:</strong> If you buy something from us–a subscription, you will provide additional personal and payment information that is required to process the transaction and your payment, such as your name, credit card information, and contact information.</li>
		</ul>
		<h5>Information We Collect Automatically</h5>
		<p>We also collect some information automatically:</p>
		<ul>
			<li><strong>Log Information:</strong> Like most online service providers, we collect information that web browsers, mobile devices, and servers typically make available, such as the browser type, IP address, unique device identifiers, language preference, referring site, the date and time of access, operating system, and mobile network information. We collect log information when you use our Services–for example, when you create or make changes to your Lesson.</li>
			<li><strong>Usage Information:</strong> We collect information about your usage of our Services. For example, we collect information about the actions that site administrators and users perform on a site–in other words, who did what, when and to what thing on a site (e.g., [Name] deleted “[title of Lesson]” at [time/date]). We also collect information about what happens when you use our Services (e.g., page views, button clicks) along with information about your device (e.g., mobile screen size, name of cellular network, and mobile device manufacturer). We use this information to, for example, provide our Services to you, as well as get insights on how people use our Services, so we can make our Services better.</li>
			<li><strong>Location Information:</strong> We may determine the approximate location of your device from your IP address. We collect and use this information to, for example, calculate how many people visit our Services from certain geographic regions. </li>
			<li><strong>Information from Cookies & Other Technologies:</strong> A cookie is a string of information that a website stores on a visitor’s computer, and that the visitor’s browser provides to the website each time the visitor returns. </li>
		</ul>
		<br>
		<h3>How We Use Information</h3>
		<p>We use information about you as mentioned above and as follows:</p>
		<ul>
			<li>To provide our Services–for example, to set up and maintain your account, host your website, backup and restore your website, or charge you for any of our paid Services;</li>
			<li>To further develop our Services–for example by adding new features that we think our users will enjoy or will help them to create and manage their websites more efficiently;</li>
			<li>To monitor and analyze trends and better understand how users interact with our Services, which helps us improve our Services and make them easier to use;</li>
			<li>To monitor and protect the security of our Services, detect and prevent fraudulent transactions and other illegal activities, fight spam, and protect the rights and property of Learner Maxima and users;</li>
			<li>To communicate with you and others we think will be of interest to you, solicit your feedback, or keep you up to date on our services; and</li>
			<li>To personalize your experience using our Services, provide content recommendations and serve relevant advertisements.</li>
		</ul>
		<br>
		<h3>Sharing Information</h3>
		<h5>How We Share Information</h5>
		<p>We do not sell our users’ private personal information.</p>
		<p>We share information about you in the limited circumstances spelled out below and with appropriate safeguards on your privacy:</p>
		<ul>
			<li><strong>As Required by Law:</strong> We may disclose information about you in response to a subpoena, court order, or other governmental request.</li> 
			<li><strong>To Protect Rights and Property:</strong> We may disclose information about you when we believe in good faith that disclosure is reasonably necessary to protect the property or rights of Learner Maxima (Gigaccino Designs), third parties, or the public at large. For example, if we have a good faith belief that there is an imminent danger of death or serious physical injury, we may disclose information related to the emergency without delay.</li>
			<li><strong>With Your Consent:</strong> We may share and disclose information with your consent or at your direction. For example, we may share your information with third parties with which you authorize us to do so, such as social media services.</li>
			<li><strong>Aggregated and De-Identified Information:</strong> We may share information that has been aggregated or reasonably de-identified, so that the information could not reasonably be used to identify you. For instance, we may publish aggregate statistics about the use of our Services.</li>
			<li><strong>Published Support Requests:</strong> And if you send us a request (for example, via a support email), we reserve the right to publish that request in order to help us clarify or respond to your request or to help us support other users.</li>
		</ul>
		<h5>Information Shared Publicly</h5>
		<p>Information that you choose to make public is–you guessed it–disclosed publicly. That means, of course, that information like your public profile, posts, other content that you make public on our website, are all available to others–and we hope you get a lot of views! Public information may also be indexed by search engines or used by third parties. Please keep all of this in mind when deciding what you would like to share.</p>
		<br>
		<h3>Security</h3>
		<p>While no online service is 100% secure, we work very hard to protect information about you against unauthorized access, use, alteration, or destruction, and take reasonable measures to do so. To enhance the security of your account, we encourage you to set a proper password.</p>
		<br>
		<h3>Privacy Policy Changes</h3>
		<p>Although most changes are likely to be minor, Learner Maxima may change its Privacy Policy from time to time. Learner Maxima encourages visitors to frequently check this page for any changes to its Privacy Policy. If we make changes, we will notify you by revising the change log below, and, in some cases, we may provide additional notice (such as sending an email). Your continued use of the Services after any change in this Privacy Policy will constitute your consent to such change.</p>
	</div>

@endsection
