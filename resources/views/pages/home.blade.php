@extends ('layouts.app')

@section ('content')

	{{-- Landing --}}
	<section class="container mt-3 mb-5">
		<div class="row justify-content-center">
			<h3 class="col-10 col-sm-8 text-center">{{ config('app.description') }}</h3>
		</div>
		<div class="row justify-content-center mb-3">
			<p class="col-10 col-sm-8 text-center lead">Students! Think of us as Netflix for your CXC Exams.</p>
		</div>
		<div class="row justify-content-center">
			<div class="col-10 col-sm-8 col-md-6">
				@component('components.form.search', ['type' => 'home'])
				@endcomponent
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-8 col-sm-6 col-md-4 mb-3">
				<img src="https://learnermaxima.nyc3.digitaloceanspaces.com/assets/pages/home/rocketship.png" class="img-fluid" alt="Rocket Ship">
			</div>
		</div>
		@if (Auth::Guest())
			<div class="text-center">
				<a href="{{ route('lesson.index') }}" class="btn btn-secondary text-white">All Lessons</a>
			</div>
		@else
			@can ('create', \App\Lesson::class)
				<div class="text-center">
					<a href="{{ route('lesson.create') }}" class="btn btn-secondary text-white">Create Lesson</a>
				</div>
			@else
				<div class="text-center">
					<a href="{{ route('lesson.index') }}" class="btn btn-secondary text-white">All Lessons</a>
				</div>
			@endcannot
		@endif
	</section>

	{{-- Learning --}}
	<section class="container mb-5">
		<div class="row justify-content-center">
			<h3 class="col-8 text-center">Learn from Qualified Teachers and Tutors</h3>
		</div>
		<div class="row justify-content-center">
			<p class="col-8 text-center font-weight-light">Don't hesitate. Enroll in a subject of your choice today and learn from home. Prepare for all CAPE and CSEC exams from the best teachers and tutors. We provide a wide catalog of Classes for both CSEC and CAPE subjects.</p>
		</div>
		<div class="row justify-content-center">
			<div class="col-10 col-sm-8 col-md-6 mb-3">
				<img src="https://learnermaxima.nyc3.digitaloceanspaces.com/assets/pages/home/levels.png" class="img-fluid" alt="Rocket Ship taking off">
			</div>
		</div>
		@if (Auth::Guest())
			<div class="text-center">
				<a href="{{ route('page.faq') }}" class="btn btn-secondary text-white">Frequently Asked Questions</a>
			</div>
		@else
			@can ('create', \App\Lesson::class)
				<div class="text-center">
					<a href="{{ route('lesson.index') }}" class="btn btn-secondary text-white">All Lessons</a>
				</div>
			@endcan
			@cannot ('create', \App\Lesson::class)
				<div class="text-center">
					<a href="{{ route('subscription.mySubscriptions') }}" class="btn btn-secondary text-white">My Subscriptions</a>
				</div>
			@endcannot
		@endif
	</section>

	{{-- About --}}
	<section class="mb-5 pt-5 pb-5 font-weight-light bg-lightgray">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8 text-center">
					<h3 class="text-secondary">Not your ordinary extra Lessons for CXC CSEC and CAPE.</h3>
					<p class="mt-3">Welcome to the first fully ONLINE Lessons platform for CXC CSEC and CAPE. After a long day at school, who wants to go to another classroom? Two hours while you're exhausted after a day of school may seem unbearable. That's where <span class="text-primary">Learner</span> <span class="text-secondary">Maxima</span> saves the day for both teachers and students alike! <span class="text-primary">Learner</span> <span class="text-secondary">Maxima</span> is a student’s dream come through. It allows you to get the extra help that you may need for your CXC CSEC and CAPE exams. At anytime, anywhere, you can get access to Lessons in any subject. We provide a unique experience, providing FULLY Online CXC CSEC and CAPE Classes. All you have to do is Subscribe to a Lesson of your choice for only $149.00 TTD per month and let the magic begin.</p>
					<div class="text-center">
						<a href="{{ route('page.howItWorks') }}" class="btn btn-secondary text-white">How It Works</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	{{-- Call to Action --}}
	@if (Auth::Guest())
		<section class="container mb-5">
			<div class="row justify-content-center mb-3">
				<div class="col-md-8 text-center">
					<h3>Don't miss out on all the possibilities!</h3>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-6 col-md-4 p-2 mb-3">
					<div class="card">
						<div class="card-title text-center pt-3">
							<h3>Become a Student</h3>
						</div>
						<div class="card-body text-center">
							<img src="https://learnermaxima.nyc3.digitaloceanspaces.com/assets/pages/home/student.png" class="img-fluid" alt="Student">
						</div>
					</div>
				</div>
				<div class="col-6 col-md-4 p-2 mb-3">
					<div class="card">
						<div class="card-title text-center pt-3">
							<h3>Become a Teacher</h3>
						</div>
						<div class="card-body text-center">
							<img src="https://learnermaxima.nyc3.digitaloceanspaces.com/assets/pages/home/teacher.png" class="img-fluid" alt="Teacher">
						</div>
					</div>
				</div>
			</div>
			<div class="text-center">
				<a href="{{ route('login') }}" class="btn btn-secondary text-white">Login / Sign Up</a>
			</div>
		</section>
	@endif

	{{-- Recently Added --}}
	@if ($recent_lessons->count() > 0)
		<section class="container mb-5">
			<div class="row justify-content-center">
				<div class="col-md-8 text-center">
					<h3>Recently Added Lessons</h3>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-lg-9">
					@component('components.lesson.card-list', ['lessons' => $recent_lessons])
					@endcomponent
				</div>
			</div>
		</section>
	@endif

@endsection

