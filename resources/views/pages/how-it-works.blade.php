@extends ('layouts.app')

@section ('meta-description', "View our How It Works to get a detailed explaination of ".config('app.name')." - ".config('app.description'))

@section ('content')

	@component('components.banner.small')
	    <h2>
	        @yield ('title', 'How It Works')
	    </h2>
	@endcomponent

	<div class="container">
		<ol class="m-1">
			<li class="p-1" >Sign up or Log In as a Tutor/Teacher or Student.</li>
			<li class="p-1" >As a Student, you search for Lessons you’re interested in for example: Mathematics for CSEC by Jane Doe, and simply subscribe for a month.</li>
			<li class="p-1" >
				Subscription payments are $149.00 TTD per month per Lesson. After hitting that Subscribe button, you can either pay cash through WiPay (you must log into your WiPay account) or using a Credit Card.
				<div>
					<div class="text-secondary">
						How does WiPay work?
					</div>
					For more information, you can check out <a href="https://wipaytoday.com">https://wipaytoday.com</a>. However, the basic steps are as followed:
					<ul>
						<li class="p-1">Open an account on WiPay for free: <a href="https://www.wipayfinancial.com/v1/Sign_Up">https://www.wipayfinancial.com/v1/Sign_Up</a>.</li>
						<li class="p-1">Purchase a "WIPAY" Top-up Card at ANY Lotto booth or Massy Stores nationwide, valued at <span class="text-secondary">10, 30, 50, 100, 200, 300, 500 and 1000 TTD</span>. A <span class="text-secondary">$5.00 TTD transaction fee</span> will be added, so if you decide to buy a $200.00 TTD WiPay voucher, you will be required to pay $205.00 TTD.</li>
						<li class="p-1">
							Once you’re logged on into your WiPay Account, select <span class="text-secondary">DEPOSIT</span>, then select <span class="text-secondary">VOUCHER</span>. Load the <span class="text-secondary">Serial Number</span> (refer to the photo below highlighted in blue) onto your account on WiPay: 
							<div class="text-center">
								<img class="m-2" style="max-height: 350px" src="https://learnermaxima.nyc3.digitaloceanspaces.com/assets/pages/faq/wipay_voucher.jpg" alt="Wipay Voucher">
							</div>
						</li>
						<li class="p-1">Your account will then have credit to make payments and you can then subscribe to any lesson you desire.</li>
						<li class="p-1">Note: Payment options are available for one month, two months or three months at a time.</li>
					</ul>
			</li>
			<li class="p-1" >Once subscribed to a lesson you will have access to video lessons, quizzes, notes and assignments posted by the teacher.</li>
			<li class="p-1">Becoming a Teacher/Tutor is easy! Simply Sign Up or Log In as a Teacher and upload a form of Identification (.doc, .docx, .png, .jpeg, .pdf and .zip). If you’re a Registered Teacher, please upload the documentation containing your registration number under “Qualifications”. If you are a Tutor, please upload all CXC CSEC and CAPE Certificates, as well as any other degrees, diplomas or certificates (.doc, .docx, .png, .jpeg, .pdf and .zip) under “Qualifications”. All information sent to us is NOT Public, but Private and Confidential. You can fill out your experiences, if chosen to, on the Sign Up form or in your Profile. Once you have signed up, it takes 1-2 business days for you to be approved as a Teacher/Tutor. Once approved, you will receive an email giving you a payment break down scheme.</li>
			<li class="p-1">Teachers are labelled as those who have their Teacher’s Registration Number, and Tutors are labelled as those who has their CSEC and CAPE passes, Degrees, Diplomas or other qualifications, as well as any experience in tutoring.</li>
			<li class="p-1">Teachers and Tutors must publish a classroom at least ONCE per week.</li>
			<li class="p-1">Teachers and Tutors can upload videos, create quizzes, upload notes and assignments and its solutions for their students.</li>
			<li class="p-1">If a Teacher or Tutor has not published any classrooms for three (3) weeks, he or she will be deemed inactive and receive an Email informing them to publish a classroom. If a teacher is still inactive after one extra week has passed (4 weeks), subscriptions will be disabled for your lesson and payouts will be held. A Teacher/ Tutor will have two (2) more weeks to re-activate their lesson, but will not be paid for their period of inactivity, and their students will be refunded website credit. If more than six (6) weeks passes, we will be removing all lessons and all data will be lost.</li>
			<li class="p-1">Students can upgrade their profiles to a Teacher or Tutor profile once they are 18 years or over, have their passes (CSEC and/or CAPE) or other qualifications and wish to tutor and receive an income.</li>
			<li class="p-1">Teachers and Tutors can teach any Lesson (subject) they like, and any amount of Lessons (subjects).</li>
			<li class="p-1">Teachers and Tutors will receive an income via bank transfers every month. A Teacher/Tutor must put his/her bank information into his/her “My Account- Update Payout Details”, to ensure that he/she gets his income. The income for that specific month will depend on the number of subscribers per lesson.</li>

		</ol>
	</div>

@endsection
