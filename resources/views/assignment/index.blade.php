@extends ('layouts.app')

@section ('title')
	Assignments - {{ $classroom->title }} - {{ $lesson->level }} {{ $lesson->subject }}, taught by {{ $lesson->user->name }}
@endsection

@section ('content')

	@component('components.banner.small')
	    <h2>Assignments</h2>

	    <small><a href="{{ route('classroom.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number]) }}">
	        {{ $classroom->title }} - {{ $lesson->level }} {{ $lesson->subject }}
	    </a></small>
	@endcomponent

	@if ($classroom->assignments->count() < 1)
		<div class="container p-5">
			<h3 class="text-center">{{ $lesson->user->name }} hasn't uploaded an Assignment to this Classroom.</h3>
		</div>
	@endif

	<div class="container">
		@foreach ($classroom->assignments as $index=>$assignment)
			@component ('components.assignment.card', ['lesson' => $lesson, 'classroom' => $classroom, 'assignment' => $assignment, 'classroom_number' => $classroom_number, 'index' => $index])
			@endcomponent
		@endforeach
		@can ('create', [\App\Assignment::class, $classroom])
			<div class="row justify-content-center">
				<a href="{{ route('assignment.create', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number]) }}" class="btn btn-secondary text-white">Create Assignment</a>
			</div>
		@endcan
	</div>


@endsection
