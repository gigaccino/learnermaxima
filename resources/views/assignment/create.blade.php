@extends ('layouts.form')

@section ('title', 'Creating Assignment')

@section ('subtitle')
	<a href="{{ route('classroom.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number]) }}">
	    {{ $classroom->title }} - {{ $lesson->level }} {{ $lesson->subject }}</a>
@endsection

@section ('form')

    @component ('components.form.master', ['method' => 'POST', 'form_action' => 'Create Assignment','url' => route('assignment.store', ['classroom' => $classroom->id]), 'enctype' => 'multipart/form-data', 'csrf_token' => csrf_token()])

        @component ('components.form.input', ['name'=> 'title', 'type'=> 'text', 'errors' => $errors, 'value'=> old('title')])
            Title
        @endcomponent

        @component ('components.form.textarea', ['name'=> 'instructions', 'errors' => $errors, 'max_characters' => \App\Assignment::getMaxInstructionsLength(), 'value'=> old('instructions')])
            Instructions
        @endcomponent

        <hr>

        @component ('components.form.file', ['name'=> 'questions', 'errors' => $errors, 'not_required' => true])
            Question Sheet (optional)
        @endcomponent
        
        <hr>
        
        @component ('components.form.file', ['name'=> 'solutions', 'errors' => $errors, 'not_required' => true])
            Solution Sheet (optional)
        @endcomponent

        <p class="text-center">Your Solutions can be released in the future. Please enter the number of days we should wait before releasing the Solutions (We start counting from the day you Publish this Classroom). Leave Blank for IMMEDIATE release.</p>        

        @component ('components.form.input', ['name'=> 'solutions_release_days', 'type'=> 'text', 'errors' => $errors, 'value'=> old('solutions_release_days'), 'not_required' => true])
            Number of Days
        @endcomponent
    
    @endcomponent

@endsection
