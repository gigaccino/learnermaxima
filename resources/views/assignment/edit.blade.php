@extends ('layouts.form')

@section ('title', 'Updating Assignment')

@section ('subtitle')
	<a href="{{ route('classroom.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom_number]) }}">
	    {{ $classroom->title }} - {{ $lesson->level }} {{ $lesson->subject }}</a>
@endsection

@section ('form')

    @component ('components.form.master', ['method' => 'PATCH', 'form_action' => 'Update Assignment','url' => route('assignment.update', ['assignment' => $assignment->id]), 'enctype' => 'multipart/form-data', 'csrf_token' => csrf_token()])

        @component ('components.form.input', ['name'=> 'title', 'type'=> 'text', 'errors' => $errors, 'value'=> $assignment->title])
            Title
        @endcomponent

        @component ('components.form.textarea', ['name'=> 'instructions', 'errors' => $errors, 'max_characters' => \App\Assignment::getMaxInstructionsLength(), 'value'=> $assignment->instructions])
            Instructions
        @endcomponent
        
        @if (isset($assignment->solutions_release_days))
            <hr>
            <p class="text-center">Your Solutions can be released in the future. Please enter the number of days we should wait before releasing the Solutions (We start counting from the day you Publish this Classroom). Leave Blank for IMMEDIATE release.</p> 
            @component ('components.form.input', ['name'=> 'solutions_release_days', 'type'=> 'text', 'errors' => $errors, 'value'=> $assignment->solutions_release_days, 'not_required' => true])
                Number of Days
            @endcomponent
        @endif

    @endcomponent

    <hr>

    <div class="text-center mb-3">
        <h3>Assignment Files</h3>
    </div>

    <div class="row mb-3">
        @if (isset($assignment->questions))
            <div class="col-6">
                <p class="card-text">Questions: <a href="{{ $assignment->questions_download_path }}" class="text-uppercase font-weight-light text-secondary"> <i class="ion-ios-cloud-download"> </i>Download</a></p>
            </div>
        @endif
        @if (isset($assignment->solutions))
            <div class="col-6">
                <p class="card-text">Solutions: <a href="{{ $assignment->solutions_download_path }}" class="text-uppercase font-weight-light text-secondary"> <i class="ion-ios-cloud-download"> </i>Download</a></p>
            </div>
        @endif
    </div>
    
    @if (isset($assignment->solutions_release_days))
        @if ($assignment->solutions_release_days > 0)
            <p class="card-text text-center">Solutions will be released {{ $assignment->solutions_release_days }} days after you publish this Classroom.</p>
        @else
            <p class="card-text text-center">Solutions will be released immediately after you publish this Classroom.</p>
        @endif
    @endif

    <hr>

    @can ('delete', [\App\Assignment::class, $assignment])
        <div class="row mb-3 justify-content-center">
            @include ('assignment.destroy')
        </div>
    @endcan 

@endsection
