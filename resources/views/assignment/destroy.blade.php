@component ('components.form.master', ['method' => 'DELETE', 'form_action' => 'Delete Assignment', 'btn_class' => 'btn-danger', 'url' => route('assignment.destroy', ['assignment' => $assignment->id]), 'csrf_token' => csrf_token()])
@endcomponent
