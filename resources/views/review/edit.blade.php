@extends ('layouts.form')

@section ('title', 'Updating your Review')

@section ('subtitle')
    <a href="{{ route('lesson.show', ['lesson' => $lesson->pretty_id]) }}">{{ $lesson->level }} {{ $lesson->subject }}</a>
@endsection

@section ('form')

    @component ('components.form.master', ['method' => 'PATCH', 'form_action' => 'Update Review','url' => route('review.update', ['review' => $review->id]), 'csrf_token' => csrf_token()])

        @component ('components.form.select', ['name'=> 'rating', 'errors' => $errors, 'value' => $review->rating, 'list' => $review_list])
            Rating
        @endcomponent

        @component ('components.form.textarea', ['name'=> 'body', 'errors' => $errors, 'max_characters' => \App\Review::getMaxBodyLength(), 'value'=>$review->body])
            Body
        @endcomponent

    @endcomponent

    @can ('delete', [\App\Review::class, $review])
        <hr>
    	@include ('review.destroy')
    @endcan

@endsection
