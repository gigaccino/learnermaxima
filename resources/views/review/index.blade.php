@if($lesson->reviews->count() > 0)
	<hr>
	<div class="row justify-content-center">
		<h3>Latest Reviews</h3>
	</div>
	<div class="row justify-content-center">
		@if (isset($user_review))
			@can('update', [\App\Review::class, $user_review])
				<a href="{{ route('review.edit', ['lesson' => $lesson->pretty_id, 'review_number' => $user_review->number]) }}" class="btn btn-secondary text-white">Edit Review</a>
			@endcan
		@endif
	</div>
	<div class="row justify-content-center">
		<div class="col-8">
			@foreach ($reviews as $index=>$review)
				<div class="container-review">
					<div class="row">
						<div class="col-md-6">
							<small class="font-weight-light">
								<a href="{{ route('user.show', ['user' => $review->user->pretty_id]) }}">
								    {{ $review->user->name }}
								</a> • 
								{{$review->created_at->diffForHumans()}}
							</small>
							@can('update', [\App\Review::class, $review])
								<a href="{{ route('review.edit', ['lesson' => $lesson->pretty_id, 'review_number' => $index + 1]) }}" class="ion-gear-b text-secondary"></a>
							@endcan
						</div>
						<div class="col-md-6 container-rating">
							@component ('components.lesson.rating', ['rating' => $review->rating])
							@endcomponent
						</div>
					</div>
					<div class="row">
						<div class="col">
							<p>{{$review->body}}</p>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
@endif

<div class="text-center">
	@can('create', [\App\Review::class, $lesson])
		<a href="{{ route('review.create', ['lesson' => $lesson->pretty_id]) }}" class="btn btn-secondary text-white">New Review</a>
	@endcan
</div>

<div class="row justify-content-center pt-2">

	{{ $reviews->links() }}

</div>
