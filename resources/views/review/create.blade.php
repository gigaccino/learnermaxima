@extends ('layouts.form')

@section ('title', 'Writing Review')

@section ('subtitle')
	<a href="{{ route('lesson.show', ['lesson' => $lesson->pretty_id]) }}">{{ $lesson->level }} {{ $lesson->subject }}</a>
@endsection

@section ('form')

    @component ('components.form.master', ['method' => 'POST', 'form_action' => 'Post Review','url' => route('review.store', ['lesson' => $lesson->id]), 'csrf_token' => csrf_token()])

        @component ('components.form.select', ['name'=> 'rating', 'errors' => $errors, 'list' => $review_list])
            Rating
        @endcomponent

        @component ('components.form.textarea', ['name'=> 'body', 'errors' => $errors, 'max_characters' => \App\Review::getMaxBodyLength(), 'value'=> old('body')])
            Body
        @endcomponent

    @endcomponent

@endsection
