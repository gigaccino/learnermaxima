@component ('components.form.master', ['method' => 'DELETE', 'form_action' => 'Delete Review', 'btn_class' => 'btn-danger','url' => route('review.destroy', ['review' => $review->id]), 'csrf_token' => csrf_token()])
@endcomponent
