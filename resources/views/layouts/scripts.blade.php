<script src="{{ asset('js/bootstrap.js') }}"></script>

@yield ('scripts')

@if (Session::has('sweet_alert.alert'))
    <script>
        (function () {
            swal({
                text: "{!! Session::get('sweet_alert.text') !!}",
                title: "{!! Session::get('sweet_alert.title') !!}",
                timer: {!! Session::get('sweet_alert.timer') !!},
                icon: "{!! Session::get('sweet_alert.type') !!}",
                button: false
            });
        }());
    </script>
@endif
