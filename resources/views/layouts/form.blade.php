@extends('layouts.app')

@section('content')

    @component('components.banner.small')
        <h2>
            @yield ('title', 'Form')
        </h2>

        <small>
            @yield ('subtitle', 'Please fill out correctly')
        </small>
    @endcomponent

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6">
                @yield ('form')
            </div>
        </div>
    </div>

@endsection

@section ('scripts')
    <script src="{{ asset('js/form.js') }}"></script>
@endsection
