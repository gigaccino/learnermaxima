<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    {{-- Meta --}}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="@yield('meta-description', config('app.description'))">
    <meta name="keywords" content="cxc, csec, cape, caribbean, examinations, exams, lessons, extra lessons, online lessons, cxc lessons, csec lessons, cape lessons, online classes, cxc classes, csec classes, cape classes, gigaccino, learner maxima" />
    <meta name="author" content="{{ config('app.owner') }}">

    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- Page Title --}}
    <title>{{ config('app.name') }} | @yield ('title', 'Online Lessons')</title>

    {{-- Styles --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="/favicon.png"/>

    {{-- Scripts --}}
    <script src="{{ asset('js/manifest.js') }}"></script>
    <script src="{{ asset('js/vendor.js') }}"></script>
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-91904223-2"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-91904223-2');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '173455443422246');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=173455443422246&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>

    <noscript>
        <div class="container text-center p-2 m-2">
            <h2><span class="text-primary">Learner</span><span class="text-secondary">Maxima</span></h2>
            <p>Sorry! It looks like you've got JavaScript disabled. Unfortunately, JavaScript must be enabled to use this website. <a href="http://www.enable-javascript.com" target="_blank"> Instructions on how to enable JavaScript can be found here.</a> We apologize for the inconvenience.</p>
        </div>
    </noscript>

    {{-- Root Div --}}
    <div id="root">

        {{-- Header --}}
        <header>

            {{-- Primary Nav --}}
            <nav class="navbar navbar-expand-lg navbar-light m-2" id="nav-primary">
                <div class="row w-100 m-0">
                    <div class="col-4 pl-0">
                        {{-- Brand --}}
                        <a class="navbar-brand" href="{{ url('/') }}">
                            <img src="https://learnermaxima.nyc3.digitaloceanspaces.com/assets/pages/layout/logo.png" height="30" class="d-inline-block align-top" alt="Learner Maxima Logo">
                        </a>
                    </div>
                    <div class="col-4">
                        @if (!(Route::getCurrentRoute()->uri() === '/'))
                            @component('components.form.search', ['type' => 'desktop'])
                            @endcomponent
                        @endif
                    </div>
                    <div class="col-4 pr-0">
                        {{-- Toggler (Mobile) --}}
                        <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#navCollapse" aria-controls="navCollapse" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <ul id="nav-desktop" class="navbar-nav float-right">
                            @component ('components.nav.items') 
                            @endcomponent
                        </ul>
                    </div>
                </div>
            </nav>

            {{-- Mobile Nav --}}
            <nav class="navbar navbar-expand-lg navbar-light pt-0 pb-0" id="nav-mobile">
                <div class="collapse navbar-collapse" id="navCollapse">
                    <ul class="navbar-nav ml-auto">
                        @if (!(Route::getCurrentRoute()->uri() === '/'))
                            <li class="nav-item">
                                <div class="container">
                                    @component('components.form.search', ['type' => 'mobile'])
                                    @endcomponent
                                </div>
                            </li>
                        @endif
                        @component ('components.nav.items') 
                        @endcomponent
                    </ul>
                </div>
            </nav>

            {{-- Secondary Nav --}}
            @if (!(Auth::guest()))
                <nav id="nav-secondary" class="container-fluid">
                    <div class="row">

                        @can ('create', \App\Lesson::class)
                            <a class="col-6 col-sm-3 p-3 text-center btn" href="{{ route('lesson.create') }}">
                                Create Lesson
                            </a>
                        @else
                            <a class="col-6 col-sm-3 p-3 text-center btn" href="{{ route('lesson.index') }}">
                                All Lessons
                            </a>
                        @endcannot

                        @can ('create', \App\Lesson::class)
                            <a class="col-6 col-sm-3 p-3 text-center btn" href="{{ route('lesson.myLessons') }}">
                                My Lessons
                            </a>
                        @else
                            <a class="col-6 col-sm-3 p-3 text-center btn" href="{{ route('subscription.mySubscriptions') }}">
                                My Subscriptions
                            </a>
                        @endcan

                        <a class="col-6 col-sm-3 p-3 text-center btn" href="{{ route('user.myProfile') }}">
                            My Profile
                        </a>

                        <a class="col-6 col-sm-3 p-3 text-center btn" href="{{ route('user.myAccount') }}">
                            My Account
                        </a>

                    </div>
                </nav>
            @endif
        </header>
        
        {{-- App --}}
        <div id="app">
            @if (isset(auth()->user()->account_message))
                <div class="alert alert-info text-center m-1" role="alert">
                    {{ auth()->user()->account_message }}
                </div>
            @endif
            @yield ('content')
            <hr>
        </div>

        {{-- Footer --}}
        <footer class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-4 p-3">
                    <div class="row justify-content-center">
                        <div class="col-8 col-md-10">
                            <img src="https://learnermaxima.nyc3.digitaloceanspaces.com/assets/pages/layout/logo.png" alt="Learner Maxima Logo" class="img-fluid">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 p-3">
                    <table class="text-center h-100 m-auto">
                        <tbody>
                            <tr>
                                <td class="align-middle"><a href="{{ route('lesson.index') }}">All Lessons</a></td>
                                @if (Auth::guest())
                                    <td class="align-middle"><a href="{{ route('login') }}">Login / Sign Up</a></td>
                                @else
                                    @if (auth()->user()->isStudent() && !auth()->user()->isPendingUpgrade())
                                        <td class="align-middle"><a href="{{ route('user.showBecomeATeacher', auth()->user()->pretty_id) }}">Become A Teacher</a></td>
                                    @elseif (auth()->user()->isTeacher())
                                        <td class="align-middle"><a href="{{ route('lesson.myLessons') }}">My Lessons</a></td>
                                    @elseif (auth()->user()->isAdmin())
                                        <td class="align-middle"><a href="{{ route('admin.home') }}">Admin Panel</a></td>
                                    @else
                                        <td class="align-middle"><a href="{{ route('user.myAccount') }}">My Account</a></td>
                                    @endif
                                @endif
                            </tr>
                            <tr>
                                <td class="align-middle"><a href="{{ route('page.howItWorks') }}">How It works</a></td>
                                <td class="align-middle"><a href="{{ route('page.privacy') }}">Privacy Policy</a></td>
                            </tr>
                            <tr>
                                <td class="align-middle"><a href="{{ route('page.faq') }}">FAQ</a></td>
                                <td class="align-middle"><a href="{{ route('page.terms') }}">Terms and Conditions</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-4 p-3">
                    <table class="text-center footer-links-table m-auto">
                        <tbody>
                            <tr>
                                <td class="align-bottom">
                                    <p class="m-0">Contact Us</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="align-top">
                                    <a href="mailto:learnermaxima@gmail.com">learnermaxima@gmail.com</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row justify-content-center m-2">
                <p class="col-10 col-sm-8 text-center m-0">We <i class="ion-ios-heart text-danger"></i> making new friends</p>
            </div>
            <div class="row justify-content-center m-2">
                <p class="col-10 col-sm-8 text-center lead m-0">
                    <a href="https://www.facebook.com/learnermaxima" class="mr-3"><i class="ion-social-facebook"></i></a>
                    <a href="https://www.instagram.com/learnermaxima" class="ml-3"><i class="ion-social-instagram-outline"></i></a>
                </p>
            </div>
            <div class="row justify-content-center m-2">
                <p class="col-10 col-sm-8 text-center m-0">
                    <span class="text-primary">Learner</span><span class="text-secondary">Maxima</span> is a product of <a href="https://www.gigaccino.com" class="text-primary">Gigaccino</a>
                </p>
            </div>
            <div class="row justify-content-center m-2">
                <p class="col-10 col-sm-8 text-center m-0">
                    © {{ Carbon\Carbon::now()->year }} {{ config('app.owner') }}
                </p>
            </div>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </footer>
    </div>

    {{-- scripts --}}
    @include('layouts.scripts')

</body>
</html>
