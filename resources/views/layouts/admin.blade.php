@extends('layouts.app')

@section('title', 'Admin Panel')

@section('content')

@component('components.banner.small')
  <h2>Admin Panel</h2>
@endcomponent

<div class="container-fluid mt-2">
	<div class="row justify-content-center">
		<main class="col-sm-10">
			<nav id="nav-secondary" class="container-fluid mb-3">
				<div class="row">
					@if (Route::currentRouteName() !== 'admin.users')
						<a class="col-4 p-3 text-center btn" href="{{ route('admin.users') }}">
						Users
						</a>
					@else
						<span class="col-4 p-3 text-center btn-lg text-secondary">
						Users
						</span>
					@endif
					@if (Route::currentRouteName() !== 'admin.lessons')
						<a class="col-4 p-3 text-center btn" href="{{ route('admin.lessons') }}">
						Lessons
						</a>
					@else
						<span class="col-4 p-3 text-center btn-lg text-secondary">
						Lessons
						</span>
					@endif
					@if (Route::currentRouteName() !== 'admin.subscriptions')
						<a class="col-4 p-3 text-center btn" href="{{ route('admin.subscriptions') }}">
						Subscriptions
						</a>
					@else
						<span class="col-4 p-3 text-center btn-lg text-secondary">
						Subscriptions
						</span>
					@endif
				</div>
			</nav>
		  	@yield('admin_content')
		</main>
	</div>
</div>

@endsection
