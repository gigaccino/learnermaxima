@extends ('layouts.form')

@section ('title', 'Creating Lesson')

@section ('subtitle', 'Students will be able to subscribe to your lesson.')

@section ('form')

    @component ('components.form.master', ['method' => 'POST', 'form_action' => 'Create Lesson', 'url' => route('lesson.store'), 'enctype' => 'multipart/form-data', 'csrf_token' => csrf_token()])

        @component ('components.form.select', ['name'=> 'subject', 'errors' => $errors, 'list' => $subject_list])
            Subject
        @endcomponent

        @component ('components.form.select', ['name'=> 'level', 'errors' => $errors, 'list' => $level_list])
            Level
        @endcomponent

        @component ('components.form.textarea', ['name'=> 'description', 'errors' => $errors, 'max_characters' => \App\Lesson::getMaxDescriptionLength(), 'value'=> old('description')])
            Description
        @endcomponent

        @component ('components.form.file', ['name'=> 'photo', 'errors' => $errors, 'not_required' => true])
            Lesson Photo (optional)
        @endcomponent

    @endcomponent

@endsection
