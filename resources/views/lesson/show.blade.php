@extends ('layouts.app')

@section ('title')
	{{ $lesson->level }} {{ $lesson->subject}}, taught by {{$lesson->user->name }}
@endsection

@section ('meta-description', "Learn $lesson->level $lesson->subject, taught by ".$lesson->user->name." on ".config('app.name')." - ".config('app.description'))

@section ('content')
	@component('components.banner.large')
		<div class="container">
			<div class="row pt-4">
				<div class="col-12 col-lg-6">
					<a href="{{ route('lesson.index') }}?sort=newest-to-oldest&level={{ urlencode($lesson->level) }}&subject=all-subjects" class="pl-3 pr-3 font-weight-light bg-primary text-white">
						{{ $lesson->level }}
					</a>
					<h2>
						{{ $lesson->subject }}
						@can ('update', [\App\Lesson::class, $lesson])
							<small><a href={{ route('lesson.edit', ['lesson' => $lesson->pretty_id]) }} class="ion-gear-b text-secondary"></a></small>
						@endcan
					</h2>
					<p class="text-uppercase font-weight-light">
						<a href="{{ route('user.show', ['user' => $lesson->user->pretty_id]) }}">
						    @if ($lesson->user->isRegisteredTeacher())
						        <i class="ion-ribbon-b" title="Teacher"></i> Teacher:
						    @elseif ($lesson->user->isTeacher())
						        <i class="ion-ribbon-a" title="Tutor"></i> Tutor:
						    @endif
						    {{ $lesson->user->name }}
						</a>
						@if (isset($lesson->user->public_contact_email))
							<br>
							<small class="text-uppercase font-weight-light">EMAIL: <a href="mailto:{{ $lesson->user->public_contact_email }}">{{ $lesson->user->public_contact_email }}</a></small>
						@endif
						<br>
						<small class="text-uppercase font-weight-light text-secondary">
							Last Updated {{ Carbon\Carbon::parse($lesson->last_publish_date)->diffForHumans() }}
						</small>
						@if( isset($subscription) )
							<br>
							@if ($subscription->days_remaining === 0)
								<div class="badge badge-danger text-uppercase text-white">
									Expires Today
								</div>
							@else
								<div class="badge badge-secondary text-uppercase text-white">
									{{ $subscription->days_remaining }} Days Remaining
								</div>
							@endif
						@endif
						@can ('update', [\App\Lesson::class, $lesson])
							<br>
							<div class="badge badge-secondary text-uppercase text-white">
								Students: {{ $lesson->active_subscriptions_count }}
							</div>
							@if (!Auth::guest() && auth()->user()->isAdmin())
								@if ($lesson->isSubscriptionsDisabled())
								    <div class="badge badge-danger text-uppercase text-white">Lesson Disabled</div>
								@else
								    <div class="badge badge-secondary text-uppercase text-white">Lesson Enabled</div>
								@endif
							@endif
						@endcan
					</p>
					<p class="text-gray-300">
						{{ $lesson->description }}
					</p>
				</div>
				<div class="col-12 col-lg-6 pt-4 pb-4">
					<img class="card-img-top rounded-0" src="{{ $lesson->photo_url }}" alt="{{ $lesson->level }} {{ $lesson->subject }} Lesson Photo">
				</div>
			</div>

			<div class="row pb-4">
				<div class="col d-flex">
					<div class="align-self-center m-auto text-center">

						@can ('create', [\App\Subscription::class, $lesson])
							<a href="{{ route('subscription.create', ['lesson' => $lesson->pretty_id]) }}" class="btn btn-secondary text-white">Subscribe</a>							
						@endcan
						
						@if( isset($subscription))
							@can ('renew', [\App\Subscription::class, $subscription])
								<p class="lead text-center">Your subscription is up for renewal. Please do so if you wish to remain subscribed.</p>
								<a href="{{ route('subscription.createRenew', ['lesson' => $lesson->pretty_id]) }}" class="btn btn-secondary text-white">Renew</a>							
							@endcan
						@endif

						@if( isset($subscription) && $subscription->days_remaining === 0)
							<br>
							<p class="lead text-center">Your subscription expires today. You'll have the option to re-subscribe after it expires. Renewals can only be done 1-7 days before expiry.</p>
						@endif

						@can ('create', [\App\Classroom::class, $lesson])
							<a href="{{ route('classroom.create', ['lesson' => $lesson->pretty_id])}}" class="btn btn-secondary text-white">Create Classroom</a>
						@endcan

						@if (Auth::guest())
							<p class="lead">You must be logged in and subscribed to this Lesson to continue.</p>
							<a href="{{ route('login')}}?redirect_to={{ request()->path() }}" class="btn btn-secondary text-white">Login / Signup</a>
						@endif
						
						<hr>

						@component ('components.lesson.rating', ['rating' => $lesson->rating])
						@endcomponent

					</div>
				</div>
			</div>
		</div>
	@endcomponent
	<div class="container">
		@include ('classroom.index')
		@include ('review.index')	
	</div>
@endsection

@section ('scripts')
	<script>
	  fbq('track', 'ViewContent');
	</script>
@endsection
