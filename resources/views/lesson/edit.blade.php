@extends ('layouts.form')

@section ('title', 'Updating Lesson')

@section ('subtitle')
    <a href="{{ route('lesson.show', ['lesson' => $lesson->pretty_id]) }}">{{$lesson->level}} {{$lesson->subject}}</a>
@endsection

@section ('form')

    @if (auth()->user()->isAdmin())
        <h3 class="text-center">Lesson Status (Admin Only)</h3>
        <p class="lead text-center">Subscriptions for this Lesson are currently {!! ($lesson->isSubscriptionsDisabled()) ? '<span class="text-danger">Disabled</span>' : '<span class="text-secondary">Enabled</span>' !!}.</p>
        <div class="row justify-content-center mb-2">
            @can ('enableSubscriptions', [\App\Lesson::class, $lesson])
                <form action="{{ route('lesson.enableSubscriptions', ['lesson' => $lesson->id]) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="submit" value="Enable Lesson" class="btn btn-secondary text-white">
                </form>
            @endcan
            @can ('disableSubscriptions', [\App\Lesson::class, $lesson])
                <form action="{{ route('lesson.disableSubscriptions', ['lesson' => $lesson->id]) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="submit" value="Disable Lesson" class="btn btn-secondary text-white">
                </form>
            @endcan
        </div>
        <hr>
    @endif

    @component ('components.form.master', ['method' => 'PATCH', 'form_action' => 'Update Lesson','url' => route('lesson.update', ['lesson' => $lesson->id]), 'csrf_token' => csrf_token()])

        @component ('components.form.select', ['name'=> 'subject', 'errors' => $errors, 'value' => $lesson->subject, 'list' => $subject_list])
            Subject
        @endcomponent

        @component ('components.form.select', ['name'=> 'level', 'errors' => $errors, 'value' => $lesson->level, 'list' => $level_list])
            Level
        @endcomponent

        @component ('components.form.textarea', ['name'=> 'description', 'errors' => $errors, 'max_characters' => \App\Lesson::getMaxDescriptionLength(), 'value'=> $lesson->description])
            Description
        @endcomponent

    @endcomponent

    <hr>

    <div class="row text-muted justify-content-center">
        <p class="text-center">Lesson Photo</p>
    </div>

    <div class="row mb-3">
        <div class="col-sm-6">
            <img class="card-img-top rounded-0" src="{{ $lesson->photo_url }}" alt="{{ $lesson->level }} {{ $lesson->subject }} Lesson Photo">
        </div>
        <div class="col-sm-6 text-center pt-2">
            @can ('uploadPhoto', [\App\Lesson::class, $lesson])
                @include ('lesson.upload-photo')
            @endcan

            @can ('deletePhoto', [\App\Lesson::class, $lesson])
                @include ('lesson.replace-photo')
            @endcan
        </div>
    </div>

   
    @can ('deletePhoto', [\App\Lesson::class, $lesson])
        <div class="row justify-content-center">
            @include ('lesson.delete-photo')
        </div>
    @endcan
    
    @can ('delete', [\App\Lesson::class, $lesson])
        <hr class="mt-0">
        <h3 class="text-center">Delete Lesson (Admin Only)</h3>
        <div class="row mb-3 justify-content-center">
            @include ('lesson.destroy')
        </div>
    @endcan

@endsection
