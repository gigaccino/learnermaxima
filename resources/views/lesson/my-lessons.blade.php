@extends ('layouts.app')

@section ('title', 'My Lessons')

@section ('content')

  @component ('components.lesson.list')

    @slot ('title', 'My Lessons')

    @slot('subtitle')
      <a href="{{ route('lesson.create') }}">Create Lesson</a>
    @endslot

    <div class="container-fluid">

       <div class="row justify-content-center">
           <div class="col-lg-9">
              <div class="row">
                <div class="container-fluid text-center mt-3">
                  @include ('flash::message')
                </div>
              </div>
              <div class="row mb-3">

                @if ($lessons->count() > 0)
                  @foreach ($lessons as $lesson)

                      <div class="col-12 col-md-6">

                          @component ('components.lesson.card', ['lesson' => $lesson, 'lesson_url' => route('lesson.show', ['lesson' => $lesson->pretty_id])])
                          @endcomponent

                      </div>

                  @endforeach
                @else
                  <div class="row justify-content-center">
                    <div class="col-10 col-md-8 p-2 mb-3">
                      <img src="https://learnermaxima.nyc3.digitaloceanspaces.com/assets/pages/lessons/no-lessons.png" alt="No Subscriptions" class="img-fluid">
                    </div>
                  </div>
                @endif
              </div>
              @can ('create', \App\Lesson::class)
                <div class="text-center">
                  <a href="{{ route('lesson.create') }}" class="btn btn-secondary text-white">Create Lesson</a>
                </div>
              @endcan
           </div>
       </div>
       
    </div>

  @endcomponent
  
@endsection
