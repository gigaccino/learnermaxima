@extends ('layouts.app')

@section ('title', 'Lessons')

@section ('meta-description', "View All Lessons available at ".config('app.name')." - ".config('app.description'))

@section ('content')

  @component ('components.lesson.list')

    @slot ('title')

    	@if ($is_search)

    		<h2>Search Results: {{ request()->search_query }}</h2>
    		<small><a href="{{ route('lesson.index') }}">All Lessons</a></small>

    	@elseif (!$is_all)

    		<h2>Lessons</h2>
    		<small><a href="{{ route('lesson.index') }}">All Lessons</a></small>

    	@else

    		<h2>All Lessons</h2>

    	@endif

    @endslot
    
    <div class="container-fluid">

    	<div class="row">
    		<div class="col-12 col-lg-6">
    			<div class="container-fluid">

    				@if (isset(request()->level))
    					<p class="filter-info-l">Displaying: {{ ucwords(str_replace('-', ' ', request()->level)) }} - {{ ucwords(str_replace('-', ' ', request()->subject)) }}</p>

    				@endif
    			</div>
    		</div>
    		<div class="col-12 col-lg-6">
    			<div class="container-fluid">

    				@if (isset(request()->sort))
    					<p class="filter-info-r">Sorted: {{ ucwords(str_replace('-', ' ', request()->sort)) }}</p>
    				@endif

    			</div>	
    		</div>
    	</div>

    	<div class="row">
    		<div class="col-lg-3 list-lesson-filters">
    			<nav class="navbar navbar-expand-lg navbar-light pt-0 pb-0" id="filters">
    				<button class="btn btn-secondary btn-hide-lg text-white m-auto" type="button" data-toggle="collapse" data-target="#filtersCollapse" aria-controls="navCollapse" aria-expanded="false" aria-label="Toggle navigation">
    				    Refine / Filter
    				</button>
    			    <div class="collapse navbar-collapse" id="filtersCollapse">
    					<form method="GET" action="{{ route('lesson.index') }}" class="w-100">

    						<label for="sort">Sort By</label>
    						<select class="custom-select d-block mb-3 w-100" required id="sort" name="sort" optional>
    						  <option value="newest-to-oldest" selected>Select Sort</option>
    						  <option disabled>---------</option>
    						  <option value="highest-rated">Highest Rated</option>
    						  <option value="newest-to-oldest">Newest to Oldest</option>
    						  <option value="oldest-to-newest">Oldest to Newest</option>
    						</select>

    						<label for="level">Level</label>
    						<select class="custom-select d-block mb-3 w-100" required id="level" name="level" optional>
    							<option value="all-levels" selected>Select Level</option>
    							<option disabled>---------</option>
    							@foreach($level_list as $level)
    								<option value="{{ $level }}">{{ $level }}</option>
    							@endforeach
    						</select>

    						<label for="subject">Subject</label>
    						<select class="custom-select d-block mb-3 w-100" required id="subject" name="subject" optional>
    							<option value="all-subjects" selected>Select Subject</option>
    							<option disabled>---------</option>

    							@foreach ($subject_list as $subject)

    								<option value="{{ $subject }}">{{ $subject }}</option>

    							@endforeach

    						</select>

    						<input type="hidden" name="search_query" value="{{ request()->search_query }}">

    						<button type="submit" class="btn btn-secondary text-white">Apply</button>
    					</form>
    				</div>
    			</nav>
    		</div>
    		<div class="col-lg-9">

    			@component('components.lesson.card-list', ['lessons' => $lessons])
    			@endcomponent

    		</div>
    	</div>

    	<div class="row justify-content-center pt-2">

    	  {{ $lessons->links() }}

    	</div>

    </div>

  @endcomponent

@endsection

@section ('scripts')
    <script>
      fbq('track', 'Search');
    </script>
@endsection
