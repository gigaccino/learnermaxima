@component ('components.form.master', ['method' => 'DELETE', 'form_action' => 'Delete Lesson', 'btn_class' => 'btn-danger', 'url' => route('lesson.destroy', ['lesson' => $lesson->id]), 'csrf_token' => csrf_token()])
@endcomponent
