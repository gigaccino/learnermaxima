(function () {

    'use strict'

    hideAllFieldsAndShowForm();

    $('#account_type-1, #account_type-2').change(function() {

        var account_type = $(this).find('input').val();

        switch (account_type) {

            case 'teacher':
                teacherFields();
                break;

            case 'student':
                studentFields();
                break;
        }

    })

    function hideAllFieldsAndShowForm() {
        $('.teacher-account-details-container, .account-details-container').hide(0, function () {
                $('#create-account-form-container').attr("style", "display: block !important");

                var account_type = $("input[name='account_type']:checked").val();

                switch (account_type) {

                    case 'student':
                        studentFields();
                        break;

                    case 'teacher':
                        teacherFields();
                        break;

                }
        });
    }

    function teacherFields() {
        $('.account-details-container').show();
        $('.teacher-account-details-container').show();
    }

    function studentFields() {
        $('.account-details-container').show();
        $('.teacher-account-details-container').hide();
    }

}());
