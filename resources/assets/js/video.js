(function () {

	'use strict'

	var frame = $('iframe#video');

	if (frame.length) {
		var player = new Player(frame);

		player.getDuration().then(function (secs) {

			// Calculate minutes and round to 1 decimal place.
			var mins = Math.round((secs / 60) * 10) / 10;

			$('#video-duration').text('Time '+ ((mins < 1) ? (secs + " Secs") : (mins + " Mins")));

		});

		player.on('ended', function () {
			if (typeof lm !== 'undefined' && lm !== null) {

				// Checks for classroom variable
				if (typeof lm.classroom !== 'undefined' && lm.classroom !== null) {

					axios.post('/classrooms/' + lm.classroom.id + '/progresses', {
						'completed': true
					}).then(function () {
						swal({
						    text: "You're now that bit smarter!",
						    title: "Awesome!",
						    icon: "success",
						    timer: 3000,
						    button: false
						})
					});
					
				}

			}
		});
	}

})();