(function () {

	'use strict'

	setupSubmitButtons();
	setupTextAreas();
	setupFlatPickr();
	setupVideoUploadVerification();
	setupSubmitVerification();

	function setupSubmitButtons() {

		// Enable all submit buttons, except video uploads by default.
		$("form button[type=submit]")
			.not('form#upload-video button[type=submit]')
			.attr('disabled', false);

	}

	function setupTextAreas() {

		// Attach Text Area character counts for both keyup and change.
		$("form textarea").bind("keyup change", function () {

		    var $counter = $('#'+this.id+"-textarea-counter")
		    var characters_left = $counter.attr('max-characters') - $(this).val().length;

		    $counter.text("Characters Left: " + characters_left);
		    
		})

	}

	function setupFlatPickr() {

		// Attach flatpickr to flatpickr-input
		$('.flatpickr-input').flatpickr({
		    minDate: "today"
		});

	}

	function setupVideoUploadVerification() {

		// Verify file type for 'video upload'
		$("form#upload-video input#file_data").change(function () {

		    if (validateFileType(this, ".mp4, .mkv, .avi")) {

				$("form#upload-video button[type=submit]").attr('disabled', false);

		    } else {

		    	$("form#upload-video button[type=submit]").attr('disabled', true);

		    }

	    });

	}

	function setupSubmitVerification() {

		// Checks JS Namespace for 'lm'
		if (typeof lm !== 'undefined' && lm !== null) {

			// Checks for 'forms' variable
			if (typeof lm.forms !== 'undefined' && lm.forms !== null) {

				// Loops through 'forms'
				for (var index in lm.forms) {

					// Fetch 'formObj'
					var formObj = lm.forms[index];

					// Auto-submit if flagged for 'auto_submit'
					if (formObj.auto_submit === true) {
						$('form#'+formObj.id).submit();
					}

					// Check for swal alerts to attach to relevant form
					if (typeof formObj.swal_submit !== 'undefined' && formObj.swal_submit !== null) {

						$('form#'+formObj.id).submit(function (evt) {

						    evt.preventDefault();

						    var that = this;

						    var form = _.find(lm.forms, function (o) { 
						    	return o.id === evt.currentTarget.id 
						    });

						    swal({
								title: form.swal_submit.title,
								text: form.swal_submit.text,
								icon: form.swal_submit.icon,
								buttons: true,
								dangerMode: form.swal_submit.danger,
						    })
						    .then(function (willDo) {

								if (willDo) {

									if (form.swal_submit.danger) {

										swal({
											title: 'Are you really, really sure?',
											text: 'This cannot be recovered.',
											icon: form.swal_submit.icon,
											buttons: true,
											dangerMode: form.swal_submit.danger,
										}).then(function (willDoDangerously) {

											if (willDoDangerously) {
												that.submit();
											}

										});

									} else {

										that.submit();

									}

								}

						    });

						});
					}

				}

			}


		}
	}

	// File extension validation
	function validateFileType(fdata, validExt) {
		var filePath = fdata.value;
		var getFileExt = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
		var pos = validExt.indexOf(getFileExt);

		if (pos < 0) {

			swal({
				title: 'Incorrect File Type',
				text: 'Valid file types: ' + validExt,
				icon: 'warning',
				buttons: {
					cancel: {
						visible: false,
					},
					confirm: {
						text: "OK",
						value: true,
						visible: true,
						closeModal: true
					}
				}
			});

			return false;

		} else {

			return true;

		}
	}

}());
