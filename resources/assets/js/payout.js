(function () {

    'use strict'

    hideAllFieldsAndShowForm();

    $('#payout_method-1, #payout_method-2').change(function() {

        var payout_method = $(this).find('input').val();

        switch (payout_method) {

            case 'Bank Transfer':
                bankAccountFields();
                break;

            case 'Cheque':
                chequeFields();
                break;
        }

    })

    function hideAllFieldsAndShowForm() {
        $('#update-payout-form-container-bank-transfer, #update-payout-form-container-cheque').hide(0, function () {
                $('#update-payout-form-container').attr("style", "display: block !important");

                var payout_method = $("input[name='payout_method']:checked").val();

                switch (payout_method) {

                    case 'Bank Transfer':
                        bankAccountFields();
                        break;

                    case 'Cheque':
                        chequeFields();
                        break;

                }
        });
    }

    function chequeFields() {
        $('#update-payout-form-container-bank-transfer').hide();
        $('#update-payout-form-container-cheque').show();
    }

    function bankAccountFields() {
        $('#update-payout-form-container-cheque').hide();
        $('#update-payout-form-container-bank-transfer').show();
    }

}());
