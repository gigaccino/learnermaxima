<?php

namespace App;

class Discussion extends Model {

	const MAX_BODY_LENGTH = 280;

	// The attributes that are mass assignable.
	protected $fillable = [

	    'classroom_id', 'replyto', 'body'
	    
	];

	// The attributes that should be shown for arrays.
	protected $visible = [

	    'id', 'classroom_id', 'replyto', 'body'
	];

	public function user() {

		return $this->belongsTo(User::class);
		
	}

	public function classroom() {

	    return $this->belongsTo(Classroom::class);
	    
	}

	public function getNumberAttribute() {

	    $classroom = $this->classroom;

	    $results = $classroom->discussions->pluck('id')->search($this->id) + 1;

	    return $results ? $results : null;
	    
	}

	public function getChildDiscussionsAttribute() {

		return Discussion::where(['replyto' => $this->id])->get();

	}

	public function isRoot() {
		
		return $this->classroom->root_discussions->contains($this);

	}

	public static function getMaxBodyLength() {

		return self::MAX_BODY_LENGTH;

	}
    
}
