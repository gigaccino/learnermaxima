<?php

namespace App\Listeners;

use App\Events\AccountNotUpgraded;
use App\Mail\AccountNotUpgraded as AccountNotUpgradedMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class AccountNotUpgradedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AccountNotUpgraded  $event
     * @return void
     */
    public function handle(AccountNotUpgraded $event)
    {
        $user = $event->user;
        Mail::to($user)->queue(new AccountNotUpgradedMail($user));
    }
}
