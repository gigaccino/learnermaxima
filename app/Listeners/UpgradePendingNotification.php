<?php

namespace App\Listeners;

use App\Events\UpgradePending;
use App\Mail\UpgradePending as UpgradePendingMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class UpgradePendingNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpgradePending  $event
     * @return void
     */
    public function handle(UpgradePending $event)
    {
        $user = $event->user;
        Mail::to($user)->queue(new UpgradePendingMail($user));
    }
}
