<?php

namespace App\Listeners;

use App\Events\AccountUpgraded;
use App\Mail\AccountUpgraded as AccountUpgradedMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class AccountUpgradedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AccountUpgraded  $event
     * @return void
     */
    public function handle(AccountUpgraded $event)
    {
        $user = $event->user;
        Mail::to($user)->queue(new AccountUpgradedMail($user));
    }
}
