<?php

namespace App\Listeners;

use App\Events\SubscriptionRefunded;
use App\Mail\SubscriptionRefunded as SubscriptionRefundedMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SubscriptionRefundedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SubscriptionRefunded  $event
     * @return void
     */
    public function handle(SubscriptionRefunded $event)
    {
        $subscription = $event->subscription;
        Mail::to($subscription->user)->queue(new SubscriptionRefundedMail($subscription));
    }
}
