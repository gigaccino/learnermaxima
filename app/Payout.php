<?php

namespace App;

use App\Subscription;
use App\User;

class Payout extends Model {


	// The attributes that are mass assignable.
	protected $fillable = [

	   'amount', 'payout_date', 'payout_description'
	    
	];

	// The attributes that should be shown for arrays.
	protected $visible = [

	    'id', 'amount', 'payout_date', 'payout_description'

	];

	public function user() {

		return $this->belongsTo(User::class);

	}

	public function subscriptions() {

		return $this->hasMany(Subscription::class);

	}
}
