<?php

namespace App;

use App\Lesson;
use App\User;

class Review extends Model {

	const MAX_BODY_LENGTH = 280;

	// The attributes that are mass assignable.
	protected $fillable = [

	    'lesson_id', 'rating', 'body'
	    
	];

	// The attributes that should be shown for arrays.
	protected $visible = [

	    'id', 'lesson_id', 'rating', 'body'
	];

	public function user() {

		return $this->belongsTo(User::class);
		
	}

    public function lesson() {

    	return $this->belongsTo(Lesson::class);

    }

    public function getNumberAttribute() {

        $lesson = $this->lesson;

        $results = $lesson->reviews->pluck('id')->search($this->id) + 1;

        return $results ? $results : null;
        
    }

    public static function get(User $user, Lesson $lesson) {

    	return Review::where(['user_id' => $user->id, 'lesson_id' => $lesson->id])->first();

    }

    public static function getMaxBodyLength() {

    	return self::MAX_BODY_LENGTH;

    }

}
