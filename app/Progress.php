<?php

namespace App;

class Progress extends Model {
	
	// The attributes that are mass assignable.
	protected $fillable = [

	    'classroom_id', 'completed'
	    
	];

	// The attributes that should be shown for arrays.
	protected $visible = [

	    'id', 'classroom_id', 'completed'

	];

	public function user() {

	    return $this->belongsTo(User::class);
	    
	}

    public function classrooms() {

    	return $this->belongsTo(Classroom::class);

    }
}
