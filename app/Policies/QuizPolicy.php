<?php

namespace App\Policies;

use App\Quiz;
use App\Classroom;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class QuizPolicy {

    use HandlesAuthorization;

    public function index(User $user, Classroom $classroom) {

        $lesson = $classroom->lesson;

        $is_logged_in = Auth::check();
        $is_subscribed = $user->isSubscribed($lesson);
        $is_owner = $user->isOwner($lesson);
        $is_admin = $user->isAdmin();
        $is_published = $classroom->isPublished();

        return  ($is_logged_in && (($is_subscribed && $is_published) || $is_owner || $is_admin));

    }

    public function view(User $user, Quiz $quiz) {

        $classroom = $quiz->classroom;
        $lesson = $classroom->lesson;

        $is_logged_in = Auth::check();
        $is_subscribed = $user->isSubscribed($lesson);
        $is_owner = $user->isOwner($lesson);
        $is_admin = $user->isAdmin();
        $is_published = $classroom->isPublished();

        return  ($is_logged_in && (($is_subscribed && $is_published) || $is_owner || $is_admin));

    }

    public function create(User $user, Classroom $classroom) {

        $lesson = $classroom->lesson;

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($lesson);

        return ($is_logged_in && (($is_teacher && $is_owner) || $is_admin));

    }

    public function update(User $user, Quiz $quiz) {

        $classroom = $quiz->classroom;
        $lesson = $classroom->lesson;

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($lesson);

        return ($is_logged_in && (($is_teacher && $is_owner) || $is_admin));

    }

    public function delete(User $user, Quiz $quiz) {

        $classroom = $quiz->classroom;
        $lesson = $classroom->lesson;

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($lesson);

        return ($is_logged_in && (($is_teacher && $is_owner) || $is_admin));
        
    }

}
