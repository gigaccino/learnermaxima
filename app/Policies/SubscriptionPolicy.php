<?php

namespace App\Policies;

use App\Lesson;
use App\Subscription;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class SubscriptionPolicy {

    use HandlesAuthorization;

    public function index(User $user, Lesson $lesson) {

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($lesson);

        return ($is_logged_in && (($is_teacher && $is_owner) || $is_admin));
        
    }

    public function view(User $user, Subscription $subscription) {

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($subscription->lesson);
        $is_subscription_owner = $subscription->user->id === $user->id;

        return ($is_logged_in && (($is_teacher && $is_owner) || $is_admin || $is_subscription_owner));
        
    }

    public function create(User $user, Lesson $lesson) {

        $is_logged_in = Auth::check();
        $can_subscribe = $user->canSubscribe($lesson);
        $subscriptions_not_disabled = !($lesson->isSubscriptionsDisabled());

        return ($is_logged_in && $can_subscribe && $subscriptions_not_disabled);
        
    }

    public function createFree(User $user, Lesson $lesson) {

        $is_logged_in = Auth::check();
        $can_subscribe = $user->canSubscribe($lesson);
        $subscriptions_not_disabled = !($lesson->isSubscriptionsDisabled());
        $has_credit = $user->hasCredit();

        return ($is_logged_in && $can_subscribe && $subscriptions_not_disabled && $has_credit);
        
    }

    public function refund(User $user, Subscription $subscription) {

        $is_logged_in = Auth::check();
        $is_admin = $user->isAdmin();
        $less_than_4_days_elapsed = Carbon::parse($subscription->payment_date)->diffInDays(Carbon::now()) < 4;
        $is_refundable = round($subscription->subscription_length / Subscription::BASE_SUBSCRIPTION_LENGTH) >= 1;
        $is_not_refunded = !($subscription->isRefunded());
        $is_not_paid_out = !($subscription->is_paid_out);
        
        return ($is_logged_in && $is_admin && $less_than_4_days_elapsed && $is_refundable && $is_not_refunded && $is_not_paid_out);
        
    }

    public function renew(User $user, Subscription $subscription) {

        $is_logged_in = Auth::check();
        $is_subscription_owner = $subscription->user->id === $user->id;
        $is_valid_renew_period = $subscription->days_remaining <= 7 && $subscription->days_remaining >= 1;

        return ($is_logged_in && $is_subscription_owner && $is_valid_renew_period);
        
    }
    
}
