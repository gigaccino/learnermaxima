<?php

namespace App\Policies;

use App\User;
use App\Lesson;
use App\Review;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class ReviewPolicy {

    use HandlesAuthorization;

    public function create(User $user, Lesson $lesson) {

        $is_logged_in = Auth::check();
        $is_subscribed = $user->isSubscribed($lesson);
        $is_not_owner = !($user->isOwner($lesson));
        $is_admin = $user->isAdmin();
        $is_not_reviewed = !($user->reviews->intersect($lesson->reviews)->isNotEmpty());

        return  ($is_logged_in && ((($is_not_owner && $is_subscribed) || $is_admin) && $is_not_reviewed)) ;

    }

    public function update(User $user, Review $review) {

        $is_logged_in = Auth::check();
        $is_admin = $user->isAdmin();
        $is_author = $user->isAuthor($review);

        return  ($is_logged_in && ($is_author || $is_admin));

    }

    public function delete(User $user, Review $review) {

        $is_logged_in = Auth::check();
        $is_admin = $user->isAdmin();
        $is_author = $user->isAuthor($review);

        return  ($is_logged_in && ($is_author || $is_admin));
        
    }
    
}
