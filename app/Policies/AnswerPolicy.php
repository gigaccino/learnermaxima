<?php

namespace App\Policies;

use App\Answer;
use App\Question;
use App\Quiz;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class AnswerPolicy {

    use HandlesAuthorization;

    public function create(User $user, Quiz $quiz) {

        $classroom = $quiz->classroom;
        $lesson = $classroom->lesson;

        $is_logged_in = Auth::check();
        $is_owner = $user->isOwner($lesson);
        $is_subscribed = $user->isSubscribed($lesson);
        $is_admin = $user->isAdmin();
        $is_published = $classroom->isPublished();

        return ($is_logged_in && $is_published && ((!$is_owner && $is_subscribed) || $is_admin));

    }

}
