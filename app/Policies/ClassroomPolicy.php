<?php

namespace App\Policies;

use App\User;
use App\Lesson;
use App\Classroom;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class ClassroomPolicy {

    use HandlesAuthorization;

    public function view(User $user, Classroom $classroom) {

        $is_logged_in = Auth::check();
        $is_subscribed = $user->isSubscribed($classroom->lesson);
        $is_owner = $user->isOwner($classroom->lesson);
        $is_admin = $user->isAdmin();
        $is_published = $classroom->isPublished();

        return  ($is_logged_in && (($is_subscribed && $is_published) || $is_owner || $is_admin));

    }

    public function create(User $user, Lesson $lesson) {

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($lesson);

        return ($is_logged_in && (($is_teacher && $is_owner) || $is_admin));

    }

    public function update(User $user, Classroom $classroom) {

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($classroom->lesson);

        return ($is_logged_in && (($is_teacher && $is_owner) || $is_admin));

    }

    public function delete(User $user, Classroom $classroom) {

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($classroom->lesson);

        return ($is_logged_in && (($is_teacher && $is_owner) || $is_admin));
        
    }

    public function uploadVideo(User $user, Classroom $classroom) {

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($classroom->lesson);
        $is_video_unset = !(isset($classroom->video));

        return (($is_logged_in && (($is_teacher && $is_owner) || $is_admin)) && $is_video_unset);

    }

    public function deleteVideo(User $user, Classroom $classroom) {

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($classroom->lesson);
        $is_video_set = isset($classroom->video);

        return (($is_logged_in && (($is_teacher && $is_owner) || $is_admin)) && $is_video_set);

    }

    public function uploadAttachment(User $user, Classroom $classroom) {

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($classroom->lesson);
        $is_attachment_unset = !(isset($classroom->attachment));

        return (($is_logged_in && (($is_teacher && $is_owner) || $is_admin)) && $is_attachment_unset);

    }

    public function deleteAttachment(User $user, Classroom $classroom) {

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($classroom->lesson);
        $is_attachment_set = isset($classroom->attachment);

        return (($is_logged_in && (($is_teacher && $is_owner) || $is_admin)) && $is_attachment_set);

    }

    public function publish(User $user, Classroom $classroom) {

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($classroom->lesson);
        $is_video_set = isset($classroom->video);
        $is_unpublished = !$classroom->isPublished();

        return ($is_logged_in && $is_unpublished && $is_video_set && (($is_teacher && $is_owner) || $is_admin));

    }

    public function unpublish(User $user, Classroom $classroom) {

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($classroom->lesson);
        $is_published = $classroom->isPublished();

        return ($is_logged_in && $is_published && (($is_teacher && $is_owner) || $is_admin));

    }
}
