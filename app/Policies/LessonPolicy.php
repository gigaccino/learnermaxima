<?php

namespace App\Policies;

use App\User;
use App\Lesson;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class LessonPolicy {

    use HandlesAuthorization;

    public function view(User $user, Lesson $lesson) {

        $is_logged_in = Auth::check();
        $is_admin = $user->isAdmin();
        $is_teacher = $user->isTeacher();
        $is_owner = $user->isOwner($lesson);
        $is_subscribed = $user->isSubscribed($lesson);
        $is_subscriptions_not_disabled = !($lesson->isSubscriptionsDisabled());

        return ($is_subscriptions_not_disabled || $is_subscribed || ($is_logged_in && ($is_admin || ($is_teacher && $is_owner))));

    }

    public function create(User $user) {

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();

        return ($is_logged_in && ($is_teacher || $is_admin));

    }

    public function update(User $user, Lesson $lesson) {

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($lesson);

        return ($is_logged_in && (($is_teacher && $is_owner) || $is_admin));

    }

    public function delete(User $user, Lesson $lesson) {

        $is_logged_in = Auth::check();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($lesson);

        return ($is_logged_in && $is_admin);

    }

    public function uploadPhoto(User $user, Lesson $lesson) {

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($lesson);
        $is_photo_unset = !(isset($lesson->photo));

        return (($is_logged_in && (($is_teacher && $is_owner) || $is_admin)) && $is_photo_unset);

    }

    public function deletePhoto(User $user, Lesson $lesson) {

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($lesson);
        $is_photo_set = isset($lesson->photo);

        return (($is_logged_in && (($is_teacher && $is_owner) || $is_admin)) && $is_photo_set);

    }

    public function enableSubscriptions(User $user, Lesson $lesson) {

        $is_logged_in = Auth::check();
        $is_admin = $user->isAdmin();
        $is_subscriptions_disabled = $lesson->isSubscriptionsDisabled();

        return ($is_logged_in && $is_admin && $is_subscriptions_disabled);

    }

    public function disableSubscriptions(User $user, Lesson $lesson) {

        $is_logged_in = Auth::check();
        $is_admin = $user->isAdmin();
        $is_subscriptions_not_disabled = !($lesson->isSubscriptionsDisabled());

        return ($is_logged_in && $is_admin && $is_subscriptions_not_disabled);

    }

}
