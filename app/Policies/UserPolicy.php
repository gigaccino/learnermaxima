<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class UserPolicy {

    use HandlesAuthorization;

    public function admin(User $user) {

        $is_logged_in = Auth::check();
        $is_admin = $user->isAdmin();

        return  ($is_logged_in && $is_admin);

    }

    public function view(User $user) {

        $is_logged_in = Auth::check();

        return $is_logged_in;

    }

    public function viewPayouts(User $user, $user_id) {

        $target_user = User::find($user_id);

        $is_logged_in = Auth::check();
        $is_target_user = $user->id === (int) $user_id;
        $is_admin = $user->isAdmin();
        $does_target_user_have_payouts = $target_user->payouts->first() !== null;

        return  ($is_logged_in && ($is_target_user || $is_admin) && $does_target_user_have_payouts);

    }

    public function update(User $user, $user_id) {

        $is_logged_in = Auth::check();
        $is_target_user = $user->id === (int) $user_id;
        $is_admin = $user->isAdmin();

        return  ($is_logged_in && ($is_target_user || $is_admin));

    }

    public function updatePayoutDetails(User $user, $user_id) {

        $is_logged_in = Auth::check();
        $is_target_user = $user->id === (int) $user_id;
        $is_admin = $user->isAdmin();
        $is_teacher = $user->isTeacher();

        return  ($is_logged_in && (($is_target_user && $is_teacher) || $is_admin));

    }

    public function payOff(User $user, $user_id) {

        $target_user = User::find($user_id);

        $is_logged_in = Auth::check();
        $is_not_target_user = $user->id !== (int) $user_id;
        $is_admin = $user->isAdmin();
        $is_target_user_owed = $target_user->amount_owed > 0;
        $is_target_user_payable = $target_user->isTeacher() || $target_user->isAdmin();
        $is_target_user_payout_details_set = isset($target_user->payout_method);

        return  ($is_logged_in && $is_admin && $is_not_target_user && $is_target_user_owed && $is_target_user_payout_details_set && $is_target_user_payable);

    }

    public function upgrade(User $user, $user_id, $new_privilege) {

        $target_user = User::find($user_id);
        $is_logged_in = Auth::check();
        $is_not_target_user = $user->id !== (int) $user_id;
        $is_admin = $user->isAdmin();
        $is_target_user_pending_upgrade = $target_user->isPendingUpgrade();

        if ($new_privilege === 'Tutor' && $target_user->isStudent()) {
            $is_target_user_upgradable = true;
        } elseif ($new_privilege === 'Teacher' && ($target_user->isStudent() || ($target_user->isTeacher() && !($target_user->isRegisteredTeacher())))) {
            $is_target_user_upgradable = true;
        } else {
            $is_target_user_upgradable = false;
        }

        return  ($is_logged_in && $is_not_target_user && $is_admin && $is_target_user_pending_upgrade && $is_target_user_upgradable);

    }

    public function cancelUpgrade(User $user, $user_id) {

        $target_user = User::find($user_id);

        $is_logged_in = Auth::check();
        $is_not_target_user = $user->id !== (int) $user_id;
        $is_admin = $user->isAdmin();
        $is_target_user_pending_upgrade = $target_user->isPendingUpgrade();

        return  ($is_logged_in && $is_not_target_user && $is_admin && $is_target_user_pending_upgrade);

    }

    public function downgrade(User $user, $user_id) {

        $target_user = User::find($user_id);

        $is_logged_in = Auth::check();
        $is_not_target_user = $user->id !== (int) $user_id;
        $is_admin = $user->isAdmin();
        $is_target_user_teacher = $target_user->isTeacher();

        return  ($is_logged_in && $is_not_target_user && $is_admin && $is_target_user_teacher);

    }

    public function becomeATeacher(User $user, $user_id) {

        $target_user = User::find($user_id);

        $is_logged_in = Auth::check();
        $is_target_user = $user->id === (int) $user_id;
        $is_student = $user->isStudent();
        $is_target_user_not_pending_upgrade = !($target_user->isPendingUpgrade());

        return  ($is_logged_in && $is_target_user && $is_student && $is_target_user_not_pending_upgrade);

    }

    public function upgradeToRegisteredTeacher(User $user, $user_id) {

        $target_user = User::find($user_id);

        $is_logged_in = Auth::check();
        $is_admin = $user->isAdmin();
        $is_target_user_teacher = $target_user->isTeacher();
        $is_target_user_not_registered_teacher = !($target_user->isRegisteredTeacher());

        return  ($is_logged_in && $is_admin && $is_target_user_teacher && $is_target_user_not_registered_teacher);

    }

    public function uploadPhoto(User $user, $user_id) {

        $target_user = User::find($user_id);

        $is_logged_in = Auth::check();
        $is_target_user = $user->id === (int) $user_id;
        $is_admin = $user->isAdmin();
        $is_target_user_photo_unset = !(isset($target_user->photo));

        return ($is_logged_in && ($is_target_user || $is_admin) && $is_target_user_photo_unset);

    }

    public function deletePhoto(User $user, $user_id) {

        $target_user = User::find($user_id);

        $is_logged_in = Auth::check();
        $is_target_user = $user->id === (int) $user_id;
        $is_admin = $user->isAdmin();
        $is_target_user_photo_set = isset($target_user->photo);

        return ($is_logged_in && ($is_target_user || $is_admin) && $is_target_user_photo_set);

    }

    public function uploadIdentification(User $user, $user_id) {

        $target_user = User::find($user_id);

        $is_logged_in = Auth::check();
        $is_admin = $user->isAdmin();
        $is_target_user_identification_unset = !(isset($target_user->identification));

        return ($is_logged_in && $is_admin && $is_target_user_identification_unset);

    }

    public function uploadQualifications(User $user, $user_id) {

        $target_user = User::find($user_id);

        $is_logged_in = Auth::check();
        $is_admin = $user->isAdmin();
        $is_target_user_qualifications_unset = !(isset($target_user->qualifications));

        return ($is_logged_in && $is_admin && $is_target_user_qualifications_unset);

    }

    public function deleteIdentification(User $user, $user_id) {

        $target_user = User::find($user_id);

        $is_logged_in = Auth::check();
        $is_admin = $user->isAdmin();
        $is_target_user_identification_set = isset($target_user->identification);

        return ($is_logged_in && $is_admin && $is_target_user_identification_set);

    }

    public function deleteQualifications(User $user, $user_id) {

        $target_user = User::find($user_id);

        $is_logged_in = Auth::check();
        $is_admin = $user->isAdmin();
        $is_target_user_qualifications_set = isset($target_user->qualifications);

        return ($is_logged_in && $is_admin && $is_target_user_qualifications_set);

    }

    public function delete(User $user, $user_id) {

        $target_user = User::find($user_id);
        $is_target_user = $user->id === (int) $user_id;

        $is_logged_in = Auth::check();
        $is_admin = $user->isAdmin();
        $is_target_user_admin = $target_user->isAdmin();

        return ($is_logged_in && $is_admin && !$is_target_user && !$is_target_user_admin);

    }

    public function gift(User $user, $user_id) {

        $target_user = User::find($user_id);

        $is_logged_in = Auth::check();
        $is_admin = $user->isAdmin();
        $is_target_user_student = $target_user->isStudent();
        $is_not_target_user = $user->id !== (int) $user_id;

        return ($is_logged_in && $is_admin && $is_target_user_student && $is_not_target_user);

    }

}
