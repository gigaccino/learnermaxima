<?php

namespace App\Policies;

use App\Question;
use App\Quiz;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class QuestionPolicy {

    use HandlesAuthorization;

    public function create(User $user, Quiz $quiz) {

        $classroom = $quiz->classroom;
        $lesson = $classroom->lesson;

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($lesson);

        return ($is_logged_in && (($is_teacher && $is_owner) || $is_admin));

    }

    public function update(User $user, Question $question) {

        $classroom = $question->quiz->classroom;
        $lesson = $classroom->lesson;

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($lesson);

        return ($is_logged_in && (($is_teacher && $is_owner) || $is_admin));

    }

    public function delete(User $user, Question $question) {

        $classroom = $question->quiz->classroom;
        $lesson = $classroom->lesson;

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($lesson);

        return ($is_logged_in && (($is_teacher && $is_owner) || $is_admin));
        
    }

    public function uploadPhoto(User $user, Question $question) {

        $classroom = $question->quiz->classroom;
        $lesson = $classroom->lesson;

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($lesson);
        $is_photo_unset = !(isset($question->photo));

        return (($is_logged_in && (($is_teacher && $is_owner) || $is_admin)) && $is_photo_unset);

    }

    public function deletePhoto(User $user, Question $question) {

        $classroom = $question->quiz->classroom;
        $lesson = $classroom->lesson;

        $is_logged_in = Auth::check();
        $is_teacher = $user->isTeacher();
        $is_admin = $user->isAdmin();
        $is_owner = $user->isOwner($lesson);
        $is_photo_set = isset($question->photo);

        return (($is_logged_in && (($is_teacher && $is_owner) || $is_admin)) && $is_photo_set);

    }

}
