<?php

namespace App\Policies;

use App\Discussion;
use App\Classroom;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class DiscussionPolicy {

    use HandlesAuthorization;

    public function index(User $user, Classroom $classroom) {

        $is_logged_in = Auth::check();
        $is_subscribed = $user->isSubscribed($classroom->lesson);
        $is_owner = $user->isOwner($classroom->lesson);
        $is_admin = $user->isAdmin();
        $is_published = $classroom->isPublished();

        return  ($is_logged_in && (($is_subscribed && $is_published) || $is_owner || $is_admin));

    }

    public function create(User $user, Classroom $classroom, $discussion_data = null) {

        $lesson = $classroom->lesson;

        $is_logged_in = Auth::check();
        $is_subscribed = $user->isSubscribed($lesson);
        $is_owner = $user->isOwner($lesson);
        $is_admin = $user->isAdmin();
        $is_teacher = $user->isTeacher();
        $is_published = $classroom->isPublished();

        if (isset($discussion_data)) {
            if (isset($discussion_data['replyto'])) {

                $replyto_discussion = Discussion::find($discussion_data['replyto']);

                if (!$replyto_discussion->isRoot()) {

                    // Denies if not the replyto is not a root discussion element.
                    return false;

                }

            }
        }

        return  ($is_logged_in && (($is_subscribed && $is_published) || ($is_teacher && $is_owner) || $is_admin));

    }

    public function update(User $user, Discussion $discussion) {

        $classroom = $discussion->classroom;
        $lesson = $classroom->lesson;

        $is_logged_in = Auth::check();
        $is_admin = $user->isAdmin();
        $is_commenter = $user->isCommenter($discussion);
        $is_owner = $user->isOwner($lesson);
        $is_published = $classroom->isPublished();

        return  ($is_logged_in && (($is_commenter && $is_published ) || $is_admin || ($is_commenter && $is_owner)));

    }

    public function delete(User $user, Discussion $discussion) {

        $classroom = $discussion->classroom;
        $lesson = $classroom->lesson;

        $is_logged_in = Auth::check();
        $is_admin = $user->isAdmin();
        $is_commenter = $user->isCommenter($discussion);
        $is_owner = $user->isOwner($lesson);
        $is_published = $classroom->isPublished();

        return  ($is_logged_in && (($is_commenter && $is_published ) || $is_admin || ($is_commenter && $is_owner)));
        
    }
    
}
