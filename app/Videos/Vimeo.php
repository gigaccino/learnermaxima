<?php

namespace App\Videos;

use App\General;
use Carbon\Carbon;

class Vimeo {

	const UPLOAD_LIMIT = 1000000000; // 1GB
	const RATE_LIMIT = 50;

	const UPLOAD_QUOTA_ENDPOINT = '/me?fields=upload_quota';
	const UPLOAD_ENDPOINT = '/me/videos?fields=upload_link_secure,user.upload_quota';
	const VIDEO_ENDPOINT = '/videos/';

	protected $vimeo;

	protected $request_hold = false;
	protected $rate_limit_remaining;
	protected $rate_limit_reset;
	protected $upload_quota_remaining;

	public function __construct($client_id, $client_secret, $token) {

		$this->vimeo = new \Vimeo\Vimeo($client_id, $client_secret);
		
		$this->vimeo->setToken($token);

		$this->rate_limit_remaining = General::get('rate_limit_remaining');
		$this->rate_limit_reset = General::get('rate_limit_reset');
		$this->upload_quota_remaining = General::get('upload_quota_remaining');

	}

	public function getVideoID($data) {

		// 0 - ""
		// 1 - "videos"
		// 2 - video_id
		return explode('/', $data['video_uri'])[2];

	}

	public function getVideoUploadInfo($redirect_url) {

		if ($this->isUploadPossible()) {

			$response = $this->request(self::UPLOAD_ENDPOINT, ['type' => 'POST', 'redirect_url' => $redirect_url], 'POST');

			$upload_link_secure = $response['body']['upload_link_secure'];

			return compact('upload_link_secure');

		} 

		return null;

	}

	public function deleteVideo($id) {

		$this->request(self::VIDEO_ENDPOINT.$id, [], 'DELETE');

	}

	public function setVideoMetaData($id, $name, $description) {
		
		$this->request(self::VIDEO_ENDPOINT.$id.'?fields=user.upload_quota', ["name" => $name, "description" => $description], 'PATCH');

	}

	public function isUploadPossible() {

		if (!isset($this->upload_quota_remaining)) {

			$response = $this->request(self::UPLOAD_QUOTA_ENDPOINT, [], 'GET');

		}

		return $this->upload_quota_remaining > self::UPLOAD_LIMIT;
		
	}

	public function getEmbed($id) {
		return "<iframe id=\"video\" src=\"https://player.vimeo.com/video/$id\" width=\"1920\" height=\"1080\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
	}

	protected function request($endpoint, $arr, $method) {

		// If not set do the request
		if (!isset($this->upload_quota_remaining) || !isset($this->rate_limit_remaining) || !isset($this->rate_limit_reset)) {

			$response = $this->makeRequest($endpoint, $arr, $method);

		} else {

			// Turn on HOLD if the requests remaining is below limit
			if ($this->rate_limit_remaining <= self::RATE_LIMIT) {

				$this->request_hold = true;

			}

			// Turn off HOLD if the reset time has passed
			if (($this->request_hold) && (Carbon::parse($this->rate_limit_reset)->diffInMinutes(Carbon::now()) <= 0)) {

				$this->request_hold = false;

			}

			if (!$this->request_hold) {

				$response = $this->makeRequest($endpoint, $arr, $method);

			} else {

				$response = abort(502, 'Sorry, there\'s been too many requests recently. Please try again in 15 minutes.');

			}

		}

		return $response;

	}

	protected function makeRequest($endpoint, $arr, $method) {

		$response = $this->vimeo->request($endpoint, $arr, $method);

		$this->upload_quota_remaining = $this->getUploadQuotaRemaining($endpoint, $method, $response);
		$this->rate_limit_remaining = $response['headers']['X-RateLimit-Remaining'];
		$this->rate_limit_reset = Carbon::parse($response['headers']['X-RateLimit-Reset']);

		General::set('upload_quota_remaining', $this->upload_quota_remaining);
		General::set('rate_limit_remaining', $this->rate_limit_remaining);
		General::set('rate_limit_reset', $this->rate_limit_reset);

		return $response;

	}

	protected function getUploadQuotaRemaining($endpoint, $method, $response) {

		if (($endpoint === self::UPLOAD_QUOTA_ENDPOINT) && ($method !== 'DELETE')) {

			return $response['body']['upload_quota']['space']['free'];

		} else if ($method !== 'DELETE') {

			return $response['body']['user']['upload_quota']['space']['free'];

		}

		return $this->upload_quota_remaining;

	}
	
}
