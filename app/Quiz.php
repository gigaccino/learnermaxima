<?php

namespace App;

class Quiz extends Model {

	// The attributes that are mass assignable.
	protected $fillable = [

	    'title'
	    
	];

	// The attributes that should be shown for arrays.
	protected $visible = [

	    'id', 'title'

	];

	public function user() {

		return $this->belongsTo(User::class);
		
	}
    
	public function classroom() {

	    return $this->belongsTo(Classroom::class);
	    
	}

	public function questions() {

		return $this->hasMany(Question::class);

	}

	public function getNumberAttribute() {

		$classroom = $this->classroom;
	    $results = $classroom->quizzes->pluck('id')->search($this->id) + 1;

	    return $results ? $results : null;

	}

	public function getAnswersAttribute() {

		$answers_list = collect();

		$this->questions->each(function ($question, $index) use (&$answers_list) {

			$answer = auth()->user()->getAnswer($question);

			if (isset($answer)) {
				$answers_list->push($answer);
			}

		});

		return $answers_list;

	}

	public function getScoreAttribute() {

		if ($this->completed) {

			$score = 0;

			$this->answers->each(function ($answer, $index) use (&$score) {

				if ($answer->is_correct) {
					$score++;
				}

			});

			return $score;

		}

		return null;

	}

	public function getCompletedAttribute() {

		return $this->questions->count() === $this->answered_count;

	}

	public function getAnsweredCountAttribute() {

		return $this->answers->count();

	}

	public function addQuestion(Question $question) {

	    return $this->questions()->save($question);

	}

	public function getQuestionByNumber($question_number) {
		
		if ($question = $this->questions->get($question_number - 1)) {

		    return $question;
		    
		}

		abort(404, 'Question not found.');

	}
    
}
