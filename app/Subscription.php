<?php

namespace App;

use App\Events\SubscriptionRefunded;
use App\Mail\RenewalReminder;
use App\Payout;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class Subscription extends Model {

	const BASE_SUBSCRIPTION_LENGTH = 30;

	// The attributes that are mass assignable.
	protected $fillable = [

	   'lesson_id', 'teacher_id', 'payout_id', 'order_number', 'price', 'transaction_id', 'subscription_length', 'payment_date', 'refund_date', 'transaction_type', 'reminder_date'
	    
	];

	// The attributes that should be shown for arrays.
	protected $visible = [

	    'id', 'lesson_id', 'teacher_id', 'payout_id', 'order_number', 'price', 'transaction_id', 'payment_date', 'subscription_length', 'payment_date', 'refund_date', 'transaction_type', 'reminder_date'

	];

	public function user() {

		return $this->belongsTo(User::class);

	}

	public function lesson() {

	    return $this->belongsTo(Lesson::class);

	}

	public function payout() {

	    return $this->belongsTo(Payout::class);

	}

	public function getExpiryDateAttribute() {

		return Carbon::parse($this->payment_date)->addDays($this->subscription_length);

	}

	public function getPayoutAmountAttribute() {

		$multiplier = round($this->price / config('subscription.price'));

		if ($this->price > 0 && $multiplier > 0) {

			$unit_total = $this->price / $multiplier;

			$unit_teacher = $unit_total - config('subscription.gigaccino_fee') - config('subscription.payment_processor_fee');

			return $unit_teacher * $multiplier;

		}

		return 0;

	}

	public function getIsPaidOutAttribute() {

		return isset($this->payout_id);

	}

	public function getDaysRemainingAttribute() {

		if ($this->isActive()) {

			$days_since_last_payment = $this->getDaysSinceLastPayment();

			return $this->subscription_length - $days_since_last_payment;

		}

		return 0;

	}

	public function isActive() {

		$days_since_last_payment = $this->getDaysSinceLastPayment();
		$is_valid_transaction_type = $this->transaction_type === 'NEW' || $this->transaction_type === 'RENEW';
		$is_not_refunded = !$this->isRefunded();

		return ((($this->subscription_length >= $days_since_last_payment) ? true : false) && $is_valid_transaction_type && $is_not_refunded);

	}

	private function getDaysSinceLastPayment() {

		$payment_date = Carbon::parse($this->payment_date);
		$now = Carbon::now();
		$days_since_last_payment = $now->diff($payment_date)->days;

		return $days_since_last_payment;
		
	}

	public function payOff(Payout $payout) {
		
		return $this->update([
			'payout_id' => $payout->id
		]);

	}

	public function refund() {
		
		$credit = $this->user->credit + $this->subscription_length;
		$this->user->update(['credit' => $credit]);

		$this->refund_date = Carbon::now();
		$this->save();

		event(new SubscriptionRefunded($this));

		return $this;

	}

	public function isReminded() {
		
		return isset($this->reminder_date);

	}

	public function remindUserForRenewal() {

		if (!$this->isReminded()) {
			Mail::to($this->user)->queue(new RenewalReminder($this));

			$this->update([
				'reminder_date' => Carbon::now()
			]);
		}
		
	}

	public function isRefunded() {

		return isset($this->refund_date);

	}

	public function needsReminding() {
		
		return $this->isActive() && $this->days_remaining <= 1 && $this->reminder_date === null;

	}

	public static function getAllExpiring() {

		return Subscription::all()->filter(function($sub, $key) {
			return $sub->needsReminding();
		});

	}

	public static function get(User $user, Lesson $lesson) {

		if (static::exists($user, $lesson)) {
			
			return static::latest($user, $lesson);
			
		}

		return null;
		
	}

	public static function getOrderNumber(User $user, Lesson $lesson, $payment_gateway) {

		$now = Carbon::now()->format('YmdHis');

		return "$payment_gateway-$user->id-$lesson->id-$now";
		
	}

	public static function possible(User $user, Lesson $lesson) {

		return !$user->isSubscribed($lesson) && !$user->isOwner($lesson) && $user->isStudent();

	}

	public static function exists(User $user, Lesson $lesson) {

		// get latest subscription
		$subscription = static::latest($user, $lesson);

		if (isset($subscription)) {

			return $subscription->isActive();

		}

		return false;
	}

	private static function latest(User $user, Lesson $lesson) {

		return static::where([
			'user_id' => $user->id,
			'lesson_id' => $lesson->id
			])
		->get()
		->sortByDesc('payment_date')
		->first();

	}

}
