<?php

namespace App;

class General extends Model {
    
	// The attributes that are mass assignable.
	protected $fillable = [

	    'key', 'value'
	    
	];

	// The attributes that should be shown for arrays.
	protected $visible = [

	    'key', 'value'

	];

	public static function get($key) {

		$entry = self::where(['key' => $key])->first();

		if (isset($entry)) {

			return $entry->value;

		}

		return null;

	}

	public static function set($key, $value) {

		$general_list = self::where(compact('key'))->get();

		if ($general_list->isEmpty()) {

			$entry = new self(['key' => $key, 'value' => $value]);
			$entry->save();

		} else {

			$entry = $general_list->first();
			$entry->update(['value' => $value]);

		}

		return $entry;

	}

}
