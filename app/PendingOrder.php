<?php

namespace App;

class PendingOrder extends Model {

    // The attributes that are mass assignable.
    protected $fillable = [

        'order_number', 'price', 'subscription_length', 'transaction_type'
        
    ];

    // The attributes that should be shown for arrays.
    protected $visible = [

        'id', 'order_number', 'price', 'subscription_length', 'transaction_type'

    ];

    public static function set($order_number, $price, $subscription_length, $transaction_type) {

    	$pending_order_list = self::where(compact('order_number'))->get();

    	//Store Pending Order Number if not found (Duplicate Protection)
    	if ($pending_order_list->isEmpty()) {
    	    
    		return self::create(compact('order_number', 'price', 'subscription_length', 'transaction_type'));
    	    
    	}

    	return null;

    }

    public static function get($order_number) {

    	$pending_order_list = self::where(['order_number' => $order_number])->get();

    	if ($pending_order_list->isNotEmpty()) {

            $pending_order = $pending_order_list->first();

    		self::destroy($pending_order_list->toArray()); //Destroys all pending orders with a matching order number.

    		return $pending_order->toArray();
            
    	}

    	return null;

    }

}
