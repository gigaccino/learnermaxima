<?php

namespace App\Observers;

use App\Quiz;

class QuizObserver {
    /**
     * Listen to the Quiz deleting event.
     *
     * @param  \App\Quiz  $quiz
     * @return void
     */
    public function deleting(Quiz $quiz) {
        
        // Delete all Questions
        $quiz->questions->each(function($question) {
            $question->delete();
        });

    }
}