<?php

namespace App\Observers;

use App\User;
use App\Mail\Welcome;
use Illuminate\Support\Facades\Mail;

class UserObserver {

    public function created(User $user) {
        
        // Send email when user is created.
        Mail::to($user)->queue(new Welcome($user));

    }

    public function deleting(User $user) {

        $user->deleteQualifications();
        
        $user->deleteIdentification();

        $user->lessons->each(function($lesson) {
            $lesson->delete();
        });

        $user->reviews->each(function($review) {
            $review->delete();
        });

        $user->subscriptions->each(function($subscription) {
            $subscription->delete();
        });

        $user->progresses->each(function($progress) {
            $progress->delete();
        });

        $user->answers->each(function($answer) {
            $answer->delete();
        });

        $user->discussions->each(function($discussion) {
            $discussion->delete();
        });

        $user->payouts->each(function($payout) {
            $payout->delete();
        });
        
    }

}
