<?php

namespace App\Observers;

use App\Mail\Payout as PayoutMail;
use App\Payout;
use Illuminate\Support\Facades\Mail;

class PayoutObserver {

    public function updated(Payout $payout) {
        
        // Send email when user is paid.
        Mail::to($payout->user)->queue(new PayoutMail($payout));

    }

}
