<?php

namespace App\Observers;

use App\Classroom;

class ClassroomObserver {
    /**
     * Listen to the Classroom deleting event.
     *
     * @param  \App\Classroom  $classroom
     * @return void
     */
    public function deleting(Classroom $classroom) {
        
        // Delete all Assignments
        $classroom->assignments->each(function($assignment) {
            $assignment->delete();
        });

        // Delete all Quizzes
        $classroom->quizzes->each(function($quiz) {
            $quiz->delete();
        });

        // Delete all Discussions
        $classroom->discussions->each(function($discussion) {
            $discussion->delete();
        });

        // Delete all Progresses
        $classroom->progresses->each(function($progress) {
            $progress->delete();
        });

    }
}