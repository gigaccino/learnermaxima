<?php

namespace App\Observers;

use App\Question;

class QuestionObserver {
    /**
     * Listen to the Question deleting event.
     *
     * @param  \App\Question  $question
     * @return void
     */
    public function deleting(Question $question) {
        
        // Delete all Answers
        $question->answers->each(function($answer) {
            $answer->delete();
        });

    }
}