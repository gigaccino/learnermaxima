<?php

namespace App\Observers;

use App\Lesson;

class LessonObserver {
    /**
     * Listen to the Lesson deleting event.
     *
     * @param  \App\Lesson  $lesson
     * @return void
     */
    public function deleting(Lesson $lesson) {

        // Delete Associated Classrooms
        $lesson->classrooms->each(function($classroom) {
            $classroom->delete();
        });

        // Delete Associated Reviews
        $lesson->reviews->each(function($review) {
            $review->delete();
        });

        // Delete Associated Subscriptions
        $lesson->subscriptions->each(function($subscription) {
            $subscription->delete();
        });
        
    }
}