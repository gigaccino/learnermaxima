<?php

namespace App\Observers;

use App\Subscription;
use App\Mail\ThanksForSubscribing;
use Illuminate\Support\Facades\Mail;

class SubscriptionObserver {

    public function created(Subscription $subscription) {

    	$user = $subscription->user;
    	$lesson = $subscription->lesson;
    	$order_number = $subscription->order_number;
    	$transaction_id = $subscription->transaction_id;
    	$transaction_type = $subscription->transaction_type;
    	$days_remaining = $subscription->days_remaining;
        
        // Send email when user subscribes.
        Mail::to($user)->queue(new ThanksForSubscribing($user, $lesson, $order_number, $transaction_id, $transaction_type, $days_remaining));

    }

}
