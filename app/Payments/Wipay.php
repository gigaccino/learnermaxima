<?php

namespace App\Payments;

use App\PendingOrder;

class Wipay {

	const LIVE_URL = 'https://wipayfinancial.com/v1/gateway_live';
	const SANDBOX_URL = 'https://wipayfinancial.com/v1/gateway';

	const LIVE_URL_CASH = 'https://wipayfinancial.com/v1/cash_gateway';
	const SANDBOX_URL_CASH = 'https://wipayfinancial.com/v1/cash_gateway_s';

	protected $developer_id;
	protected $merchant_key;
	protected $return_url;
	protected $return_url_renew;
	protected $return_url_cash;
	protected $return_url_cash_renew;

	public function __construct($developer_id, $merchant_key) {

		$this->developer_id = $developer_id;
		$this->merchant_key = $merchant_key;
		$this->return_url = route('subscription.processWipayOrder');
		$this->return_url_renew = route('subscription.processWipayRenewOrder');
		$this->return_url_cash = route('subscription.processWipayCashOrder') . '?type=cash';
		$this->return_url_cash_renew = route('subscription.processWipayCashRenewOrder') . '?type=cash';

	}

	public function getUrl() {

		return (config('services.wipay.server') === 'LIVE') ? self::LIVE_URL : self::SANDBOX_URL;

	}

	public function getCashUrl() {

		return (config('services.wipay.server') === 'LIVE') ? self::LIVE_URL_CASH : self::SANDBOX_URL_CASH;

	}

	public function getOrderNumber($data) {

		return $data['order_id'];

	}

	public function getTransactionID($data) {

		return $data['transaction_id'];

	}

	public function getCashTransactionID($data) {

		return $data['order_number'];
		
	}

	public function getReason($data) {

		return $data['reasonDescription'];

	}

	public function getUserID($data) {

		$order_number = $this->getOrderNumber($data);
		$order_number_array = explode('-', $order_number);
		$user_id = $order_number_array[1];

		// 0 = payment gateway
		// 1 = user_id
		// 2 = lesson_id
		// 3 = time_stamp
		return (int) $user_id;

	}

	public function getLessonID($data) {

		$order_number = $this->getOrderNumber($data);
		$order_number_array = explode('-', $order_number);
		$lesson_id = $order_number_array[2];

		// 0 = payment gateway
		// 1 = user_id
		// 2 = lesson_id
		// 3 = time_stamp
		return (int) $lesson_id;

	}

	public function getOrderFormData($price, $order_number) {
		return collect([
            'total' => $price,
            'phone' => auth()->user()->phone,
            'email' => auth()->user()->email,
            'name' => auth()->user()->name,
            'order_id' => $order_number,
            'developer_id' => $this->developer_id,
            'return_url' => $this->return_url,
            'return_url_renew' => $this->return_url_renew
        ]);
	}

	public function getCashOrderFormData($price, $order_number) {
		return collect([
            'total' => $price,
            'order_id' => $order_number,
            'developer_id' => $this->developer_id,
            'return_url' => $this->return_url_cash,
            'return_url_renew' => $this->return_url_cash_renew,
            'cancel_url' => $this->return_url_cash
        ]);
	}

	public function validateOrder($data, $verified_order_data) {

		$is_successful_order_status = $this->isSuccessfulOrderStatus($data);
		$is_valid_user_id = auth()->user()->id === $this->getUserID($data);

		$validated_order_details = $this->getValidatedOrderDetails($data, $verified_order_data);

		if (isset($validated_order_details) && $is_successful_order_status && $is_valid_user_id) {

			return $validated_order_details;

		}

		return null;

	}

	public function isSuccessfulOrderStatus($data) {

		if (isset($data['status'])) {

			return $data['status'] === 'success';

		}

		return false;

	}

	protected function getValidatedOrderDetails($data, $verified_order_data) {

		$order_number = $data['order_id'];
		$received_hash = $data['hash'];

		if (isset($verified_order_data)) {

			$price = $verified_order_data['price'];

			$hash = md5($order_number.$price.$this->merchant_key);
			
			return ($hash === $received_hash) ? $verified_order_data : null;

		}

		return null;

	}
	
}
