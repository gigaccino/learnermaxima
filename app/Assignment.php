<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class Assignment extends Model {

	const MAX_INSTRUCTIONS_LENGTH = 280;

	// The attributes that are mass assignable.
	protected $fillable = [

	    'classroom_id', 'title', 'instructions', 'questions', 'solutions', 'solutions_release_days'
	    
	];

	// The attributes that should be shown for arrays.
	protected $visible = [

	    'id', 'classroom_id', 'title', 'instructions', 'questions', 'solutions', 'solutions_release_days'

	];

	public function __construct(array $attributes = array(), $questions = null, $solutions = null) {

        // Eloquent
		parent::__construct($attributes);

		if (!isset($this->solutions_release_days) || !(isset($solutions))) {

			$this->solutions_release_days = 0;
			
		}

		if (isset($questions)) {

			$this->questions = self::saveQuestions($questions);

		}

		if (isset($solutions)) {

			$this->solutions = self::saveSolutions($solutions);

		}

	}

	public function user() {

		return $this->belongsTo(User::class);
		
	}
    
	public function classroom() {

	    return $this->belongsTo(Classroom::class);
	    
	}

	public function getNumberAttribute() {

	    $classroom = $this->classroom;

	    $results = $classroom->assignments->pluck('id')->search($this->id) + 1;

	    return $results ? $results : null;
	    
	}

	public function getQuestionsDownloadPathAttribute() {

		return env('SPACES_URL').'/'.$this->questions;

	}

	public function getSolutionsDownloadPathAttribute() {

		return env('SPACES_URL').'/'.$this->solutions;

	}

	public function getSolutionsReleaseDateAttribute() {
		
		if (isset($this->classroom->publish_date)) {

			$publish_date = Carbon::parse($this->classroom->publish_date);
			$release_date = $publish_date->addDays($this->solutions_release_days);
			return $release_date;

		}

		return null;

	}

	public function getIsSolutionsReleasedAttribute() {

		$now = Carbon::now();
		$release_date = $this->solutions_release_date;

		if (isset($release_date)) {

			return $release_date->lte($now);

		}

		return false;

	}

	public function deleteQuestions() {

		if (isset($this->questions)) {

			Storage::disk('spaces')->delete($this->questions);

			$this->update([
			    'questions' => null
			]);

		}

		return $this;

	}


	public function deleteSolutions() {

		if (isset($this->solutions)) {

			Storage::disk('spaces')->delete($this->solutions);

			$this->update([
			    'solutions' => null,
			    'solutions_release_days' => 0
			]);

		}

		return $this;

	}


	public static function getMaxInstructionsLength() {

		return self::MAX_INSTRUCTIONS_LENGTH;

	}

	private static function saveQuestions($questions) {

		$location = 'uploads/assignments/questions';
		$path = $location.'/'.$questions->hashName();

		return Storage::disk('spaces')->putFile($location, $questions, 'public') ? $path : null;

	}

	private static function saveSolutions($solutions) {

		$location = 'uploads/assignments/solutions';
		$path = $location.'/'.$solutions->hashName();

		return Storage::disk('spaces')->putFile($location, $solutions, 'public') ? $path : null;

	}
    
}
