<?php

namespace App;

use App\Videos\Vimeo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class Classroom extends Model {

    const MAX_OBJECTIVE_LENGTH = 280;

	protected $vimeo;

	// The attributes that are mass assignable.
	protected $fillable = [

	    'title', 'objective', 'video', 'attachment', 'upload_link_secure', 'publish_date'
	    
	];

	// The attributes that should be shown for arrays.
	protected $visible = [

	    'id', 'title', 'objective', 'video', 'attachment', 'upload_link_secure', 'publish_date'

	];

	public function __construct(array $attributes = array(), $attachment = null) {

        // Eloquent
		parent::__construct($attributes);

	    $this->vimeo = resolve(Vimeo::class);

        if (isset($attachment)) {

            $this->attachment = self::saveAttachment($attachment);

        }

	}

    public function lesson() {

        return $this->belongsTo(Lesson::class);
        
    }

    public function progresses() {

        return $this->hasMany(Progress::class);

    }

    public function assignments() {

        return $this->hasMany(Assignment::class);

    }

    public function quizzes() {

        return $this->hasMany(Quiz::class);

    }

    public function discussions() {

        return $this->hasMany(Discussion::class);

    }

    public function getNumberAttribute() {

        $lesson = $this->lesson;

        $results = $lesson->classrooms->pluck('id')->search($this->id) + 1;

        return $results ? $results : null;
        
    }

    public function getAttachmentDownloadPathAttribute() {

        return env('SPACES_URL').'/'.$this->attachment;

    }

    public function getProgressCompletedAttribute() {

        if (auth()->user() !== null) {

            $progress = Progress::where(['user_id' => auth()->user()->id, 'classroom_id' => $this->id])->first();

            return isset($progress);

        }

        return false;

    }

    public function getRootDiscussionsAttribute() {

        return $this->discussions->filter(function($discussion, $index) {
            return !isset($discussion->replyto);
        });
        
    }

    public function isPublished() {

        return isset($this->publish_date);
        
    }

    public function publish() {
        
        $this->update(['publish_date' => Carbon::now()]);
        $this->lesson->updateLatestPublishDate();
        return $this;

    }

    public function unpublish() {
        
        $this->update(['publish_date' => null]);
        $this->lesson->updateLatestPublishDate();
        return $this;

    }

    public function getAssignmentByNumber($assignment_number) {

        if ($assignment = $this->assignments->get($assignment_number - 1)) {
            return $assignment;
        }
        
        abort(404, 'Assignment not found.');

    }

    public function getQuizByNumber($quiz_number) {

        if ($quiz = $this->quizzes->get($quiz_number - 1)) {

            return $quiz;
            
        }
        
        abort(404, 'Quiz not found.');

    }

    public function getDiscussionByNumber($discussion_number) {

        if ($discussion = $this->discussions->get($discussion_number - 1)) {
            return $discussion;
        }
        
        abort(404, 'Discussion not found.');

    }

    public function getVideoUploadLinkSecure($return_route_string) {
    	
    	if (!isset($this->upload_link_secure)) {

    	    $upload_info = $this->vimeo->getVideoUploadInfo(route($return_route_string, ['classroom' => $this->id]));
    	    $this->update($upload_info);

    	}

    	return $this->upload_link_secure;

    }

    public function addAssignment(Assignment $assignment) {

        return $this->assignments()->save($assignment);

    }

    public function addQuiz(Quiz $quiz) {

        return $this->quizzes()->save($quiz);

    }

    public function updateVideoData($video_data) {

    	// Get Video ID
    	$video_id = $this->vimeo->getVideoID($video_data);

    	// Associate with Classroom
    	$this->update([
    	        'video' => $video_id
    	    ]);

    	// Get relevant variables
    	$lesson = $this->lesson;
    	$user = $lesson->user;

    	// Format Video Title and Description
    	$video_title = "Classroom $this->id";
    	$video_description = "Classroom $this->id taught by User $user->id in Lesson $lesson->id";

    	// Set Video Title and Description
    	$this->vimeo->setVideoMetaData($video_id, $video_title, $video_description);
    	
    }

    public function getVideoEmbed() {
        return $this->vimeo->getEmbed($this->video);
    }

    public function deleteVideo() {

    	if (isset($this->video)) {

    		$this->vimeo->deleteVideo($this->video);

    		$this->update([
    		    'video' => null,
    		    'upload_link_secure' => null
    		]);

    	}

    	return $this;
    	
    }

    public function deleteAttachment() {

        if (isset($this->attachment)) {

            Storage::disk('spaces')->delete($this->attachment);

            $this->update([
                'attachment' => null
            ]);

        }

        return $this;
        
    }

    public function uploadAttachment($file) {
        
        $file_uri =  self::saveAttachment($file);

        $this->update([
            'attachment' => $file_uri
        ]);

    }

    public static function getMaxObjectiveLength() {

        return self::MAX_OBJECTIVE_LENGTH;

    }

    private static function saveAttachment($attachment) {

        $location = 'uploads/classrooms/attachments';
        $path = $location.'/'.$attachment->hashName();

        return Storage::disk('spaces')->putFile($location, $attachment, 'public') ? $path : null;

    }

}
