<?php

namespace App\Providers;

use App\Classroom;
use App\Defaults\Bank;
use App\Defaults\Level;
use App\Defaults\PaymentMethod;
use App\Defaults\PayoutMethod;
use App\Defaults\Review;
use App\Defaults\Subject;
use App\Defaults\SubscriptionLength;
use App\Lesson;
use App\Observers\ClassroomObserver;
use App\Observers\LessonObserver;
use App\Observers\PayoutObserver;
use App\Observers\QuestionObserver;
use App\Observers\QuizObserver;
use App\Observers\SubscriptionObserver;
use App\Observers\UserObserver;
use App\Payments\Wipay;
use App\Payout;
use App\Question;
use App\Quiz;
use App\Subscription;
use App\User;
use App\Videos\Vimeo;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {

        // Fix for MariaDB
        Schema::defaultStringLength(191);

        // Observers
        User::observe(UserObserver::class);
        Subscription::observe(SubscriptionObserver::class);
        Classroom::observe(ClassroomObserver::class);
        Lesson::observe(LessonObserver::class);
        Quiz::observe(QuizObserver::class);
        Question::observe(QuestionObserver::class);
        Payout::observe(PayoutObserver::class);
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {

        $this->app->singleton(Vimeo::class, function() {
            return new Vimeo(config('services.vimeo.client_id'), config('services.vimeo.client_secret'), config('services.vimeo.token'));
        });

        $this->app->singleton(Wipay::class, function() {
            return new Wipay(config('services.wipay.developer_id'), config('services.wipay.merchant_key'));
        });

        $this->app->singleton(Subject::class, function() {
            return new Subject();
        });

        $this->app->singleton(Level::class, function() {
            return new Level();
        });

        $this->app->singleton(Review::class, function() {
            return new Review();
        });

        $this->app->singleton(PaymentMethod::class, function() {
            return new PaymentMethod();
        });

        $this->app->singleton(Bank::class, function() {
            return new Bank();
        });

        $this->app->singleton(PayoutMethod::class, function() {
            return new PayoutMethod();
        });

        $this->app->singleton(SubscriptionLength::class, function() {
            return new SubscriptionLength();
        });

    }
    
}
