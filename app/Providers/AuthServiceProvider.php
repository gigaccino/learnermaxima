<?php

namespace App\Providers;

use App\Answer;
use App\Assignment;
use App\Classroom;
use App\Discussion;
use App\Lesson;
use App\Policies\AnswerPolicy;
use App\Policies\AssignmentPolicy;
use App\Policies\ClassroomPolicy;
use App\Policies\DiscussionPolicy;
use App\Policies\LessonPolicy;
use App\Policies\QuestionPolicy;
use App\Policies\QuizPolicy;
use App\Policies\ReviewPolicy;
use App\Policies\SubscriptionPolicy;
use App\Policies\UserPolicy;
use App\Question;
use App\Quiz;
use App\Review;
use App\Subscription;
use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
        Lesson::class => LessonPolicy::class,
        Classroom::class => ClassroomPolicy::class,
        Review::class => ReviewPolicy::class,
        Subscription::class => SubscriptionPolicy::class,
        Assignment::class => AssignmentPolicy::class,
        Quiz::class => QuizPolicy::class,
        Question::class => QuestionPolicy::class,
        Answer::class => AnswerPolicy::class,
        Discussion::class => DiscussionPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
