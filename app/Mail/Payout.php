<?php

namespace App\Mail;

use App\Payout as PayoutObj;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Payout extends Mailable implements ShouldQueue
{
    public $user;
    public $payout;

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(PayoutObj $payout)
    {
        $this->payout = $payout;
        $this->user = $payout->user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.payout');
    }
}
