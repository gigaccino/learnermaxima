<?php

namespace App\Mail;

use App\User;
use App\Lesson;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ThanksForSubscribing extends Mailable implements ShouldQueue
{
    public $user, $lesson, $order_number, $transaction_id, $transaction_type, $days_remaining;

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Lesson $lesson, $order_number, $transaction_id, $transaction_type, $days_remaining)
    {
        $this->user = $user;
        $this->lesson = $lesson;
        $this->order_number = $order_number;
        $this->transaction_id = $transaction_id;
        $this->transaction_type = $transaction_type;
        $this->days_remaining = $days_remaining;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.thanks-for-subscribing');
    }
}
