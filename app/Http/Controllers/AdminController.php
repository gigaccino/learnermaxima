<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\Subscription;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdminController extends Controller {

	public function __construct() {

	    $this->middleware('auth');

	}
    
	public function home() {

		$this->authorize('admin', User::class);
		return redirect()->route('admin.users');

	}

	public function users() {

		$this->authorize('admin', User::class);
		$show = request()->show;

		switch ($show) {

			case 'owed':
				$users = User::orderBy('created_at', 'desc')->get()->where('amount_owed', '>', 0);
				$show_description = 'Users with amounts owed';
				break;
			case 'pending_upgrade':
				$users = User::orderBy('created_at', 'desc')->where('pending_upgrade', 1)->get();
				$show_description = 'Users waiting to have their accounts upgraded to a Teacher\'s account';
				break;
			default:
				$users = User::orderBy('created_at', 'desc')->paginate(10);
				$show_description = 'all Users';
				break;

		}

		$table_headings = ['name', 'phone', 'privilege', 'credit', 'identification', 'qualifications', 'amount owed'];

		return view('admin.users', compact('table_headings', 'users', 'show_description'));
		
	}

	public function lessons() {

		$this->authorize('admin', User::class);
		$show = request()->show;

		switch ($show) {

			case 'disabled':
				$lessons = Lesson::where('subscriptions_disabled', 1)->orderBy('created_at', 'desc')->get();
				$show_description = 'disabled Lessons';
				break;
			case 'inactive':
				$lessons = Lesson::where('last_publish_date', '<=', Carbon::now()->subDays(14))->orderBy('created_at', 'desc')->get();
				$show_description = 'Lessons with over 14 days of inactivity';
				break;
			default:
				$lessons = Lesson::orderBy('created_at', 'desc')->paginate(10);
				$show_description = 'all Lessons';
				break;

		}

		$table_headings = ['lesson', 'teacher', 'last updated (trinidad time)', 'subscriptions', 'status'];

		return view('admin.lessons', compact('table_headings', 'lessons', 'show_description'));
		
	}

	public function subscriptions() {

		$this->authorize('admin', User::class);

		$table_headings = ['student', 'lesson', 'teacher', 'order number', 'payment date (trinidad time)', 'subscription length (days)', 'status', 'transaction type'];

		$subscriptions = Subscription::where('payment_date', '>=', Carbon::now()->subDays(50))->orderBy('created_at', 'desc')->paginate(10);
		$show_description = 'all Subscriptions within the last 30 days';

		return view('admin.subscriptions', compact('table_headings', 'subscriptions', 'show_description'));
		
	}

	public function find() {
		
		$this->authorize('admin', User::class);

		$type = request()->type;
		$search_query = request()->search_query;

		switch ($type) {

			case 'user':
				$user = User::where('email', $search_query)->first();
				if (isset($user)) {

					$users = collect([$user]);
					$show_description = "Results for '$search_query'";

					$table_headings = ['name', 'phone', 'privilege', 'identification', 'qualifications', 'amount owed'];

					return view('admin.users', compact('table_headings', 'users', 'show_description'));
				} else {
					return view ('admin.no-results');
				}
				break;
			case 'subscription':
				$subscription = Subscription::withTrashed()->where('order_number', $search_query)->first();
				if (isset($subscription)) {

					$subscriptions = collect([$subscription]);
					$show_description = "Results for '$search_query'";

					$table_headings = ['student', 'lesson', 'teacher', 'order number', 'payment date (trinidad time)', 'subscription length (days)', 'status', 'transaction type'];

					return view('admin.subscriptions', compact('table_headings', 'subscriptions', 'show_description'));
				} else {
					return view ('admin.no-results');
				}
				break;
			default:
				return view ('admin.no-results');
				break;

		}

		return view ('admin.no-results');

	}

}
