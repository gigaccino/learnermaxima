<?php

namespace App\Http\Controllers;

use App\Defaults\Level;
use App\Defaults\Subject;
use App\Lesson;
use App\Review;
use App\Subscription;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use JavaScript;


class LessonController extends Controller {

    protected $subject_list;
    protected $level_list;

    public function __construct() {

        $this->middleware('auth', ['except' => ['index', 'show']]);
        $this->subject_list = (resolve(Subject::class))->getList();
        $this->level_list = (resolve(Level::class))->getList();
    }

    public function index() {

        $is_all = true;
        $is_search = false;

        $subject = request()->subject;
        $level = request()->level;

        $subject_list = $this->subject_list;
        $level_list = $this->level_list;

        // Check if search or index
        if (isset(request()->search_query)) {
            $search_query = request()->search_query;
            $relevant_lessons = Lesson::search($search_query);
            $is_search = true;
        } else {
            $relevant_lessons = Lesson::latest();
        }

        // If Valid Subject and Level
        if (($subject_list->contains($subject)) && ($level_list->contains($level))) {
            // Fetch Lesson Collection according to specified Subject and Level
            $lesson_collection = $relevant_lessons->where('subject', $subject)->where('level', $level)->get();
            $is_all = false;
        } elseif ($level_list->contains($level)) {
            // Else.. if Level Valid (invalid subject)
            $lesson_collection = $relevant_lessons->where('level', $level)->get();
            $is_all = false;
        } else if ($subject_list->contains($subject)) {
            // Else.. if Subject Valid (invalid level)
            $lesson_collection = $relevant_lessons->where('subject', $subject)->get();
            $is_all = false;
        } else {
            //Both invalid
            $lesson_collection = $relevant_lessons->get();
        }

        // Sort Lesson Collection, and paginate accordingly
        switch (request()->sort) {
            case 'highest-rated':
                $sorted_lesson_collection = $lesson_collection->sortByDesc(function($lesson, $key) {
                    return $lesson->rating;
                });

                // Filter out Lessons that user can not view/subscribe
                $sorted_lesson_collection = $sorted_lesson_collection->filter(function ($lesson, $key) {
                    if (Auth::check()) {
                        return auth()->user()->can('view', [Lesson::class, $lesson]);
                    } else {
                        return !$lesson->isSubscriptionsDisabled();
                    }
                });

                $lessons = $this->paginate($sorted_lesson_collection, ['sort' => 'highest-rated']);
                break;
            case 'oldest-to-newest':
                $sorted_lesson_collection = $lesson_collection->sortBy('created_at');

                // Filter out Lessons that user can not view/subscribe
                $sorted_lesson_collection = $sorted_lesson_collection->filter(function ($lesson, $key) {
                    if (Auth::check()) {
                        return auth()->user()->can('view', [Lesson::class, $lesson]);
                    } else {
                        return !$lesson->isSubscriptionsDisabled();
                    }
                });

                $lessons = $this->paginate($sorted_lesson_collection, ['sort' => 'oldest-to-newest']);
                break;
            case 'newest-to-oldest':
                $sorted_lesson_collection = $lesson_collection->sortByDesc('created_at');

                // Filter out Lessons that user can not view/subscribe
                $sorted_lesson_collection = $sorted_lesson_collection->filter(function ($lesson, $key) {
                    if (Auth::check()) {
                        return auth()->user()->can('view', [Lesson::class, $lesson]);
                    } else {
                        return !$lesson->isSubscriptionsDisabled();
                    }
                });

                $lessons = $this->paginate($sorted_lesson_collection, ['sort' => 'newest-to-oldest']);
                break;
            default:
                $sorted_lesson_collection = $lesson_collection->sortByDesc('created_at');

                // Filter out Lessons that user can not view/subscribe
                $sorted_lesson_collection = $sorted_lesson_collection->filter(function ($lesson, $key) {
                    if (Auth::check()) {
                        return auth()->user()->can('view', [Lesson::class, $lesson]);
                    } else {
                        return !$lesson->isSubscriptionsDisabled();
                    }
                });

                $lessons = $this->paginate($sorted_lesson_collection);
        }

        return view('lesson.index', compact('lessons', 'subject_list', 'level_list', 'is_search', 'is_all'));

    }

    public function show(Lesson $lesson) {

        $reviews = $this->paginateReviews($lesson->reviews->sortByDesc('created_at'), $lesson->pretty_id);

        if (Auth::check()) {
            $user_review = Review::get(auth()->user(), $lesson);
            $subscription = Subscription::get(auth()->user(), $lesson);
        } else {
            $user_review = null;
            $subscription = null;
        }

        return view('lesson.show', compact('lesson', 'reviews', 'user_review', 'subscription'));

    }

    public function create() {

        $this->authorize('create', Lesson::class);

        $subject_list = $this->subject_list;
        $level_list = $this->level_list;

        return view('lesson.create', compact('subject_list', 'level_list'));

    }

    public function store() {

        $this->authorize('create', Lesson::class);

        $lesson_data = request()->validate([
                'photo' => 'image|max:25000',
                'subject' => 'required|in:'.implode(',', $this->subject_list->toArray()),
                'level' => 'required|in:'.implode(',', $this->level_list->toArray()),
                'description' => 'required|max:'.Lesson::getMaxDescriptionLength().'|profane:'.resource_path('lang/en/profane_local.php')
            ]);


        $lesson_data = $lesson_data + [
            'price' => config('subscription.price'),
            'subscriptions_disabled' => 1,
            'last_publish_date' => Carbon::now()
        ];

        $lesson = auth()->user()->addLesson(new Lesson($lesson_data, request()->photo));

        alert()->success('Your lesson has been created. It\'s now time to add your first Classroom and upload a video!', 'Lesson Created');

        return redirect()->route('lesson.myLessons');

    }

    public function edit(Lesson $lesson) {

        $this->authorize('update', [Lesson::class, $lesson]);

        $subject_list = $this->subject_list;
        $level_list = $this->level_list;

        JavaScript::put(['forms' => [

            [
            'id' => 'delete-lesson',
            'swal_submit' => [
                'title' => 'Deleting Lesson',
                'text' => 'Are you sure you\'d like to delete your Lesson? ALL ASSOCIATED DATA WILL BE LOST.',
                'icon' => 'warning',
                'danger' => true,
                ]
            ],

            [
            'id' => 'delete-photo',
            'swal_submit' => [
                'title' => 'Deleting Lesson Photo',
                'text' => 'Are you sure you\'d like to delete your Lesson Photo? The default Lesson Photo will be set until you upload a new Lesson Photo.',
                'icon' => 'warning',
                'danger' => true,
                ]
            ],

            [
            'id' => 'replace-photo',
            'swal_submit' => [
                'title' => 'Replacing Lesson Photo',
                'text' => 'Are you sure you\'d like to replace your Lesson Photo? The current Lesson Photo will be changed.',
                'icon' => 'warning',
                'danger' => false,
                ]
            ]

        ]]);
        
        return view('lesson.edit', compact('lesson', 'subject_list', 'level_list'));

    }

    public function update(Lesson $lesson) {

        $this->authorize('update', [Lesson::class, $lesson]);

        $lesson_data = request()->validate([
                'subject' => 'required|in:'.implode(',', $this->subject_list->toArray()),
                'level' => 'required|in:'.implode(',', $this->level_list->toArray()),
                'description' => 'required|max:'.Lesson::getMaxDescriptionLength().'|profane:'.resource_path('lang/en/profane_local.php'),
            ]);
        
        $lesson->update($lesson_data);

        alert()->success('Your lesson has been updated.', 'Lesson Updated');

        return redirect()->route('lesson.show', ['lesson' => $lesson->pretty_id]);

    }

    public function destroy(Lesson $lesson) {

        $this->authorize('delete', [Lesson::class, $lesson]);

        $lesson->delete();

        alert()->success('Your lesson has been deleted.', 'Lesson Deleted');

        return redirect()->route('lesson.myLessons');

    }

    public function myLessons() {

        $user = auth()->user();

        $this->authorize('create', Lesson::class);

        $my_subscriptions_link = route('subscription.mySubscriptions');

        $lessons = $user->lessons->sortByDesc('created_at');

        flash("If you have active subscriptions which were paid for before your account was upgraded to a Teacher's account, you can find them <a href=\"$my_subscriptions_link\" class=\"alert-link\">here</a>.")->important();
        
        return view('lesson.my-lessons', compact('user', 'lessons'));

    }

    public function storePhoto(Lesson $lesson) {

        $this->authorize('uploadPhoto', [Lesson::class, $lesson]);

        $lesson_data = request()->validate([
                'photo' => 'image|max:25000'
            ]);

        $lesson->uploadPhoto(request()->photo);

        alert()->success('Your photo has been added to your lesson.', 'Photo Uploaded');

        return redirect()->route('lesson.show', ['lesson' => $lesson->pretty_id]);
        
    }

    public function replacePhoto(Lesson $lesson) {

        $this->authorize('deletePhoto', [Lesson::class, $lesson]);
        
        $lesson->deletePhoto();

        return $this->storePhoto($lesson);

    }

    public function deletePhoto(Lesson $lesson) {

        $this->authorize('deletePhoto', [Lesson::class, $lesson]);
        
        $lesson->deletePhoto();

        alert()->success('Your photo has been deleted from your lesson.', 'Photo Deleted');

        return redirect()->route('lesson.show', ['lesson' => $lesson->pretty_id]);

    }

    public function disableSubscriptions(Lesson $lesson) {

        $this->authorize('disableSubscriptions', [Lesson::class, $lesson]);
        
        $lesson->disableSubscriptions();

        alert()->success('You have disabled subscriptions for this Lesson.', 'Lesson Disabled');

        return redirect()->back();

    }

    public function enableSubscriptions(Lesson $lesson) {

        $this->authorize('enableSubscriptions', [Lesson::class, $lesson]);
        
        $lesson->enableSubscriptions();

        alert()->success('You have enabled subscriptions for this Lesson.', 'Lesson Enabled');

        return redirect()->back();

    }

    protected function paginate($items, $append_array = []) {

        $per_page = config('app.pagination');

        //Get current page form url e.g. &page=1
        $current_page = LengthAwarePaginator::resolveCurrentPage();

        //Slice the collection to get the items to display in current page
        $current_page_items = $items->slice(($current_page - 1) * $per_page, $per_page);

        //Create our paginator and pass it to the view
        $lessons = new LengthAwarePaginator($current_page_items, count($items), $per_page);

        // Append appropriate query strings
        $lessons->setPath('lessons');
        $lessons->appends(['subject' => request()->subject]);
        $lessons->appends(['level' => request()->level]);

        if (isset(request()->search_query)) {
            $search_query = request()->search_query;
            $lessons->appends(['search_query' => $search_query]);
        }

        if (!empty($append_array)) {
            $lessons->appends($append_array);
        }

        return $lessons;
        
    }

    protected function paginateReviews($items, $path) {

        $per_page = config('app.pagination');

        //Get current page form url e.g. &page=1
        $current_page = LengthAwarePaginator::resolveCurrentPage();

        //Slice the collection to get the items to display in current page
        $current_page_items = $items->slice(($current_page - 1) * $per_page, $per_page);

        //Create our paginator and pass it to the view
        return (new LengthAwarePaginator($current_page_items, count($items), $per_page))->setPath($path);
        
    }
    
}
