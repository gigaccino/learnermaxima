<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\Lesson;
use Illuminate\Pagination\LengthAwarePaginator;
use JavaScript;

class ClassroomController extends Controller {

    public function __construct() {
        
        $this->middleware('auth');

    }

    public function index(Lesson $lesson) {

    	return redirect()->route('lesson.show', ['lesson' => $lesson->pretty_id]);

    }

    public function show(Lesson $lesson, $classroom_number) {

        $classroom = $lesson->getClassroomByNumber($classroom_number);
        $next_classroom = $lesson->getNextClassroom($classroom_number);

        $this->authorize('view', [Classroom::class, $classroom]);

        $root_discussions = $this->paginate($classroom->root_discussions->sortByDesc('created_at'), $classroom_number);

        JavaScript::put(
            [
                'classroom' => [ 
                    'id' => $classroom->id 
                ]
            ]
        );

        return view('classroom.show', compact('lesson', 'classroom', 'classroom_number', 'root_discussions', 'next_classroom'));

    }

    public function create(Lesson $lesson) {

       $this->authorize('create', [Classroom::class, $lesson]);

        return view('classroom.create', compact('lesson'));
        
    }

    public function store(Lesson $lesson) {

        $this->authorize('create', [Classroom::class, $lesson]);

        $classroom_data = request()->validate([
                'title' => 'required|max:30|profane:'.resource_path('lang/en/profane_local.php'),
                'objective' => 'required|max:'.Classroom::getMaxObjectiveLength().'|profane:'.resource_path('lang/en/profane_local.php'),
                'attachment' => 'file|max:25000|mimes:doc,docx,ppt,pptx,pdf,txt,rtf,jpg,jpeg,bmp,png,zip',
            ]);

    	$classroom = $lesson->addClassroom(new Classroom($classroom_data, request()->attachment));

        // Alert user that video has been uploaded.
        alert()->success('Your classroom has been created. Please upload your video.', 'Classroom created');

        return redirect()->route('classroom.uploadVideo', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number]);
        
    }

    public function edit(Lesson $lesson, $classroom_number) {

        $classroom = $lesson->getClassroomByNumber($classroom_number);

        $this->authorize('update', [Classroom::class, $classroom]);

        JavaScript::put(['forms' => [

            [
            'id' => 'delete-video',
            'swal_submit' => [
                'title' => 'Deleting Classroom Video',
                'text' => 'Are you sure you\'d like to delete your Classroom Video? Your Classroom will be unpublished until you upload a new video, and publish it.',
                'icon' => 'warning',
                'danger' => true,
                ]
            ],

            [
            'id' => 'delete-classroom',
            'swal_submit' => [
                'title' => 'Deleting Classroom',
                'text' => 'Are you sure you\'d like to delete your Classroom? ALL ASSOCIATED DATA WILL BE LOST.',
                'icon' => 'warning',
                'danger' => true,
                ]
            ],

        ]]);

        return view('classroom.edit', compact('lesson', 'classroom', 'classroom_number'));

    }

    public function update(Classroom $classroom) {

        $this->authorize('update', [Classroom::class, $classroom]);

        $classroom_data = request()->validate([
                'title' => 'required|max:30|profane:'.resource_path('lang/en/profane_local.php'),
                'objective' => 'required|max:'.Classroom::getMaxObjectiveLength().'|profane:'.resource_path('lang/en/profane_local.php'),
            ]);

        $lesson = $classroom->lesson;

        $classroom->update($classroom_data);

        alert()->success('Your classroom has been updated.', 'Classroom Updated');

        return redirect()->route('classroom.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number]);

    }

    public function destroy(Classroom $classroom) {

        $this->authorize('delete', [Classroom::class, $classroom]);

        $lesson = $classroom->lesson;

        $classroom->delete();

        alert()->success('Your classroom has been deleted.', 'Classroom Deleted');

        return redirect()->route('lesson.show', ['lesson' => $lesson->pretty_id]);
        
    }

    public function uploadVideo(Lesson $lesson, $classroom_number) {

        $classroom = $lesson->getClassroomByNumber($classroom_number);

        $this->authorize('uploadVideo', [Classroom::class, $classroom]);

        $upload_link_secure = $classroom->getVideoUploadLinkSecure('classroom.uploadVideoComplete');

        return view('classroom.upload-video', compact('classroom', 'lesson', 'classroom_number', 'upload_link_secure'));
        
    }

    public function uploadVideoComplete(Classroom $classroom) {

        $this->authorize('uploadVideo', [Classroom::class, $classroom]);

        $video_data = request()->validate([
                'video_uri' => 'required'
            ]);

        $classroom->updateVideoData($video_data);

        alert()->success('Your video has been uploaded and is currently being processed. It will be available shortly.', 'Video Uploaded');

        return redirect()->route('classroom.edit', ['lesson_id' => $classroom->lesson->pretty_id, 'classroom_number' => $classroom->number]);

    }

    public function deleteVideo(Classroom $classroom) {

        $this->authorize('deleteVideo', [Classroom::class, $classroom]);
        
        $classroom->deleteVideo();
        $classroom->unpublish();

        alert()->success('Your video has been deleted from your classroom.', 'Video deleted');

        return redirect()->route('classroom.edit', ['lesson_id' => $classroom->lesson->pretty_id, 'classroom_number' => $classroom->number]);

    }

    public function storeAttachment(Classroom $classroom) {
        
        $this->authorize('uploadAttachment', [Classroom::class, $classroom]);

        $classroom_data = request()->validate([
                'attachment' => 'file|max:25000|mimes:doc,docx,ppt,pptx,pdf,txt,rtf,jpg,jpeg,bmp,png,zip',
            ]);

        $classroom->uploadAttachment(request()->attachment);

        alert()->success('Your Notes has been added to your Classroom.', 'Notes Uploaded');

        return redirect()->route('classroom.show', ['lesson_id' => $classroom->lesson->pretty_id, 'classroom_number' => $classroom->number]);
    }

    public function deleteAttachment(Classroom $classroom) {

        $this->authorize('deleteAttachment', [Classroom::class, $classroom]);

        $classroom->deleteAttachment();

        alert()->success('Your Notes has been deleted from your Classroom.', 'Notes Deleted');

        return redirect()->route('classroom.show', ['lesson_id' => $classroom->lesson->pretty_id, 'classroom_number' => $classroom->number]);    
    }

    public function publish(Classroom $classroom) {
        
        $this->authorize('publish', [Classroom::class, $classroom]);

        $classroom->publish();

        alert()->success('Your Classroom has been published.', 'Classroom Published');

        return redirect()->route('classroom.show', ['lesson_id' => $classroom->lesson->pretty_id, 'classroom_number' => $classroom->number]);  

    }

    public function unpublish(Classroom $classroom) {
        
        $this->authorize('unpublish', [Classroom::class, $classroom]);

        $classroom->unpublish();

        alert()->success('Your Classroom has been unpublished and hidden from students.', 'Classroom Unpublished');

        return redirect()->route('classroom.show', ['lesson_id' => $classroom->lesson->pretty_id, 'classroom_number' => $classroom->number]);

    }

    protected function paginate($items, $path) {

        $per_page = config('app.pagination');

        //Get current page form url e.g. &page=1
        $current_page = LengthAwarePaginator::resolveCurrentPage();

        //Slice the collection to get the items to display in current page
        $current_page_items = $items->slice(($current_page - 1) * $per_page, $per_page);

        //Create our paginator and pass it to the view
        return (new LengthAwarePaginator($current_page_items, count($items), $per_page))->setPath($path);
        
    }

}
