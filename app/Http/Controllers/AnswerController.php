<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Quiz;
use Illuminate\Http\Request;

class AnswerController extends Controller {

	public function __construct() {
	    
	    $this->middleware('auth');
	    
	}

	public function store(Quiz $quiz) {

		$this->authorize('create', [Answer::class, $quiz]);

		for ($i = 1; $i <= $quiz->questions->count(); $i++) {

		    $validation_array['answer_for_question_'.$i] = 'in:a,b,c,d';

		}

		$answer_data = request()->validate($validation_array);

		$answers_list = collect($answer_data);

		$quiz->questions->each(function ($question, $index) use (&$answers_list) {

        	$answer = $answers_list->get('answer_for_question_'.($index + 1));

        	if (isset($answer)) {

        		$answer = auth()->user()->addAnswer(new Answer([
        			'question_id' => $question['id'],
        			'answer' => $answer
        		]));

        	}
        	
        });

		$classroom = $quiz->classroom;
		$lesson = $classroom->lesson;

		alert()->success('Your answers were submitted. You can now view the results for all submitted answers.', 'Answers submitted');

		return redirect()->route('quiz.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number, 'quiz_number' => $quiz->number]);

	}

}
