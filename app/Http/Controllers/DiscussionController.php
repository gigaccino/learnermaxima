<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\Discussion;
use App\Lesson;
use Illuminate\Http\Request;
use JavaScript;

class DiscussionController extends Controller {

    public function __construct() {
        
        $this->middleware('auth');
        
    }

	public function create(Lesson $lesson, $classroom_number) {

		$classroom = $lesson->getClassroomByNumber($classroom_number);

        if (isset(request()->replyto)) {

            $replyto = Discussion::find(request()->replyto);

            return view('discussion.create', compact('lesson', 'classroom', 'replyto'));

        }

        return view('discussion.create', compact('lesson', 'classroom'));

    }

    public function store(Classroom $classroom) {

        $lesson = $classroom->lesson;

        $discussion_data = request()->validate([
            'body' => 'required|max:'.Discussion::getMaxBodyLength().'|profane:'.resource_path('lang/en/profane_local.php'),
            'replyto' => 'numeric'
        ]);

        $this->authorize('create', [Discussion::class, $classroom, $discussion_data]);

        $discussion_data = $discussion_data + ['classroom_id' => $classroom->id];

        $discussion = auth()->user()->addDiscussion(new Discussion($discussion_data));

        alert()->success('Your discussion has been posted.', 'Discussion Posted');
        
        return redirect()->route('classroom.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number]);

    }

    public function edit(Lesson $lesson, $classroom_number, $discussion_number) {

        $classroom = $lesson->getClassroomByNumber($classroom_number);
        $discussion = $classroom->getDiscussionByNumber($discussion_number);

        $this->authorize('update', [Discussion::class, $discussion]);

        JavaScript::put(['forms' => [

            [
            'id' => 'delete-discussion',
            'swal_submit' => [
                'title' => 'Deleting Discussion',
                'text' => 'Are you sure you\'d like to delete your Discussion for this Lesson?',
                'icon' => 'warning',
                'danger' => true,
                ]
            ]

        ]]);

        return view('discussion.edit', compact('lesson', 'classroom', 'discussion'));
        
    }

    public function update(Discussion $discussion) {

        $this->authorize('update', [Discussion::class, $discussion]);

        $classroom = $discussion->classroom;
        $lesson = $classroom->lesson;

        $discussion_data = request()->validate([
            'body' => 'required|max:'.Discussion::getMaxBodyLength().'|profane:'.resource_path('lang/en/profane_local.php'),
        ]);

        $discussion->update($discussion_data);

        alert()->success('Your discussion has been updated.', 'Discussion Updated');

        return redirect()->route('classroom.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number]);

    }

    public function destroy(Discussion $discussion) {

        $this->authorize('delete', [Discussion::class, $discussion]);

        $classroom = $discussion->classroom;
        $lesson = $classroom->lesson;

        $discussion->delete();

        alert()->success('Your discussion has been deleted.', 'Discussion Deleted');

        return redirect()->route('classroom.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number]);

    }
    
}
