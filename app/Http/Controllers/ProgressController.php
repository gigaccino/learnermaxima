<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\Progress;
use Illuminate\Http\Request;

class ProgressController extends Controller {
	
	public function __construct() {
	    
	    $this->middleware('auth');
	    
	}

    public function store(Classroom $classroom) {
    	
    	if (request()->completed) {

    		auth()->user()->addProgress(new Progress([
    			'classroom_id' => $classroom->id
    		]));

    	}

    }
}
