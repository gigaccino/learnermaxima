<?php

namespace App\Http\Controllers;

use App\Defaults\Bank;
use App\Defaults\PayoutMethod;
use App\Rules\CorrectPassword;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use JavaScript;

class UserController extends Controller {

	protected $bank_list;

	public function __construct() {

	    $this->middleware('auth');
	    $this->bank_list = resolve(Bank::class)->getList();
	    $this->payout_method_list = resolve(PayoutMethod::class)->getList();

	}

	public function show(User $user) {

		$this->authorize('view', User::class);

		return view('user.show', compact('user'));

	}

	public function edit(User $user) {

		$this->authorize('update', [User::class, $user->id]);

		JavaScript::put(['forms' => [

		    [
		    'id' => 'delete-user',
		    'swal_submit' => [
		        'title' => 'Deleting User',
		        'text' => 'Are you sure you\'d like to delete this User? ALL ASSOCIATED DATA WILL BE LOST.',
		        'icon' => 'warning',
		        'danger' => true,
		        ]
		    ],
		    [
		    'id' => 'delete-identification',
		    'swal_submit' => [
		        'title' => 'Deleting Identification',
		        'text' => 'Are you sure you\'d like to delete this User\'s Identification?',
		        'icon' => 'warning',
		        'danger' => true,
		        ]
		    ],
		    [
		    'id' => 'delete-qualifications',
		    'swal_submit' => [
		        'title' => 'Deleting Qualifications',
		        'text' => 'Are you sure you\'d like to delete this User\'s Qualifications?',
		        'icon' => 'warning',
		        'danger' => true,
		        ]
		    ]

		]]);


		if (!isset($user->payout_method)) {
			flash('Please update your Payout Details immediately.')->error()->important();
		} else {
			flash('Ensure that your Full Name, Phone Number and Payout Details are correct.')->warning()->important();
		}

		return view('user.edit', compact('user'));

	}

	public function update(User $user) {

		$this->authorize('update', [User::class, $user->id]);

		$user_data = request()->validate([
            'name' => 'required|string|max:30|profane:'.resource_path('lang/en/profane_local.php'),
            'phone' => 'required|numeric|digits_between:7,15'
        ]);

		$user->update($user_data);

		if (isset(request()->current_password)) {

			$password_data = request()->validate([
				'current_password' => ['required', new CorrectPassword],
	            'password' => 'required|string|min:6|confirmed',
			]);

	        auth()->user()->forceFill([
	            'password' => bcrypt($password_data['password']),
	            'remember_token' => Str::random(60),
	        ])->save();

	        alert()->success("Use your new password to log in from now on.", 'Password successfully changed');


		} else {

			alert()->success("Now get back to $user->action_string 😛", 'Account details updated');

		}

		return redirect()->route('user.edit', ['user' => $user->pretty_id]);

	}

	public function editProfile(User $user) {

		$this->authorize('update', [User::class, $user->id]);

		return view('user.edit-profile', compact('user'));

	}

	public function editPayoutDetails(User $user) {

		$this->authorize('updatePayoutDetails', [User::class, $user->id]);

		$bank_list = $this->bank_list;
		$payout_method_list = $this->payout_method_list;

		flash('DOUBLE CHECK your details below.')->warning()->important();

		return view('user.edit-payout', compact('user', 'bank_list', 'payout_method_list'));

	}

	public function updateProfile(User $user) {

		$this->authorize('update', [User::class, $user->id]);

		$user_data = request()->validate([
			'public_contact_email' => 'nullable|string|email|max:30|profane:'.resource_path('lang/en/profane_local.php'),
			'teaching_experience' => 'nullable|string|profane:'.resource_path('lang/en/profane_local.php').'|max:'.User::getMaxTeachingExperienceLength(),
            'tagline' => 'nullable|string|max:30|profane:'.resource_path('lang/en/profane_local.php'),
        ]);

        $user->update($user_data);

        if (!$user->isTeacher() && !$user->isAdmin() && isset($user->teaching_experience)) {
        	$user->update(['teaching_experience' => null]);
        }

        alert()->success("Now get back to $user->action_string 😛", 'Profile details updated');
		
        return redirect()->route('user.show', ['user' => $user->pretty_id]);

	}

	public function updatePayoutDetails(User $user) {

		$this->authorize('updatePayoutDetails', [User::class, $user->id]);

		$payout_details = request()->validate([
			'payout_method' => 'required|in:'.implode(',', $this->payout_method_list->toArray()),
		]);

		switch ($payout_details['payout_method']) {

			case 'Bank Transfer':
				$bank_details = request()->validate([
				    'bank' => 'required|in:'.implode(',', $this->bank_list->toArray()),
				    'bank_account_number' => 'required|max:30',
				]);
				$user->update([
					'payout_method' => 'Bank Transfer',
					'bank' => $bank_details['bank'],
					'bank_account_number' => $bank_details['bank_account_number']
				]);
				break;

			case 'Cheque':
				$user_details = request()->validate([
				    'address' => 'required|max:'.\App\User::getMaxAddressLength(),
				]);
				$user->update([
					'payout_method' => 'Cheque',
					'address' => $user_details['address'],
				]);
				break;

		}

		alert()->success("Now get back to earning some $$ 😛", 'Payout details updated');


		return redirect()->route('user.edit', ['user' => $user->pretty_id]);

	}

	public function myProfile() {

		return redirect()->route('user.show', ['user' => auth()->user()->pretty_id]);

	}

    public function myAccount() {

		return $this->edit(auth()->user());

    }

    public function showPayOff(User $user) {

    	$this->authorize('payOff', [User::class, $user->id]);

    	$payout_method = $user->payout_method;

    	if ($payout_method === 'Bank Transfer') {

    		$bank = $user->bank;
    		$bank_account_number = $user->bank_account_number;
    		return view('user.pay-off', compact('user', 'payout_method', 'bank', 'bank_account_number'));

    	} elseif ($payout_method === 'Cheque') {

    		$address = $user->address;
    		return view('user.pay-off', compact('user', 'payout_method', 'address'));

    	} else {

    		return redirect()->route('admin.users');

    	}

    }

    public function payOff(User $user) {

    	$this->authorize('payOff', [User::class, $user->id]);

    	$payoff_data = request()->validate([
    		'payout_method' => 'required|in:'.implode(',', $this->payout_method_list->toArray()),
    	]);

    	$cheque_number_requirements = (request()->payout_method === 'Cheque') ? 'required' : '';
    	$address_requirements = (request()->payout_method === 'Cheque') ? 'required' : '';
    	$bank_requirements = (request()->payout_method === 'Bank Transfer') ? 'required' : '';
    	$bank_account_number_requirements = (request()->payout_method === 'Bank Transfer') ? 'required' : '';

		$payoff_data = request()->validate([
			'payout_method' => 'required|in:'.implode(',', $this->payout_method_list->toArray()),
			'subscriptions_count' => 'required|numeric',
			'cheque_number' => $cheque_number_requirements,
			'address' => $address_requirements,
			'bank' => $bank_requirements,
			'bank_account_number' => $bank_account_number_requirements,
		]);

    	$user->payOff($payoff_data);

    	alert()->success("The user has been paid off and notified.", 'User Paid Off');

    	return redirect()->route('admin.users', ['show' => 'owed']);

    }

    public function showPayouts(User $user) {

		$this->authorize('viewPayouts', [User::class, $user->id]);

		$table_headings = ['payout_id', 'amount', 'payout_date'];

		return view('user.payouts', compact('user', 'table_headings'));

    }

    public function showBecomeATeacher(User $user) {

    	$this->authorize('becomeATeacher', [User::class, $user->id]);

    	return view('user.become-a-teacher', compact('user'));
    	
    }

    public function becomeATeacher(User $user) {

    	$this->authorize('becomeATeacher', [User::class, $user->id]);

    	$upgrade_data = request()->validate([
    	        'identification' => 'required|file|max:25000|mimes:doc,docx,pdf,txt,rtf,jpg,jpeg,bmp,png,zip',
    	        'qualifications' => 'required|file|max:25000|mimes:doc,docx,pdf,txt,rtf,jpg,jpeg,bmp,png,zip',
    	        'teaching_experience' => 'nullable|string|profane:'.resource_path('lang/en/profane_local.php').'|max:'.User::getMaxTeachingExperienceLength(),
    	    ]);

    	$user->flagForTeacherUpgrade(request()->identification, request()->qualifications);

    	if (isset($upgrade_data['teaching_experience'])) {

    		$user->update([
    			'teaching_experience' => $upgrade_data['teaching_experience']
    		]);

    	}

    	alert()->warning("Your account will be granted Tutor/Teacher privilege once approved. Please wait 1-2 business days.", "Upgrade Pending, $user->name");

    	return redirect()->route('user.edit', ['user' => $user->pretty_id]);
    	
    }

    public function showUpgradeToRegisteredTeacher(User $user) {

    	$this->authorize('upgradeToRegisteredTeacher', [User::class, $user->id]);

    	return view('user.upgrade-to-registered-teacher', compact('user'));
    	
    }

    public function upgradeToRegisteredTeacher(User $user) {

    	$this->authorize('upgradeToRegisteredTeacher', [User::class, $user->id]);

    	$upgrade_data = request()->validate([
    	        'identification' => 'required|file|max:25000|mimes:doc,docx,pdf,txt,rtf,jpg,jpeg,bmp,png,zip',
    	        'qualifications' => 'required|file|max:25000|mimes:doc,docx,pdf,txt,rtf,jpg,jpeg,bmp,png,zip'
    	    ]);

    	$user->deleteIdentification();
    	$user->deleteQualifications();

    	$user->upgradeToRegisteredTeacher(request()->identification, request()->qualifications);

    	alert()->success("The account has been granted Teacher privileges.", "Upgrade Completed.");

    	return redirect()->route('user.edit', ['user' => $user->pretty_id]);
    	
    }


    public function upgrade(User $user) {

    	$this->authorize('upgrade', [User::class, $user->id, request()->new_privilege]);

    	$user->upgrade(request()->new_privilege);

    	alert()->success("The user has been upgraded to a $user->privilege_name and notified.", 'User Upgraded');

    	return redirect()->back();

    }

    public function cancelUpgrade(User $user) {

    	$this->authorize('cancelUpgrade', [User::class, $user->id]);

    	$user->cancelUpgrade();

    	alert()->success("The user's upgrade request has been cancelled and notified.", 'User Not Upgraded');

    	return redirect()->back();

    }


    public function downgrade(User $user) {

    	$this->authorize('downgrade', [User::class, $user->id]);

		$is_downgraded = $user->downgrade();

		if ($is_downgraded) {

			alert()->success("The user has been downgraded to a Student and notified.", 'User Downgraded');

		} else {

			alert()->error("The user has NOT been downgraded. If the user has an amount owed, please pay off User.", 'User NOT Downgraded');

		}

		return redirect()->back();

	}

	public function storePhoto(User $user) {

	    $this->authorize('uploadPhoto', [User::class, $user->id]);

	    $user_data = request()->validate([
	            'photo' => 'image|max:25000'
	        ]);

	    $user->uploadPhoto(request()->photo);

	    alert()->success('Your profile photo has been updated', 'Profile Photo Uploaded');

	    return redirect()->route('user.show', ['user' => $user->pretty_id]);
	    
	}

	public function replacePhoto(User $user) {

	    $this->authorize('deletePhoto', [User::class, $user->id]);
	    
	    $user->deletePhoto();

	    return $this->storePhoto($user);

	}

	public function deletePhoto(User $user) {

	    $this->authorize('deletePhoto', [User::class, $user->id]);
	    
	    $user->deletePhoto();

	    alert()->success('Your profile photo has been deleted.', 'Profile Photo Deleted');

	    return redirect()->route('user.show', ['user' => $user->pretty_id]);

	}

	public function storeIdentification(User $user) {

	    $this->authorize('uploadIdentification', [User::class, $user->id]);

	    $user_data = request()->validate([
	            'identification' => 'required|file|max:25000|mimes:doc,docx,pdf,txt,rtf,jpg,jpeg,bmp,png,zip'
	        ]);

	    $user->uploadIdentification(request()->identification);

	    alert()->success('The User\'s Identification has been updated.', 'Identification Uploaded');

	    return redirect()->back();
	    
	}

	public function storeQualifications(User $user) {

	    $this->authorize('uploadQualifications', [User::class, $user->id]);

	    $user_data = request()->validate([
	            'qualifications' => 'required|file|max:25000|mimes:doc,docx,pdf,txt,rtf,jpg,jpeg,bmp,png,zip'
	        ]);

	    $user->uploadQualifications(request()->qualifications);

	    alert()->success('The User\'s Qualifications has been updated.', 'Qualifications Uploaded');

	    return redirect()->back();
	    
	}

	public function deleteIdentification(User $user) {
		
		$this->authorize('deleteIdentification', [User::class, $user->id]);
		
		$user->deleteIdentification();

		alert()->success('The User\'s Identification has been deleted.', 'Identification Deleted');

		return redirect()->back();

	}

	public function deleteQualifications(User $user) {
		
		$this->authorize('deleteQualifications', [User::class, $user->id]);
		
		$user->deleteQualifications();

		alert()->success('The User\'s Qualifications has been deleted.', 'Qualifications Deleted');

		return redirect()->back();
		
	}

	public function gift(User $user) {

		$this->authorize('gift', [User::class, $user->id]);

		$gift_amount = User::GIFT_CREDIT;
		$user->credit += $gift_amount;
		$user->save();

		alert()->success("The user has been gifted $gift_amount days of free credit.", 'User Gifted Credit');

		return redirect()->back();
		
	}

	public function destroy(User $user) {

		$this->authorize('delete', [User::class, $user->id]);

		$user_email = $user->email;

		// Ensures account creation in the future (if re-created).
		$user->update([
			'email' => 'DELETED'.'_'.Carbon::now()->format('YmdHis').'_'.$user_email
		]);

		$user->delete();

		alert()->success('The user has been deleted.', 'User Deleted');

		return redirect()->route('admin.users');
		
	}

}
