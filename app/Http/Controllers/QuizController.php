<?php

namespace App\Http\Controllers;

use App\Question;
use App\Classroom;
use App\Lesson;
use App\Quiz;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use JavaScript;

class QuizController extends Controller {

	public function __construct() {
	    
	    $this->middleware('auth');
	    
	}

	public function index(Lesson $lesson, $classroom_number) {

		$classroom = $lesson->getClassroomByNumber($classroom_number);

		$this->authorize('index', [Quiz::class, $classroom]);
	    
	    return view('quiz.index', compact('lesson', 'classroom', 'classroom_number'));
	    
	}

	public function show(Lesson $lesson, $classroom_number, $quiz_number) {
	    
	    $classroom = $lesson->getClassroomByNumber($classroom_number);
	    $quiz = $classroom->getQuizByNumber($quiz_number);

	    $this->authorize('view', [Quiz::class, $quiz]);

    	$questions = $quiz->questions;

    	if (!Gate::allows('create', [Question::class, $quiz])) {

    		if (!$quiz->completed) {
    			$msg = 'You have ' . ($quiz->questions->count() - $quiz->answered_count) . ' unanswered Question(s). Please answer all Questions to receive your results.';
    			flash($msg)->important();
    		} else {
    			$msg = 'Results: <span class="font-weight-bold">' . $quiz->score.' out of ' . $quiz->questions->count() . ' Correct</span>. You can review the correct answers below.';
    			flash($msg)->success()->important();
    		}

    	}

	    return view('quiz.show', compact('questions', 'quiz', 'classroom', 'lesson', 'classroom_number', 'quiz_number'));
	    
	}

	public function create(Lesson $lesson, $classroom_number) {

		$classroom = $lesson->getClassroomByNumber($classroom_number);

	    $this->authorize('create', [Quiz::class, $classroom]);

	    return view('quiz.create', compact('lesson', 'classroom', 'classroom_number'));

	}

	public function store(Classroom $classroom) {

	    $this->authorize('create', [Quiz::class, $classroom]);

	    $lesson = $classroom->lesson;

	    $quiz_data = request()->validate([
	            'title' => 'required|max:30|profane:'.resource_path('lang/en/profane_local.php')
    	        ]);

	    $quiz = $classroom->addQuiz(new Quiz($quiz_data));

	    // Alert user that quiz has been created.
	    alert()->success('Your quiz has been created. Please add a Question to your Quiz.', 'Quiz created');
	    
	    return redirect()->route('question.create', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number, 'quiz_number' => $quiz->number]);

	}

	public function edit(Lesson $lesson, $classroom_number, $quiz_number) {

	    $classroom = $lesson->getClassroomByNumber($classroom_number);
	    $quiz = $classroom->getQuizByNumber($quiz_number);

	    $this->authorize('update', [Quiz::class, $quiz]);

	    JavaScript::put(['forms' => [

	    	[
	    	'id' => 'delete-quiz',
	    	'swal_submit' => [
	    	    'title' => 'Deleting Quiz',
	    	    'text' => 'Are you sure you\'d like to delete this Quiz?',
	    	    'icon' => 'warning',
	    	    'danger' => true,
	    	    ]
	    	]

	    ]]);

	    $show_quiz_link = route('quiz.show', ['lesson' => $lesson->id, 'classroom_number' => $classroom_number, 'quiz_number' => $quiz_number]);

	    flash("If you would like to view, add or edit one of the questions for this Quiz, you can do that <a href=\"$show_quiz_link\" class=\"alert-link\">here</a>.")->important();

	    return view('quiz.edit', compact('lesson', 'classroom', 'quiz', 'classroom_number', 'quiz_number'));
	    
	}

	public function update(Quiz $quiz) {

	    $this->authorize('update', [Quiz::class, $quiz]);

	    $classroom = $quiz->classroom;
	    $lesson = $classroom->lesson;

	    $quiz_data = request()->validate([
	            'title' => 'required|max:30|profane:'.resource_path('lang/en/profane_local.php')
	        ]);

	    $quiz->update($quiz_data);

	    alert()->success('Your quiz has been updated.', 'Quiz Updated');

	    return redirect()->route('quiz.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number , 'quiz_number' => $quiz->number]);

	}

	public function destroy(Quiz $quiz) {

	    $this->authorize('delete', [Quiz::class, $quiz]);

	    $classroom = $quiz->classroom;
	    $lesson = $classroom->lesson;

	    $quiz->delete();

	    alert()->success('Your quiz has been deleted.', 'Quiz Deleted');

	    return redirect()->route('quiz.index', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number]);

	}

}
