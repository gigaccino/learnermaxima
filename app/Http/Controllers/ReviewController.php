<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\Review;
use App\Defaults\Review as DefaultReview;
use JavaScript;

class ReviewController extends Controller {

    protected $review_list;
    
    public function __construct() {
        
        $this->middleware('auth', ['except' => ['index', 'show']]);
        $this->review_list = resolve(DefaultReview::class)->getList();
        
    }

    public function create(Lesson $lesson) {

        $this->authorize('create', [Review::class, $lesson]);

        $review_list = $this->review_list;

        return view('review.create', compact('lesson', 'review_list'));

    }

    public function store(Lesson $lesson) {

        $this->authorize('create', [Review::class, $lesson]);

        $review_data = request()->validate([
            'rating' => 'required|in:'.implode(',', $this->review_list->toArray()),
            'body' => 'required|max:'.Review::getMaxBodyLength().'|profane:'.resource_path('lang/en/profane_local.php')
        ]);

        $review_data = $review_data + ['lesson_id' => $lesson->id];

        $review = auth()->user()->addReview(new Review($review_data));

        alert()->success('Your review has been posted.', 'Review Posted');
        
        return redirect()->route('lesson.show', ['lesson' => $lesson->pretty_id]);

    }

    public function edit(Lesson $lesson, $review_number) {

        $review = $lesson->getReviewByNumber($review_number);

        $review_list = $this->review_list;

        $this->authorize('update', [Review::class, $review]);

        JavaScript::put(['forms' => [

            [
            'id' => 'delete-review',
            'swal_submit' => [
                'title' => 'Deleting Review',
                'text' => 'Are you sure you\'d like to delete your Review for this Lesson?',
                'icon' => 'warning',
                'danger' => true,
                ]
            ]

        ]]);

        return view('review.edit', compact('lesson', 'review', 'review_list'));
        
    }

    public function update(Review $review) {

        $this->authorize('update', [Review::class, $review]);

        $review_data = request()->validate([
            'rating' => 'required|in:'.implode(',', $this->review_list->toArray()),
            'body' => 'required|max:'.Review::getMaxBodyLength().'|profane:'.resource_path('lang/en/profane_local.php')
        ]);

        $review->update($review_data);

        alert()->success('Your review has been updated.', 'Review Updated');

        return redirect()->route('lesson.show', ['lesson' => $review->lesson->pretty_id]);

    }

    public function destroy(Review $review) {

        $this->authorize('delete', [Review::class, $review]);

        $review->delete();

        alert()->success('Your review has been deleted.', 'Review Deleted');

        return redirect()->route('lesson.show', ['lesson' => $review->lesson->pretty_id]);

    }

}
