<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Classroom;
use App\Lesson;
use Illuminate\Http\Request;
use JavaScript;

class AssignmentController extends Controller {
    
	public function __construct() {
	    
	    $this->middleware('auth');
	    
	}

	public function index(Lesson $lesson, $classroom_number) {

		$classroom = $lesson->getClassroomByNumber($classroom_number);

		$this->authorize('index', [Assignment::class, $classroom]);
	    
	    return view('assignment.index', compact('lesson', 'classroom', 'classroom_number'));
	    
	}

	public function create(Lesson $lesson, $classroom_number) {

		$classroom = $lesson->getClassroomByNumber($classroom_number);

	    $this->authorize('create', [Assignment::class, $classroom]);

	    return view('assignment.create', compact('lesson', 'classroom', 'classroom_number'));

	}

	public function store(Classroom $classroom) {

	    $this->authorize('create', [Assignment::class, $classroom]);

	    $lesson = $classroom->lesson;

	    $assignment_data = request()->validate([
	            'questions' => 'file|max:25000|mimes:doc,docx,pdf,txt,rtf,jpg,jpeg,bmp,png,zip',
	            'solutions' => 'file|max:25000|mimes:doc,docx,pdf,txt,rtf,jpg,jpeg,bmp,png,zip',
	            'solutions_release_days' => 'nullable|integer|min:0',
	            'title' => 'required|max:30|profane:'.resource_path('lang/en/profane_local.php'),
	            'instructions' => 'required|max:'.Assignment::getMaxInstructionsLength().'|profane:'.resource_path('lang/en/profane_local.php'),
	        ]);

	    $assignment = $classroom->addAssignment(new Assignment($assignment_data, request()->questions, request()->solutions));

	    // Alert user that assignment has been created.
	    alert()->success('Your assignment has been created.', 'Assignment created');

	    return redirect()->route('assignment.index', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number]);

	}

	public function edit(Lesson $lesson, $classroom_number, $assignment_number) {

	    $classroom = $lesson->getClassroomByNumber($classroom_number);
	    $assignment = $classroom->getAssignmentByNumber($assignment_number);

	    $this->authorize('update', [Assignment::class, $assignment]);

	    JavaScript::put(['forms' => [

	    	[
	    	'id' => 'delete-assignment',
	    	'swal_submit' => [
	    	    'title' => 'Deleting Assignment',
	    	    'text' => 'Are you sure you\'d like to delete this Assignment?',
	    	    'icon' => 'warning',
	    	    'danger' => true,
	    	    ]
	    	]

	    ]]);

	    return view('assignment.edit', compact('lesson', 'classroom', 'assignment', 'classroom_number', 'assignment_number'));
	    
	}

	public function update(Assignment $assignment) {

	    $this->authorize('update', [Assignment::class, $assignment]);

	    $classroom = $assignment->classroom;
	    $lesson = $classroom->lesson;

	    $assignment_data = request()->validate([
	    	'title' => 'required|max:30|profane:'.resource_path('lang/en/profane_local.php'),
	    	'instructions' => 'required|max:'.Assignment::getMaxInstructionsLength().'|profane:'.resource_path('lang/en/profane_local.php'),
	    	'solutions_release_days' => 'nullable|integer|min:0',
	    ]);

	    if ($assignment_data['solutions_release_days'] === null) {
	    	$assignment_data['solutions_release_days'] = 0;
	    }

	    $assignment->update($assignment_data);

	    alert()->success('Your assignment has been updated.', 'Assignment Updated');

	    return redirect()->route('assignment.index', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number]);

	}

	public function destroy(Assignment $assignment) {

		$this->authorize('delete', [Assignment::class, $assignment]);

		$classroom = $assignment->classroom;
		$lesson = $classroom->lesson;

	    $assignment->delete();

	    alert()->success('Your assignment has been deleted.', 'Assignment Deleted');

	    return redirect()->route('assignment.index', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number]);

	}

}
