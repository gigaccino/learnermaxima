<?php

namespace App\Http\Controllers;

use App\Lesson;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller {

    public function home() {

        $recent_lessons = Lesson::latest()->get()->sortByDesc('created_at')->slice(0,4);

        // Filter out Lessons that user can not view/subscribe
        $recent_lessons = $recent_lessons->filter(function ($lesson, $key) {
            if (Auth::check()) {
                return auth()->user()->can('view', [Lesson::class, $lesson]);
            } else {
                return !$lesson->isSubscriptionsDisabled();
            }
        });

        return view('pages.home', compact('recent_lessons'));
        
    }

    public function howItWorks() {
    	
    	return view('pages.how-it-works');
    	
    }

    public function faq() {
    	
    	return view('pages.faq');
    	
    }

    public function privacy() {
    	
    	return view('pages.privacy');
    	
    }

    public function terms() {
    	
    	return view('pages.terms');
    	
    }

}
