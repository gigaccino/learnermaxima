<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\Question;
use App\Quiz;
use Illuminate\Http\Request;
use JavaScript;

class QuestionController extends Controller {

	public function __construct() {
	    
	    $this->middleware('auth');
	    
	}

	public function create(Lesson $lesson, $classroom_number, $quiz_number) {

		$classroom = $lesson->getClassroomByNumber($classroom_number);
		$quiz = $classroom->getQuizByNumber($quiz_number);

	    $this->authorize('create', [Question::class, $quiz]);

	    return view('question.create', compact('lesson', 'classroom', 'quiz', 'classroom_number', 'quiz_number'));

	}

	public function store(Quiz $quiz) {

	    $this->authorize('create', [Question::class, $quiz]);

	    $classroom = $quiz->classroom;
	    $lesson = $classroom->lesson;

	    $question_data = request()->validate([
	            'question' => 'required|max:'.Question::getMaxQuestionLength().'|profane:'.resource_path('lang/en/profane_local.php'),
	            'photo' => 'image|max:25000',
	            'answer_a' => 'required|max:'.Question::getMaxAnswerLength().'|profane:'.resource_path('lang/en/profane_local.php'),
	            'answer_b' => 'required|max:'.Question::getMaxAnswerLength().'|profane:'.resource_path('lang/en/profane_local.php'),
	            'answer_c' => 'required|max:'.Question::getMaxAnswerLength().'|profane:'.resource_path('lang/en/profane_local.php'),
	            'answer_d' => 'required|max:'.Question::getMaxAnswerLength().'|profane:'.resource_path('lang/en/profane_local.php'),
	            'correct_answer' => 'required|in:a,b,c,d'
	        ]);

	    $question = $quiz->addQuestion(new Question($question_data, request()->photo));

	    // Alert user that question has been created.
	    alert()->success('Your question has been added to the quiz.', 'Question added');
	    
	    return redirect()->route('quiz.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number, 'quiz_number' => $quiz->number]);

	}

	public function edit(Lesson $lesson, $classroom_number, $quiz_number, $question_number) {

	    $classroom = $lesson->getClassroomByNumber($classroom_number);
	    $quiz = $classroom->getQuizByNumber($quiz_number);
	    $question = $quiz->getQuestionByNumber($question_number);

	    $this->authorize('update', [Question::class, $question]);

	    JavaScript::put(['forms' => [

	    	[
	    	'id' => 'delete-question',
	    	'swal_submit' => [
	    	    'title' => 'Deleting Question',
	    	    'text' => 'Are you sure you\'d like to delete this Question?',
	    	    'icon' => 'warning',
	    	    'danger' => true,
	    	    ]
	    	],
	    	[
	    	'id' => 'delete-photo',
	    	'swal_submit' => [
	    	    'title' => 'Deleting Photo',
	    	    'text' => 'Are you sure you\'d like to delete this Question Photo?',
	    	    'icon' => 'warning',
	    	    'danger' => true,
	    	    ]
	    	],
	    	[
	    	'id' => 'replace-photo',
	    	'swal_submit' => [
	    	    'title' => 'Replacing Photo',
	    	    'text' => 'Are you sure you\'d like to delete this Question Photo?',
	    	    'icon' => 'warning',
	    	    'danger' => true,
	    	    ]
	    	]

	    ]]);

	    return view('question.edit', compact('lesson', 'classroom', 'quiz', 'question', 'classroom_number', 'quiz_number', 'question_number'));
	    
	}

	public function update(Question $question) {

	    $this->authorize('update', [Question::class, $question]);

	    $quiz = $question->quiz;
	    $classroom = $quiz->classroom;
	    $lesson = $classroom->lesson;

	    $question_data = request()->validate([
	            'question' => 'required|max:'.Question::getMaxQuestionLength().'|profane:'.resource_path('lang/en/profane_local.php'),
	            'answer_a' => 'required|max:'.Question::getMaxAnswerLength().'|profane:'.resource_path('lang/en/profane_local.php'),
	            'answer_b' => 'required|max:'.Question::getMaxAnswerLength().'|profane:'.resource_path('lang/en/profane_local.php'),
	            'answer_c' => 'required|max:'.Question::getMaxAnswerLength().'|profane:'.resource_path('lang/en/profane_local.php'),
	            'answer_d' => 'required|max:'.Question::getMaxAnswerLength().'|profane:'.resource_path('lang/en/profane_local.php')
	        ]);

	    $question->update($question_data);

	    alert()->success('Your question has been updated.', 'Question Updated');

	    return redirect()->route('quiz.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number, 'quiz_number' => $quiz->number]);

	}

	public function destroy(Question $question) {

	    $this->authorize('delete', [Question::class, $question]);

	    $quiz = $question->quiz;
	    $classroom = $quiz->classroom;
	    $lesson = $classroom->lesson;

	    $question->delete();

	    alert()->success('Your question has been deleted.', 'Question Deleted');

	    return redirect()->route('quiz.show', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number, 'quiz_number' => $quiz->number]);

	}

	public function storePhoto(Question $question) {

	    $this->authorize('uploadPhoto', [Question::class, $question]);

	    $quiz = $question->quiz;
	    $classroom = $quiz->classroom;
	    $lesson = $classroom->lesson;

	    $question_data = request()->validate([
	            'photo' => 'image|max:25000'
	        ]);

	    $question->uploadPhoto(request()->photo);

	    alert()->success('Your photo has been added to your question.', 'Photo Uploaded');

	    return redirect()->route('question.edit', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number, 'quiz_number' => $quiz->number, 'question_number' => $question->number]); 
	}

	public function replacePhoto(Question $question) {

	    $this->authorize('deletePhoto', [Question::class, $question]);
	    
	    $question->deletePhoto();

	    return $this->storePhoto($question);

	}

	public function deletePhoto(Question $question) {

	    $this->authorize('deletePhoto', [Question::class, $question]);

	    $quiz = $question->quiz;
	    $classroom = $quiz->classroom;
	    $lesson = $classroom->lesson;
	    
	    $question->deletePhoto();

	    alert()->success('Your photo has been deleted from your question.', 'Photo Deleted');

	    return redirect()->route('question.edit', ['lesson' => $lesson->pretty_id, 'classroom_number' => $classroom->number, 'quiz_number' => $quiz->number, 'question_number' => $question->number]); 
	}
    
}
