<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller {
    
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        $this->middleware('guest');

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        
        return Validator::make($data, [
            'account_type' => 'required|string|in:student,teacher',
            'identification' => ( (isset($data['account_type'])) ? (($data['account_type'] === 'teacher') ? 'required|file|max:25000|mimes:doc,docx,pdf,txt,rtf,jpg,jpeg,bmp,png,zip' : '') : ''),
            'qualifications' => ( (isset($data['account_type'])) ? (($data['account_type'] === 'teacher') ? 'required|' : 'nullable|') : ''). 'file|max:25000|mimes:doc,docx,pdf,txt,rtf,jpg,jpeg,bmp,png,zip',
            'teaching_experience' => 'nullable|string|profane:'.resource_path('lang/en/profane_local.php').'|max:'.User::getMaxTeachingExperienceLength(),
            'name' => 'required|string|max:30|profane:'.resource_path('lang/en/profane_local.php'),
            'email' => 'required|string|email|max:50|unique:users',
            'phone' => 'required|numeric|digits_between:7,15',
            'password' => 'required|string|min:6|confirmed',
            'terms' => 'required|in:true'
        ]);

        // return Validator::make($data, [
        //     'account_type' => 'required|string|in:teacher',
        //     'identification' => ( (isset($data['account_type'])) ? (($data['account_type'] === 'teacher') ? 'required|file|max:25000|mimes:doc,docx,pdf,txt,rtf,jpg,jpeg,bmp,png,zip' : '') : ''),
        //     'qualifications' => ( (isset($data['account_type'])) ? (($data['account_type'] === 'teacher') ? 'required|' : 'nullable|') : ''). 'file|max:25000|mimes:doc,docx,pdf,txt,rtf,jpg,jpeg,bmp,png,zip',
        //     'teaching_experience' => 'nullable|string|profane:'.resource_path('lang/en/profane_local.php').'|max:'.User::getMaxTeachingExperienceLength(),
        //     'name' => 'required|string|max:30|profane:'.resource_path('lang/en/profane_local.php'),
        //     'email' => 'required|string|email|max:50|unique:users',
        //     'phone' => 'required|numeric|digits_between:7,15',
        //     'password' => 'required|string|min:6|confirmed',
        //     'terms' => 'required|in:true'
        // ]);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {
        
        //Create Account
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' => bcrypt($data['password']),
            'credit' => ($data['account_type'] !== 'teacher')? env("NEW_ACCOUNT_CREDIT", 0) : 0
        ]);

        if (isset($user)) {

            if ($data['account_type'] === 'teacher') {

                $user->flagForTeacherUpgrade(request()->identification, request()->qualifications);

                if (isset($data['teaching_experience'])) {
                    $user->update(['teaching_experience' => $data['teaching_experience']]);
                }

                alert()->warning("Your account will be granted Tutor/Teacher privilege once approved. Please wait 1-2 business days.", "Welcome aboard, $user->name");

            } else {

                alert()->success("Try searching for a subject you're interested in.", "Welcome aboard, $user->name");
                
            }

        }

        $this->redirectTo = isset(request()->redirect_to) ? URL::to(request()->redirect_to) : '/';

        return $user;

    }

}
