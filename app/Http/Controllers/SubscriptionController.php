<?php

namespace App\Http\Controllers;

use App\Defaults\PaymentMethod;
use App\Defaults\SubscriptionLength;
use App\Lesson;
use App\Mail\ThanksForSubscribing;
use App\Payments\Wipay;
use App\PendingOrder;
use App\Subscription;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use JavaScript;

class SubscriptionController extends Controller {

    protected $wipay;

    public function __construct() {

        $this->middleware('auth');
        $this->wipay = resolve(Wipay::class);
        $this->payment_method_list = resolve(PaymentMethod::class)->getList();
        $this->subscription_length_list = resolve(SubscriptionLength::class)->getList();

    }

    public function show($order_number) {

        $subscription = Subscription::where('order_number', $order_number)->first();

        if (isset($subscription)) {

            $this->authorize('view', [Subscription::class, $subscription]);

            return view('subscription.show', compact('subscription'));

        }

        return abort(404);

    }

    public function mySubscriptions() {

        $user = auth()->user();

        $subscriptions_active = collect([]);
        $subscriptions_expired = collect([]);

        $subscriptions = $user->subscriptions->sortByDesc('payment_date');

        $subscriptions->each(function ($subscription) use (&$subscriptions_active, $subscriptions_expired) {

            if ($subscription->isActive()) {
                $subscriptions_active->push($subscription);
            } else {
                if ($subscription->transaction_type !== 'OLD') {
                    $subscriptions_expired->push($subscription);
                }
            }

        });

        $subscriptions_expired = $subscriptions_expired->slice(0, 10);

        return view('subscription.my-subscriptions', compact('user', 'subscriptions_active', 'subscriptions_expired'));

    }

    public function create(Lesson $lesson) {
        
        $this->authorize('create', [Subscription::class, $lesson]);

        $wipay = $this->wipay; 
        $payment_method_list = $this->payment_method_list->toArray(); 
        $subscription_length_list = $this->subscription_length_list->toArray();

        return view('subscription.create', compact('lesson', 'wipay', 'order_number', 'payment_method_list', 'subscription_length_list'));

    }

    public function createFree(Lesson $lesson) {
        
        $this->authorize('createFree', [Subscription::class, $lesson]);

        $user = auth()->user();

        return view('subscription.create-free', compact('user', 'lesson'));

    }

    public function createRenew(Lesson $lesson) {

        $subscription = Subscription::get(auth()->user(), $lesson);

        if (isset($subscription)) {

            $this->authorize('renew', [Subscription::class, $subscription]);

            $wipay = $this->wipay; 
            $payment_method_list = $this->payment_method_list->toArray(); 
            $subscription_length_list = $this->subscription_length_list->toArray();

            return view('subscription.create-renew', compact('lesson', 'wipay', 'order_number', 'payment_method_list', 'subscription_length_list'));

        } else {

            return abort(403, 'No Subscription found.');

        }

    }

    public function createOrder(Lesson $lesson) {

        $this->authorize('create', [Subscription::class, $lesson]);

        $lesson_data = request()->validate([
                'subscription_length' => 'required|in:'.implode(',', $this->subscription_length_list->toArray()),
                'payment_method' => 'required|in:'.implode(',', $this->payment_method_list->toArray())
            ]);

        $subscription_length = $lesson_data['subscription_length'];
        $payment_method = $lesson_data['payment_method'];
        $months = SubscriptionLength::getMonths($subscription_length);

        switch ($payment_method) {

            case 'Credit Card':
                return $this->createWipayOrder($lesson, $months);
                break;
            case 'WiPay':
                return $this->createWipayCashOrder($lesson, $months);
                break;
            default:
                return abort(403, 'Invalid Payment Method.');
                break;

        }        
    }

    public function createRenewOrder(Lesson $lesson) {

        $subscription = Subscription::get(auth()->user(), $lesson);

        $this->authorize('renew', [Subscription::class, $subscription]);

        $lesson_data = request()->validate([
                'subscription_length' => 'required|in:'.implode(',', $this->subscription_length_list->toArray()),
                'payment_method' => 'required|in:'.implode(',', $this->payment_method_list->toArray())
            ]);

        $subscription_length = $lesson_data['subscription_length'];
        $payment_method = $lesson_data['payment_method'];
        $months = SubscriptionLength::getMonths($subscription_length);

        switch ($payment_method) {

            case 'Credit Card':
                return $this->createWipayOrder($lesson, $months, 'RENEW');
                break;
            case 'WiPay':
                return $this->createWipayCashOrder($lesson, $months, 'RENEW');
                break;
            default:
                return abort(403, 'Invalid Payment Method.');
                break;

        }        
    }

    public function processWipayOrder() {

        $request_data = request()->all();

        $lesson_id = $this->wipay->getLessonID($request_data);
        $lesson = Lesson::findOrFail($lesson_id);
        $order_number = $this->wipay->getOrderNumber($request_data);
        $transaction_id = $this->wipay->getTransactionID($request_data);
        $verified_order_data = PendingOrder::get($order_number);

        // $this->authorize('create', [Subscription::class, $lesson]);

        if ($this->wipay->isSuccessfulOrderStatus($request_data)) {

            $order_data = $this->wipay->validateOrder($request_data, $verified_order_data);

            // Valid Order
            if (isset($order_data)) {

                return $this->store($lesson, $transaction_id, $order_data);

            } else {

                // Payment was NOT reason for subscription failure
                alert()->error("Yikes, we could not validate your payment. If you tried subscribing to a Lesson and can't access it, kindly send us an email at learnermaxima@gmail.com and we'll have this rectified in a jiffy!", 'Subscription failed')->autoclose(0);

            }

            return redirect()->route('lesson.show', ['lesson' => $lesson->pretty_id]);

        } else {

            alert()->error('Oops, there was an problem with your payment method. Please try a different method / credit card.', 'Subscription failed')->autoclose(0);

        }

        return redirect()->route('lesson.show', ['lesson' => $lesson->pretty_id]);

    }

    public function processWipayCashOrder() {

        $request_data = request()->all();

        if (isset($request_data['reason'])) {

            $reason = $request_data['reason'];
            alert()->error("Error: $reason", 'Your transaction could not be completed.')->autoclose(0);
            return redirect()->route('page.home');

        }

        $lesson_id = $this->wipay->getLessonID($request_data);
        $lesson = Lesson::findOrFail($lesson_id);
        $order_number = $this->wipay->getOrderNumber($request_data);
        $transaction_id = $this->wipay->getCashTransactionID($request_data);
        $verified_order_data = PendingOrder::get($order_number);

        // $this->authorize('create', [Subscription::class, $lesson]);

        if ($this->wipay->isSuccessfulOrderStatus($request_data)) {

            $order_data = $this->wipay->validateOrder($request_data, $verified_order_data);

            // Valid Order
            if (isset($order_data)) {

                return $this->store($lesson, $transaction_id, $order_data);

            } else {

                // Payment was NOT reason for subscription failure
                alert()->error("Yikes, we could not validate your payment. If you tried subscribing to a Lesson and can't access it, kindly send us an email at learnermaxima@gmail.com and we'll have this rectified in a jiffy!", 'Subscription failed')->autoclose(0);
            }

            return redirect()->route('lesson.show', ['lesson' => $lesson->pretty_id]);

        } else {

            alert()->error('Oops, there was an problem with your payment method. Please try a different method / credit card.', 'Subscription failed')->autoclose(0);

        }

        return redirect()->route('lesson.show', ['lesson' => $lesson->pretty_id]);

    }

    public function processWipayRenewOrder() {

        $request_data = request()->all();

        $lesson_id = $this->wipay->getLessonID($request_data);
        $lesson = Lesson::findOrFail($lesson_id);
        $order_number = $this->wipay->getOrderNumber($request_data);
        $transaction_id = $this->wipay->getTransactionID($request_data);
        $verified_order_data = PendingOrder::get($order_number);

        // $this->authorize('create', [Subscription::class, $lesson]);

        if ($this->wipay->isSuccessfulOrderStatus($request_data)) {

            $order_data = $this->wipay->validateOrder($request_data, $verified_order_data);

            // Valid Order
            if (isset($order_data)) {

                return $this->storeRenew($lesson, $transaction_id, $order_data);

            } else {

                // Payment was NOT reason for subscription failure
                alert()->error("Yikes, we could not validate your payment. If you tried subscribing to a Lesson and can't access it, kindly send us an email at learnermaxima@gmail.com and we'll have this rectified in a jiffy!", 'Renewal failed')->autoclose(0);

            }

            return redirect()->route('lesson.show', ['lesson' => $lesson->pretty_id]);

        } else {

            alert()->error('Oops, there was an problem with your payment method. Please try a different method / credit card.', 'Renewal failed')->autoclose(0);

        }

        return redirect()->route('lesson.show', ['lesson' => $lesson->pretty_id]);

    }

    public function processWipayCashRenewOrder() {

        $request_data = request()->all();

        if (isset($request_data['reason'])) {

            $reason = $request_data['reason'];
            alert()->error("Error: $reason", 'Your transaction could not be completed.')->autoclose(0);
            return redirect()->route('page.home');

        }

        $lesson_id = $this->wipay->getLessonID($request_data);
        $lesson = Lesson::findOrFail($lesson_id);
        $order_number = $this->wipay->getOrderNumber($request_data);
        $transaction_id = $this->wipay->getCashTransactionID($request_data);
        $verified_order_data = PendingOrder::get($order_number);

        // $this->authorize('create', [Subscription::class, $lesson]);

        if ($this->wipay->isSuccessfulOrderStatus($request_data)) {

            $order_data = $this->wipay->validateOrder($request_data, $verified_order_data);

            // Valid Order
            if (isset($order_data)) {

                return $this->storeRenew($lesson, $transaction_id, $order_data);

            } else {

                // Payment was NOT reason for subscription failure
                alert()->error("Yikes, we could not validate your payment. If you tried subscribing to a Lesson and can't access it, kindly send us an email at learnermaxima@gmail.com and we'll have this rectified in a jiffy!", 'Renewal failed')->autoclose(0);
            }

            return redirect()->route('lesson.show', ['lesson' => $lesson->pretty_id]);

        } else {

            alert()->error('Oops, there was an problem with your payment method. Please try a different method / credit card.', 'Renewal failed')->autoclose(0);

        }

        return redirect()->route('lesson.show', ['lesson' => $lesson->pretty_id]);

    }

    protected function createWipayOrder(Lesson $lesson, $months, $transaction_type='NEW') {

        JavaScript::put(['forms' => [
            [
                'id' => 'wipay',
                'auto_submit' => true
            ]
        ]]);

        switch ($months) {
            case 1:
                return $this->generateWipayOrder($lesson, 1, $transaction_type);
                break;
            case 2:
                return $this->generateWipayOrder($lesson, 2, $transaction_type);
                break;
            case 3:
                return $this->generateWipayOrder($lesson, 3, $transaction_type);
                break;
            default:
                return abort(403, 'Invalid Subscription Length.');
        }

    }

    protected function createWipayCashOrder(Lesson $lesson, $months, $transaction_type='NEW') {

        JavaScript::put(['forms' => [
            [
                'id' => 'wipay',
                'auto_submit' => true
            ]
        ]]);

        switch ($months) {
            case 1:
                return $this->generateWipayCashOrder($lesson, 1, $transaction_type);
                break;
            case 2:
                return $this->generateWipayCashOrder($lesson, 2, $transaction_type);
                break;
            case 3:
                return $this->generateWipayCashOrder($lesson, 3, $transaction_type);
                break;
            default:
                return abort(403, 'Invalid Subscription Length.');
        }

    }

    protected function generateWipayOrder(Lesson $lesson, $months, $transaction_type) {
        
        $order_number = Subscription::getOrderNumber(auth()->user(), $lesson, 'WCC');
        $subscription_length = Subscription::BASE_SUBSCRIPTION_LENGTH * $months;
        $price = $lesson->price * $months;

        $pending_order = PendingOrder::set($order_number, $price, $subscription_length, $transaction_type);

        $wipay_data = $this->wipay->getOrderFormData($price, $order_number);

        $wipay_url = $this->wipay->getUrl();

        return view('subscription.redirect-wipay-order', compact('wipay_url', 'wipay_data', 'transaction_type'));

    }

    protected function generateWipayCashOrder(Lesson $lesson, $months, $transaction_type) {
        
        $order_number = Subscription::getOrderNumber(auth()->user(), $lesson, 'WC');
        $subscription_length = Subscription::BASE_SUBSCRIPTION_LENGTH * $months;
        $price = $lesson->price * $months;

        $pending_order = PendingOrder::set($order_number, $price, $subscription_length, $transaction_type);

        $wipay_data = $this->wipay->getCashOrderFormData($price, $order_number);

        $wipay_url = $this->wipay->getCashUrl();

        return view('subscription.redirect-wipay-cash-order', compact('wipay_url', 'wipay_data', 'transaction_type'));

    }

    protected function store(Lesson $lesson, $transaction_id, $order_data) {

        // $this->authorize('create', [Subscription::class, $lesson]);

        $order_number = $order_data['order_number'];
        $price = $order_data['price'];
        $subscription_length = $order_data['subscription_length'];

        $subscription = 
            auth()->user()->addSubscription(new Subscription([
                'lesson_id' => $lesson->id,
                'teacher_id' => $lesson->user->id,
                'transaction_type' => 'NEW',
                'order_number' => $order_number,
                'transaction_id' => $transaction_id,
                'price' => $price,
                'subscription_length' => $subscription_length,
                'payment_date' => Carbon::now()
            ]));
        
        alert()->success("You've been subscribed for $subscription->subscription_length days.", 'Subscription successful');

        return redirect()->route('lesson.show', ['lesson' => $lesson->pretty_id]);

    }

    protected function storeFree(Lesson $lesson) {

        $this->authorize('createFree', [Subscription::class, $lesson]);

        $order_number = Subscription::getOrderNumber(auth()->user(), $lesson, 'CREDIT');
        $transaction_id = 'CREDIT-TRANSACTION';
        $subscription_length = auth()->user()->credit;

        $months = round($subscription_length / 30);
        $price = $months * $lesson->price;

        $subscription = 
            auth()->user()->addSubscription(new Subscription([
                'lesson_id' => $lesson->id,
                'teacher_id' => $lesson->user->id,
                'transaction_type' => 'NEW',
                'order_number' => $order_number,
                'transaction_id' => $transaction_id,
                'price' => $price,
                'subscription_length' => $subscription_length,
                'payment_date' => Carbon::now()
            ]));

        auth()->user()->clearCredit();
        
        alert()->success("You've been subscribed for $subscription->subscription_length days.", 'Subscription successful');

        return redirect()->route('lesson.show', ['lesson' => $lesson->pretty_id]);

    }

    protected function storeRenew(Lesson $lesson, $transaction_id, $order_data) {

        // $this->authorize('create', [Subscription::class, $lesson]);

        $old_subscription = Subscription::get(auth()->user(), $lesson);

        $new_sub = new Subscription([
                'lesson_id' => $lesson->id,
                'teacher_id' => $lesson->user->id,
                'transaction_type' => 'RENEW',
                'order_number' => $order_data['order_number'],
                'transaction_id' => $transaction_id,
                'price' => $order_data['price'],
                'subscription_length' => $old_subscription->days_remaining + $order_data['subscription_length'],
                'payment_date' => Carbon::now()
            ]);

        $new_subscription = auth()->user()->addSubscription($new_sub);

        $old_subscription->update(['transaction_type' => 'OLD']);
        
        alert()->success("You subscription has been renewed for $new_subscription->subscription_length days.", 'Renewal successful');

        return redirect()->route('lesson.show', ['lesson' => $lesson->pretty_id]);

    }

    protected function refund(Subscription $subscription) {

        $this->authorize('refund', [Subscription::class, $subscription]);

        $refunded_subscription = $subscription->refund();
        
        alert()->success("You've successfully refunded $subscription->order_number.", 'Refund successful');

        return redirect()->back();

    }

}
