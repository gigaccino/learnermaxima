<?php

namespace App\Defaults;

class Bank {

	protected $list = [
		'First Citizens Bank Limited',
		'Republic Bank Limited',
		'Scotiabank T&T Limited',
		'RBC Royal Bank (Trinidad & Tobago) Ltd',
		'JMMB Bank (T&T) Ltd'
	];

	public function getList() {
		
		return collect($this->list);
		
	}

}
