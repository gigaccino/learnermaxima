<?php

namespace App\Defaults;

class Level {

	protected $list = [
		'CSEC',
		'CAPE Unit 1',
		'CAPE Unit 2'
	];

	public function getList() {
		
		return collect($this->list);
		
	}

}
