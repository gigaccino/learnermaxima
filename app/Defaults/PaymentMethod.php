<?php

namespace App\Defaults;

class PaymentMethod {

	protected $list = [
		'Credit Card',
		'WiPay',
	];

	public function getList() {
		
		return collect($this->list);
		
	}

}
