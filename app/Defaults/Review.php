<?php

namespace App\Defaults;

class Review {

	protected $list = [
		1,
		2,
		3,
		4,
		5
	];

	public function getList() {
		
		return collect($this->list);
		
	}

	public function getMax() {
		
		return max($this->list);

	}

}
