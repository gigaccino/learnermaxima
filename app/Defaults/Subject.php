<?php

namespace App\Defaults;

class Subject {

	const DEFAULT_SUBJECT_PHOTO_PATH = 'https://learnermaxima.nyc3.digitaloceanspaces.com/assets/defaults/subjects/photos/';

	protected $csec_list = [
		'ADDITIONAL MATHEMATICS',
		'AGRICULTURAL SCIENCE',
		'BIOLOGY',
		'CARIBBEAN HISTORY',
		'CERTIFICATE IN BUSINESS STUDIES',
		'CHEMISTRY',
		'ECONOMICS',
		'ELECTRONIC DOCUMENT PREPARATION AND MANAGEMENT (EDPM)',
		'ENGLISH',
		'GEOGRAPHY',
		'HOME ECONOMICS',
		'HUMAN AND SOCIAL BIOLOGY',
		'INDUSTRIAL TECHNOLOGY',
		'INFORMATION TECHNOLOGY',
		'INTEGRATED SCIENCE',
		'MATHEMATICS',
		'MODERN LANGUAGES',
		'MUSIC',
		'OFFICE ADMINISTRATION',
		'PHYSICAL EDUCATION AND SPORT',
		'PHYSICS',
		'PRINCIPLES OF ACCOUNTS',
		'PRINCIPLES OF BUSINESS',
		'RELIGIOUS EDUCATION',
		'SOCIAL STUDIES',
		'TECHNICAL DRAWING',
		'THEATRE ARTS',
		'VISUAL ARTS',
	];

	protected $cape_list = [
		'ACCOUNTING',
		'ANIMATION & GAME DESIGN',
		'AGRICULTURAL SCIENCE',
		'APPLIED MATHEMATICS',
		'ART AND DESIGN',
		'BIOLOGY',
		'BUILDING AND MECHANICAL ENGINEERING DRAWING',
		'CARIBBEAN STUDIES',
		'CHEMISTRY',
		'COMMUNICATION STUDIES',
		'COMPUTER SCIENCE',
		'DIGITAL MEDIA',
		'ELECTRICAL AND ELECTRONIC ENGINEERING TECHNOLOGY',
		'ECONOMICS',
		'ENTREPRENEURSHIP',
		'ENVIRONMENTAL SCIENCE',
		'FINANCIAL SERVICES STUDIES',
		'FOOD AND NUTRITION',
		'FRENCH',
		'GEOGRAPHY',
		'GREEN ENGINEERING',
		'HISTORY',
		'INFORMATION TECHNOLOGY',
		'INTEGRATED MATHEMATICS',
		'LAW',
		'LITERATURES IN ENGLISH',
		'LOGISTICS AND SUPPLY CHAIN OPERATIONS',
		'MANAGEMENT OF BUSINESS',
		'PERFORMING ARTS',
		'PHYSICS',
		'PHYSICAL EDUCATION AND SPORT',
		'PURE MATHEMATICS',
		'SOCIOLOGY',
		'SPANISH',
		'TOURISM',
	];

	public function getList() {

		$list = collect(array_merge($this->csec_list, $this->cape_list))->sort()->unique()->map(function ($item, $key) {
			return ucwords(strtolower($item));
		});

		return $list;

	}

	public function getPhotoUrl($subject) {
		
		$list_collection = $this->getList();
		
		if ($list_collection->contains($subject)) {

			$lower_name = strtolower($subject);
			$name = str_replace(' ', '_', $lower_name);
			return self::DEFAULT_SUBJECT_PHOTO_PATH.$name.'.jpg';

		}

		return self::DEFAULT_SUBJECT_PHOTO_PATH.'default.jpg';

	}
	
}
