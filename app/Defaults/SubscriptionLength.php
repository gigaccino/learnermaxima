<?php

namespace App\Defaults;

class SubscriptionLength {

	protected $list = [
		'1 Month',
		'2 Months',
		'3 Months',
	];

	public function getList() {
		
		return collect($this->list);
		
	}

	public static function getMonths($subscription_length) {
		
		return explode(" ", $subscription_length)[0];

	}

}
