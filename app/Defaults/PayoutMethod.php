<?php

namespace App\Defaults;

class PayoutMethod {

	protected $list = [
		'Bank Transfer',
		// 'Cheque', 
	];

	public function getList() {
		
		return collect($this->list);
		
	}

}
