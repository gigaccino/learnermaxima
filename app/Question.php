<?php

namespace App;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class Question extends Model {

	const MAX_QUESTION_LENGTH = 280;
	const MAX_ANSWER_LENGTH = 140;
    
	// The attributes that are mass assignable.
	protected $fillable = [

	    'quiz_id', 'question', 'photo', 'answer_a', 'answer_b', 'answer_c', 'answer_d', 'correct_answer'
	    
	];

	// The attributes that should be shown for arrays.
	protected $visible = [

	    'id', 'quiz_id', 'question', 'photo', 'answer_a', 'answer_b', 'answer_c', 'answer_d', 'correct_answer'

	];

	public function __construct(array $attributes = array(), $photo = null) {

        // Eloquent
		parent::__construct($attributes);

		if (isset($photo)) {

			$this->photo = self::savePhoto($photo);

		}

	}

	public function user() {

		return $this->belongsTo(User::class);
		
	}

	public function answers() {

	    return $this->hasMany(Answer::class);

	}

	public function quiz() {

	    return $this->belongsTo(Quiz::class);

	}

	public function getNumberAttribute() {

	    $quiz = $this->quiz;

	    $results = $quiz->questions->pluck('id')->search($this->id) + 1;

	    return $results ? $results : null;
	    
	}

	public function getPhotoUrlAttribute() {

		if (isset($this->photo)) {

			return Storage::disk('spaces')->url($this->photo);

		}

		return null;

	}

	public function validateAnswer(Answer $answer) {

		return $this->correct_answer === $answer->answer;

	}

	public function deletePhoto() {

		if (isset($this->photo)) {

			Storage::disk('spaces')->delete($this->photo);

			$this->update([
			    'photo' => null
			]);

		}

		return $this;
		
	}

	public function uploadPhoto($photo) {
		
		$photo_uri =  self::savePhoto($photo);

		$this->update([
		    'photo' => $photo_uri
		]);

	}

	public static function getMaxQuestionLength() {

		return self::MAX_QUESTION_LENGTH;

	}

	public static function getMaxAnswerLength() {

		return self::MAX_ANSWER_LENGTH;

	}

	private static function savePhoto($photo) {

		$path = 'uploads/questions/photos/'.$photo->hashName();
		$img = Image::make($photo)->fit(1920, 1080)->encode();
		
		return Storage::disk('spaces')->put($path, (string) $img, 'public') ? $path : null;

	}
    
}
