<?php

namespace App;

class Answer extends Model {
	
	// The attributes that are mass assignable.
	protected $fillable = [

	    'user_id', 'question_id', 'answer'
	    
	];

	// The attributes that should be shown for arrays.
	protected $visible = [

	    'id', 'user_id', 'question_id', 'answer'

	];

	public function user() {

	    return $this->belongsTo(User::class);

	}

	public function question() {

	    return $this->belongsTo(Question::class);

	}

	public function getIsCorrectAttribute() {

		return $this->question->correct_answer === $this->answer;

	}

}
