<?php

namespace App;

use App\Events\AccountNotUpgraded;
use App\Events\AccountUpgraded;
use App\Events\UpgradePending;
use App\Payout;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable {
    
    use Notifiable;
    use SoftDeletes;

    const MAX_ADDRESS_LENGTH = 140;
    const MAX_TEACHING_EXPERIENCE_LENGTH = 280;
    const GIFT_CREDIT = 5;

    const DEFAULT_PHOTO_URL = 'https://learnermaxima.nyc3.digitaloceanspaces.com/assets/defaults/users/photos/avatar.png';

    // The attributes that are mass assignable.
    protected $fillable = [

        'photo', 'identification', 'name', 'email', 'public_contact_email', 'phone', 'tagline', 'address', 'qualifications', 'teaching_experience', 'payout_method', 'bank', 'bank_account_number', 'pending_upgrade', 'password', 'notes', 'credit'
        
    ];

    // The attributes that should be shown for arrays.
    protected $visible = [

        'id', 'photo', 'identification', 'name', 'email', 'public_contact_email', 'phone', 'tagline', 'address', 'qualifications', 'teaching_experience', 'payout_method', 'bank', 'bank_account_number', 'pending_upgrade', 'notes', 'credit'

    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function getPrettyIDAttribute() {

        $id = str_replace(" ", "-", $this->id);
        $name = str_replace(" ", "-", $this->name);

        return "$id-$name";

    }

    public function getPrivilegeNameAttribute() {

        $name = '';

        switch ($this->privilege) {
            case 1:
                $name = 'Student';
                break;
            case 2:
                $name = 'Tutor';
                break;
            case 3:
                $name = 'Teacher';
                break;
            case 4:
                $name = 'Admin';
                break;
            default:
                $name = 'Unknown';
        }

        return $name;

    }

    public function getAccountMessageAttribute() {
        
        $message = '';

        if ($this->isPendingUpgrade()) {
            $message = $message . 'Your account will be upgraded pending our approval. Please wait 1-2 business days.';
        }

        return (strlen($message) > 0) ? $message: null;

    }

    public function getTeacherSubscriptionsAttribute() {
        
        return Subscription::where(['teacher_id' => $this->id, 'payout_id' => null]);

    }

    public function getAmountOwedAttribute() {

        $amount_owed = 0;

        $this->teacher_subscriptions->each(function ($subscription, $index) use (&$amount_owed) {
            $amount_owed += $subscription->payout_amount;
        });

        return $amount_owed;

    }

    public function getIdentificationUrlAttribute() {

        if (isset($this->identification)) {

            return Storage::disk('spaces')->url($this->identification);

        }

        return null;

    }

    public function getQualificationsUrlAttribute() {

        if (isset($this->qualifications)) {

            return Storage::disk('spaces')->url($this->qualifications);

        }

        return null;

    }

    public function getPhotoUrlAttribute() {

        if (isset($this->photo)) {

            return Storage::disk('spaces')->url($this->photo);

        } else {

            return self::DEFAULT_PHOTO_URL;

        }

    }

    public function getActionStringAttribute() {

        if ($this->isStudent()) {
            return 'learning';
        } elseif ($this->isTeacher()) {
            return 'teaching';
        } elseif ($this->isAdmin()) {
            return 'administrating';
        } else {
            return 'using the site';
        }

    }

    public function lessons() {

        return $this->hasMany(Lesson::class);

    }

    public function reviews() {

        return $this->hasMany(Review::class);

    }

    public function subscriptions() {

        return $this->hasMany(Subscription::class);

    }

    public function progresses() {

        return $this->hasMany(Progress::class);

    }

    public function answers() {

        return $this->hasMany(Answer::class);

    }

    public function discussions() {

        return $this->hasMany(Discussion::class);

    }

    public function payouts() {

        return $this->hasMany(Payout::class);

    }

    public function isStudent() {

        return $this->privilege === 1;

    }

    public function isTeacher() {

        return $this->privilege === 2 || $this->privilege === 3;

    }

    public function isRegisteredTeacher() {

        return $this->privilege === 3;
        
    }

    public function isAdmin() {

        return $this->privilege === 4;

    }

    public function isOwner(Lesson $lesson) {

        return $this->id === $lesson->user->id;

    }

    public function isAuthor(Review $review) {

        return $this->id === $review->user->id;

    }

    public function isCommenter(Discussion $discussion) {

        return $this->id === $discussion->user->id;

    }

    public function isSubscribed(Lesson $lesson) {

        return Subscription::exists($this, $lesson);

    }

    public function isPendingUpgrade() {
        
        return $this->pending_upgrade === 1;

    }

    public function canSubscribe(Lesson $lesson) {

        return Subscription::possible($this, $lesson);

    }

    public function addLesson(Lesson $lesson) {

        return $this->lessons()->save($lesson);

    }

    public function addProgress(Progress $progress) {

        $progression_list = Progress::where(['user_id' => $this->id, 'classroom_id' => $progress->classroom_id])->get();

        if ($progression_list->count() === 0) {

            return $this->lessons()->save($progress);

        }

        return $progression_list->first();

    }

    public function addReview(Review $review) {

        return $this->lessons()->save($review);

    }

    public function addSubscription(Subscription $subscription) {

        return $this->lessons()->save($subscription);
        
    }

    public function addAnswer(Answer $answer) {

        $no_answer_found = $this->getAnswer(Question::find($answer->question_id)) === null;

        if ($no_answer_found) {

            return $this->answers()->save($answer);

        } else {

            return null;

        }

    }

    public function getAnswer(Question $question) {

        return Answer::where(['user_id' => $this->id, 'question_id' => $question->id])->first();
        
    }

    public function getAnswerValue(Question $question) {

        $answer = $this->getAnswer($question);

        if (isset($answer)) {

            return $answer->answer;

        }

        return null;
        
    }

    public function addDiscussion(Discussion $discussion) {

        return $this->discussions()->save($discussion);

    }

    public function addPayout(Payout $payout) {

        return $this->payouts()->save($payout);

    }

    public function payOff($payoff_data) {

        $subscriptions_count = $payoff_data['subscriptions_count'];
        $payout_method = $payoff_data['payout_method'];

        if ($payout_method === 'Bank Transfer') {
            $bank = $payoff_data['bank'];
            $bank_account_number = $payoff_data['bank_account_number'];
            $payout_description = 'Transfer: '.$bank.', Account No.: '.$bank_account_number;
        } else if ($payout_method === 'Cheque') {
            $address = $payoff_data['address'];
            $payout_description = 'Cheque (No.:'.$payoff_data['cheque_number'].'), Mailed to: '.$address;
        } else {
            $payout_description = 'Unknown Payout type';
        }


        $amount = 0;

        $payout = $this->addPayout(new Payout([
            'amount' => $amount,
            'payout_date' => Carbon::now(),
            'payout_description' => $payout_description
        ]));

        $this->teacher_subscriptions->each(function ($subscription, $index) use (&$amount_owed, &$subscriptions_count, &$amount, &$payout) {
            if ($index < $subscriptions_count) {
                $amount += $subscription->payout_amount;
                $subscription->payOff($payout);
            }
        });

        $payout->update(['amount' => $amount]);

    }

    public function upgrade($new_privilege) {

        switch ($new_privilege) {

            case 'Teacher':
                $this->privilege = 3;
                break;
            case 'Tutor':
                $this->privilege = 2;
                break;
            default:
                $this->privilege = 1;

        }
        
        $this->pending_upgrade = 0;

        $this->update();

        event(new AccountUpgraded($this));

        return $this;

    }

    public function cancelUpgrade() {
        
        $this->update([
            'privilege' => 1,
            'teaching_experience' => null,
            'pending_upgrade' => 0
        ]);

        $this->deleteQualifications();
        $this->deleteIdentification();

        event(new AccountNotUpgraded($this));

        return $this->update();

    }

    public function downgrade() {
        
        $this->privilege = 1;
        $this->teaching_experience = null;

        $this->save();

        $this->deleteQualifications();
        $this->deleteIdentification();

        return $this;

    }

    public function flagForTeacherUpgrade($identification, $qualifications) {
        
        $this->uploadIdentification($identification);
        $this->uploadQualifications($qualifications);
        $this->update([
            'pending_upgrade' => 1
        ]);
        event(new UpgradePending($this));

    }

    public function upgradeToRegisteredTeacher($identification, $qualifications) {

        $this->uploadIdentification($identification);
        $this->uploadQualifications($qualifications);
        $this->upgrade('Teacher');
        
    }

    public function uploadPhoto($photo) {

        $photo_uri =  self::savePhoto('uploads/users/photos/', $photo, 'avatar');

        $this->update([
            'photo' => $photo_uri
        ]);
        
    }

    public function deletePhoto() {

        if (isset($this->photo)) {

            Storage::disk('spaces')->delete($this->photo);

            $this->update([
                'photo' => null
            ]);

        }

        return $this;
        
    }

    public function uploadQualifications($qualifications) {

        $this->qualifications = self::saveFile('uploads/users/qualifications/', $qualifications);
        $this->update();
        
    }

    public function uploadIdentification($identification) {

        $this->identification = self::saveFile('uploads/users/identification/', $identification);
        $this->update();
        
    }

    public function deleteQualifications() {

        if (isset($this->qualifications)) {

            Storage::disk('spaces')->delete($this->qualifications);

            $this->update([
                'qualifications' => null
            ]);

        }

        return $this;
        
    }

    public function deleteIdentification() {

        if (isset($this->identification)) {

            Storage::disk('spaces')->delete($this->identification);

            $this->update([
                'identification' => null
            ]);

        }

        return $this;
        
    }

    public function hasCredit() {

        return $this->credit > 0;
        
    }

    public function clearCredit() {

        $this->credit = 0;
        $this->save();
        
    }

    public static function getMaxAddressLength() {

        return self::MAX_ADDRESS_LENGTH;

    }

    public static function getMaxTeachingExperienceLength() {

        return self::MAX_TEACHING_EXPERIENCE_LENGTH;

    }

    private static function savePhoto($location, $photo, $type = 'hd') {

        $path = $location.$photo->hashName();

        if ($type === 'avatar') {

            $img = Image::make($photo)->fit(300, 300)->encode();

        } else {

            $img = Image::make($photo)->fit(1920, 1080)->encode();

        }
        
        return Storage::disk('spaces')->put($path, (string) $img, 'public') ? $path : null;

    }

    private static function saveFile($location, $file) {
        
        $path = $location.$file->hashName();

        return Storage::disk('spaces')->putFile($location, $file, 'public') ? $path : null;

    }

}
