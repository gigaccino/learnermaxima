<?php

namespace App;

use App\Defaults\Subject;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Laravel\Scout\Searchable;

class Lesson extends Model {

	use Searchable;

	const MAX_DESCRIPTION_LENGTH = 280;

	public $asYouType = true;

	// The attributes that are mass assignable.
	protected $fillable = [

	    'photo', 'subject', 'level', 'price', 'description', 'last_publish_date', 'subscriptions_disabled', 'marked_for_deletion'
	    
	];

	// The attributes that should be shown for arrays.
	protected $visible = [

	    'id', 'photo', 'subject', 'level', 'price', 'description', 'last_publish_date', 'subscriptions_disabled', 'marked_for_deletion'

	];

	public function __construct(array $attributes = array(), $photo = null) {

        // Eloquent
		parent::__construct($attributes);

		if (isset($photo)) {

			$this->photo = self::savePhoto($photo);

		}

	}

	public function user() {

	    return $this->belongsTo(User::class);

	}

	public function classrooms() {

	    return $this->hasMany(Classroom::class);

	}

	public function reviews() {

		return $this->hasMany(Review::class);

	}

	public function subscriptions() {

	    return $this->hasMany(Subscription::class);

	}

	public function getActiveSubscriptionsCountAttribute() {

		$count = 0;

		$this->subscriptions->each(function($subscription) use (&$count) {
			if ($subscription->isActive()) {
				$count++;
			}
		});

		return $count;
		
	}

	public function getPrettyIDAttribute() {

		$id = str_replace(" ", "-", $this->id);
		$level = str_replace(" ", "-", $this->level);
		$subject = str_replace(" ", "-", $this->subject); 
		$teacher_name = str_replace(" ", "-", $this->user->name);

		return "$id-$level-$subject-$teacher_name";

	}

	public function getUnwatchedClassroomsAttribute() {
		
		$count = 0;

		$this->classrooms->each(function($classroom) use (&$count){
            
			if (!$classroom->progress_completed && $classroom->isPublished()) {
				$count++;
			}

        });

		return $count;

	}

	public function getRatingAttribute() {

		$avg = $this->reviews->avg('rating');

		return $this->reviews->isNotEmpty() ?  round($avg) : 0;

	}

	public function getPhotoUrlAttribute() {

		if (isset($this->photo)) {

			return Storage::disk('spaces')->url($this->photo);

		}
		return (resolve(Subject::class))->getPhotoUrl($this->subject);

	}

	/**
	 * Get the indexable data array for the model.
	 *
	 * @return array
	 */
	public function toSearchableArray() {

	    $array = $this->toArray();

	    return [
	    	'id' => $array['id'],
	    	'subject' => $array['subject'],
	    	'level' => $array['level'],
	    	'description' => $array['description'],
	    ];

	}

	public function getClassroomByNumber($classroom_number) {

		if ($classroom = $this->classrooms->get($classroom_number - 1)) {
			return $classroom;
		}
		
		abort(404, 'Classroom not found.');

	}

	public function getReviewByNumber($review_number) {

		if ($review = $this->reviews->get($review_number - 1)) {
			return $review;
		}

		abort(404, 'Review not found.');
	}

	public function addClassroom(Classroom $classroom) {

		return $this->classrooms()->save($classroom);

	}

	public function deletePhoto() {

		if (isset($this->photo)) {

			Storage::disk('spaces')->delete($this->photo);

			$this->update([
			    'photo' => null
			]);

		}

		return $this;
		
	}

	public function uploadPhoto($photo) {
		
		$photo_uri =  self::savePhoto($photo);

		$this->update([
		    'photo' => $photo_uri
		]);

	}

	public function updateLatestPublishDate() {
		
		$last_publish_date = $this->created_at;
		$classrooms = $this->classrooms;

		$classrooms->each(function($classroom) use (&$last_publish_date) {
			if ($classroom->isPublished()) {
				if (Carbon::parse($classroom->publish_date)->gt(Carbon::parse($last_publish_date))) {
					$last_publish_date = $classroom->publish_date;
				}
			}
		});

		$this->update([
			'last_publish_date' => Carbon::parse($last_publish_date)
		]);

		return $last_publish_date;

	}

	public function getNextClassroom($classroom_number) {

		$classrooms = $this->classrooms;
		$next_classroom = null;

		$classrooms->each(function($classroom, $key) use(&$classroom_number, &$next_classroom) {
			$number = $key + 1;
			if (($number > $classroom_number) && ($classroom->isPublished())) {
				$next_classroom = $classroom;
				return false;
			}
		});

		return $next_classroom;
		
	}

	public function isSubscriptionsDisabled() {
		
		return $this->subscriptions_disabled === 1;

	}

	public function enableSubscriptions() {
		
		$this->update([
			'subscriptions_disabled' => 0
		]);

		return $this;

	}

	public function disableSubscriptions() {
		
		$this->update([
			'subscriptions_disabled' => 1
		]);

		return $this;

	}

	public static function getMaxDescriptionLength() {

		return self::MAX_DESCRIPTION_LENGTH;

	}

	private static function savePhoto($photo) {

		$path = 'uploads/lessons/photos/'.$photo->hashName();
		$img = Image::make($photo)->fit(1920, 1080)->encode();
		
		return Storage::disk('spaces')->put($path, (string) $img, 'public') ? $path : null;

	}
	
}
