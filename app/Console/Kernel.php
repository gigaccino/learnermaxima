<?php

namespace App\Console;

use App\Jobs\CleanUpDeletedAssignmentQuestions;
use App\Jobs\CleanUpDeletedAssignmentSolutions;
use App\Jobs\CleanUpDeletedClassroomAttachments;
use App\Jobs\CleanUpDeletedClassroomVideos;
use App\Jobs\CleanUpDeletedLessonPhotos;
use App\Jobs\CleanUpDeletedQuestionPhotos;
use App\Jobs\RemindRenewalsOfExpiringSubscriptions;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->job(new CleanUpDeletedLessonPhotos)->everyFiveMinutes();
        $schedule->job(new CleanUpDeletedClassroomVideos)->everyFiveMinutes();
        $schedule->job(new CleanUpDeletedClassroomAttachments)->everyFiveMinutes();
        $schedule->job(new CleanUpDeletedAssignmentQuestions)->everyFiveMinutes();
        $schedule->job(new CleanUpDeletedAssignmentSolutions)->everyFiveMinutes();
        $schedule->job(new CleanUpDeletedQuestionPhotos)->everyFiveMinutes();
        $schedule->job(new RemindRenewalsOfExpiringSubscriptions)->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
