<?php

namespace App\Jobs;

use App\Classroom;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CleanUpDeletedClassroomAttachments implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {

        // Clear video of deleted classroom.
        $deleted_classroom_with_attachment = Classroom::onlyTrashed()->whereNotNull('attachment')->first();
        
        if (isset($deleted_classroom_with_attachment)) {

            $deleted_classroom_with_attachment->deleteAttachment();

        }       

    }
}
