<?php

namespace App\Jobs;

use App\Assignment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CleanUpDeletedAssignmentSolutions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        
        // Clear assignment of deleted questions.
        $deleted_assignment_with_solutions = Assignment::onlyTrashed()->whereNotNull('solutions')->first();
        
        if (isset($deleted_assignment_with_solutions)) {

            $deleted_assignment_with_solutions->deleteSolutions();

        }   

    }
}
