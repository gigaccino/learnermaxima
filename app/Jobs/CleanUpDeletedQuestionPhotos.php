<?php

namespace App\Jobs;

use App\Question;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CleanUpDeletedQuestionPhotos implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {

        // Clear photo of deleted question.
        $deleted_question_with_photo = Question::onlyTrashed()->whereNotNull('photo')->first();
        
        if (isset($deleted_question_with_photo)) {

            $deleted_question_with_photo->deletePhoto();

        }  

    }
}
