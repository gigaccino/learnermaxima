<?php

namespace App\Jobs;

use App\Lesson;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CleanUpDeletedLessonPhotos implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {

        // Clear photo of deleted lesson.
        $deleted_lesson_with_photo = Lesson::onlyTrashed()->whereNotNull('photo')->first();
        
        if (isset($deleted_lesson_with_photo)) {

            $deleted_lesson_with_photo->deletePhoto();

        }  

    }
}
